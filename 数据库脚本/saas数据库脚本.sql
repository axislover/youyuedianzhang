USE [SuperiorShop]
GO
/****** Object:  Table [dbo].[C_ZyLog]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_ZyLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[Msg] [varchar](50) NULL,
 CONSTRAINT [PK_C_ZyLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_Users]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Users](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[ShopAppId] [int] NOT NULL,
	[AppType] [int] NOT NULL,
	[NickName] [varchar](50) NULL,
	[HeadImgUrl] [varchar](600) NULL,
	[Sex] [int] NULL,
	[Country] [varchar](50) NULL,
	[Province] [varchar](50) NULL,
	[City] [varchar](50) NULL,
	[Points] [int] NULL,
	[CreateTime] [datetime] NULL,
	[OptionStatus] [int] NULL,
	[OpenId] [varchar](100) NULL,
	[UnionId] [varchar](50) NULL,
	[Token] [varchar](50) NULL,
	[H5PhoneNumber] [varchar](50) NULL,
	[H5PassWord] [varchar](50) NULL,
	[H5UserType] [int] NULL,
	[LoginName] [varchar](50) NULL,
 CONSTRAINT [PK_C_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0,正常，-1冻结' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Users', @level2type=N'COLUMN',@level2name=N'OptionStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H5商城，手机号登录账号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Users', @level2type=N'COLUMN',@level2name=N'H5PhoneNumber'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H5商城手机号登录密码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Users', @level2type=N'COLUMN',@level2name=N'H5PassWord'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1微信注册，2手机号注册' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Users', @level2type=N'COLUMN',@level2name=N'H5UserType'
GO
/****** Object:  Table [dbo].[C_UserCoupon]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_UserCoupon](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CouponId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[CreateTime] [datetime] NULL,
	[IsUsed] [int] NULL,
 CONSTRAINT [PK_C_UserCoupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_SpecialPlace]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_SpecialPlace](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MenuId] [int] NOT NULL,
	[ImgPath] [varchar](150) NOT NULL,
	[CreateTime] [datetime] NULL,
	[ShopAdminId] [int] NOT NULL,
	[OptionStatus] [int] NOT NULL,
	[PlaceName] [varchar](20) NULL,
	[IsDel] [int] NULL,
	[Sorft] [int] NULL,
	[SpecialType] [int] NULL,
 CONSTRAINT [PK_C_SpecialPlace] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1专场 2专区' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_SpecialPlace', @level2type=N'COLUMN',@level2name=N'SpecialType'
GO
/****** Object:  Table [dbo].[C_ShopRoleAccount]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_ShopRoleAccount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Account] [varchar](50) NOT NULL,
	[PassWord] [varchar](50) NOT NULL,
	[RoleId] [int] NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[OptionStatus] [int] NULL,
	[IsDel] [int] NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_C_ShopRoleAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1正常，-1冻结' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopRoleAccount', @level2type=N'COLUMN',@level2name=N'OptionStatus'
GO
/****** Object:  Table [dbo].[C_ShopRole]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_ShopRole](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleName] [varchar](50) NULL,
	[CreateTime] [varchar](50) NULL,
	[Authoritys] [text] NULL,
	[IsDel] [int] NULL,
	[ShopAdminId] [int] NOT NULL,
 CONSTRAINT [PK_C_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_ShoppingCart]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_ShoppingCart](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Product_SkuId] [int] NOT NULL,
	[SaleNum] [int] NULL,
	[IsDel] [int] NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_C_ShoppingCart] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_ShopCashOutOrder]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_ShopCashOutOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[NickName] [varchar](50) NULL,
	[OpenId] [varchar](50) NOT NULL,
	[HeadImgUrl] [varchar](200) NULL,
	[OrderNo] [varchar](50) NOT NULL,
	[PayCode] [varchar](50) NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[CreateTime] [datetime] NULL,
	[PayTime] [datetime] NULL,
	[OrderStatus] [int] NULL,
	[Remark] [varchar](50) NULL,
	[LoginName] [varchar](50) NULL,
 CONSTRAINT [PK_C_ShopCashOutOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1成功，2失败，0处理中' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopCashOutOrder', @level2type=N'COLUMN',@level2name=N'OrderStatus'
GO
/****** Object:  Table [dbo].[C_ShopApp]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_ShopApp](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AppId] [varchar](50) NULL,
	[AppSecret] [varchar](50) NULL,
	[PaymentId] [varchar](50) NULL,
	[PaySecret] [varchar](50) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
	[ShopAdminId] [int] NOT NULL,
	[AppType] [int] NULL,
	[ServiceQQ] [varchar](50) NULL,
	[ServiceWx] [varchar](50) NULL,
	[ServicePhone] [varchar](20) NULL,
	[ShopName] [varchar](20) NULL,
	[OptionStatus] [int] NULL,
	[Remark] [varchar](50) NULL,
	[PayType] [int] NULL,
	[Version] [varchar](50) NULL,
 CONSTRAINT [PK_C_ShopExtend] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小程序APPID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopApp', @level2type=N'COLUMN',@level2name=N'AppId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小程序秘钥' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopApp', @level2type=N'COLUMN',@level2name=N'AppSecret'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小程序微信支付商户号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopApp', @level2type=N'COLUMN',@level2name=N'PaymentId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小程序微信支付秘钥' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopApp', @level2type=N'COLUMN',@level2name=N'PaySecret'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客服QQ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopApp', @level2type=N'COLUMN',@level2name=N'ServiceQQ'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客服微信' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopApp', @level2type=N'COLUMN',@level2name=N'ServiceWx'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客服联系电话' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopApp', @level2type=N'COLUMN',@level2name=N'ServicePhone'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1正常，2冻结，3，欠费' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopApp', @level2type=N'COLUMN',@level2name=N'OptionStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付类型，1.自己微信支付，2公司微信支付' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopApp', @level2type=N'COLUMN',@level2name=N'PayType'
GO
/****** Object:  Table [dbo].[C_ShopAdmin]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_ShopAdmin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LoginName] [varchar](50) NOT NULL,
	[PassWord] [varchar](50) NOT NULL,
	[Introduce] [varchar](200) NULL,
	[PayType] [int] NULL,
	[PhoneNumber] [varchar](20) NOT NULL,
	[QqCode] [varchar](20) NULL,
	[WxCode] [varchar](20) NULL,
	[Contact] [varchar](10) NOT NULL,
	[OptionStatus] [int] NOT NULL,
	[CreateTime] [datetime] NULL,
	[AccountManagerId] [int] NULL,
	[SerectId] [varchar](50) NULL,
	[ProgrameExpireTime] [datetime] NULL,
	[H5ExpireTime] [datetime] NULL,
	[CanUserAmount] [decimal](18, 2) NULL,
	[OpenId] [varchar](50) NULL,
	[NickName] [varchar](50) NULL,
	[HeadImgUrl] [varchar](200) NULL,
 CONSTRAINT [PK_C_Shop] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺付费类型1.订单比例分成,2,月付费,3年付费' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopAdmin', @level2type=N'COLUMN',@level2name=N'PayType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'联系人' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopAdmin', @level2type=N'COLUMN',@level2name=N'Contact'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'店铺状态1正常，2冻结，-1封号,0审核中' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopAdmin', @level2type=N'COLUMN',@level2name=N'OptionStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'客户经理ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopAdmin', @level2type=N'COLUMN',@level2name=N'AccountManagerId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'加密的店铺ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopAdmin', @level2type=N'COLUMN',@level2name=N'SerectId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'小程序到期时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopAdmin', @level2type=N'COLUMN',@level2name=N'ProgrameExpireTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'H5到期时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopAdmin', @level2type=N'COLUMN',@level2name=N'H5ExpireTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商户使用H5商城时，我司代收的金额。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShopAdmin', @level2type=N'COLUMN',@level2name=N'CanUserAmount'
GO
/****** Object:  Table [dbo].[C_ShippingTemplates]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_ShippingTemplates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShippingTemplatesName] [varchar](50) NOT NULL,
	[ChargType] [int] NOT NULL,
	[IsDel] [int] NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_C_ShippingTemplates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'运费模板名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShippingTemplates', @level2type=N'COLUMN',@level2name=N'ShippingTemplatesName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'计费方式，1按件数，2按重量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShippingTemplates', @level2type=N'COLUMN',@level2name=N'ChargType'
GO
/****** Object:  Table [dbo].[C_ShippingTemplateItem]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_ShippingTemplateItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShippingTemplateId] [int] NOT NULL,
	[ProvinceIds] [varchar](500) NULL,
	[FirstValue] [int] NULL,
	[FirstAmount] [decimal](18, 2) NULL,
	[NextValue] [int] NULL,
	[NextAmount] [decimal](18, 2) NULL,
	[IsDel] [int] NULL,
	[CreateTime] [datetime] NULL,
	[ProvinceStr] [varchar](500) NULL,
 CONSTRAINT [PK_C_ShippingTemplateItem] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'首件或者首重' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShippingTemplateItem', @level2type=N'COLUMN',@level2name=N'FirstValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'首件或者首重金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShippingTemplateItem', @level2type=N'COLUMN',@level2name=N'FirstAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'续重或者续件' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShippingTemplateItem', @level2type=N'COLUMN',@level2name=N'NextValue'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'续件或者续重金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ShippingTemplateItem', @level2type=N'COLUMN',@level2name=N'NextAmount'
GO
/****** Object:  Table [dbo].[C_SearchWord]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_SearchWord](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[KeyWord] [varchar](500) NOT NULL,
	[OptionStatus] [int] NULL,
	[IsDel] [int] NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PKC_SearchWord] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[c_sc]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[c_sc](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[s_no] [int] NULL,
	[c_no] [varchar](50) NULL,
	[score] [int] NULL,
 CONSTRAINT [PK_c_sc] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_Sall_Propety_Value]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Sall_Propety_Value](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[SallPropetyId] [int] NOT NULL,
	[PropetyValue] [varchar](50) NULL,
	[OptionStatus] [int] NOT NULL,
	[ProductId] [int] NULL,
	[IsDel] [int] NULL,
 CONSTRAINT [PK_C_Sall_Propety_Value] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态1正常，-1删除，0下架' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Sall_Propety_Value', @level2type=N'COLUMN',@level2name=N'OptionStatus'
GO
/****** Object:  Table [dbo].[C_Sall_Propety]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Sall_Propety](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PropetyName] [varchar](20) NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[OptionStatus] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[IsDel] [int] NULL,
 CONSTRAINT [PK_C_Sall_Propety] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'状态1正常，0下架。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Sall_Propety', @level2type=N'COLUMN',@level2name=N'OptionStatus'
GO
/****** Object:  Table [dbo].[C_RechargeOrder]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_RechargeOrder](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[RechargeId] [int] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[Title] [varchar](50) NOT NULL,
	[AppType] [int] NOT NULL,
	[SallType] [int] NOT NULL,
	[RechargeNo] [varchar](50) NOT NULL,
	[PayCode] [varchar](100) NULL,
	[CreateTime] [datetime] NULL,
	[PayTime] [datetime] NULL,
	[OrderStatus] [int] NULL,
	[LoginName] [varchar](50) NULL,
	[AccountManagerId] [int] NULL,
	[AccountMangerAmount] [decimal](18, 2) NULL,
 CONSTRAINT [PK_C_RechargeOrder] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'系统充值订单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_RechargeOrder', @level2type=N'COLUMN',@level2name=N'RechargeNo'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'支付三方交易号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_RechargeOrder', @level2type=N'COLUMN',@level2name=N'PayCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0待付款，1支付成功,2失败' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_RechargeOrder', @level2type=N'COLUMN',@level2name=N'OrderStatus'
GO
/****** Object:  Table [dbo].[C_Recharge]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Recharge](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NULL,
	[AppType] [int] NULL,
	[SallType] [int] NULL,
	[Amount] [decimal](18, 2) NULL,
 CONSTRAINT [PK_C_Recharge] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'月数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Recharge', @level2type=N'COLUMN',@level2name=N'SallType'
GO
/****** Object:  Table [dbo].[C_Question]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Question](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopAppId] [int] NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[Question] [varchar](5000) NULL,
	[CreateTime] [datetime] NULL,
	[AppType] [int] NOT NULL,
 CONSTRAINT [PK_C_Question] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_ProductImg]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_ProductImg](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ImagePath] [varchar](150) NOT NULL,
	[ImageType] [int] NULL,
	[ProductId] [int] NOT NULL,
	[Sorft] [int] NULL,
	[IsDefault] [int] NULL,
	[IsDel] [int] NULL,
 CONSTRAINT [PK_C_ProductImg] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1缩略图，2原图' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_ProductImg', @level2type=N'COLUMN',@level2name=N'ImageType'
GO
/****** Object:  Table [dbo].[C_Product_Sku]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Product_Sku](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PropetyCombineId] [varchar](50) NULL,
	[ProppetyCombineName] [varchar](100) NULL,
	[ProductId] [int] NOT NULL,
	[OptionStatus] [int] NULL,
	[StockNum] [int] NOT NULL,
	[SallPrice] [decimal](18, 2) NOT NULL,
	[LinePrice] [decimal](18, 2) NULL,
	[SkuType] [int] NOT NULL,
	[IsDel] [int] NULL,
	[SkuCode] [varchar](30) NULL,
	[ShopAdminId] [int] NOT NULL,
	[Weight] [float] NULL,
 CONSTRAINT [PK_C_Product_Sku] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售属性值ID组合100-100-222' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product_Sku', @level2type=N'COLUMN',@level2name=N'PropetyCombineId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售属性值组合红色+36cm+4G' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product_Sku', @level2type=N'COLUMN',@level2name=N'ProppetyCombineName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1正常，0下架，-1删除' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product_Sku', @level2type=N'COLUMN',@level2name=N'OptionStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售价' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product_Sku', @level2type=N'COLUMN',@level2name=N'SallPrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'划线价格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product_Sku', @level2type=N'COLUMN',@level2name=N'LinePrice'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'规格类型，1单规格，2多规格、。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product_Sku', @level2type=N'COLUMN',@level2name=N'SkuType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商品编码' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product_Sku', @level2type=N'COLUMN',@level2name=N'SkuCode'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商品重量kg' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product_Sku', @level2type=N'COLUMN',@level2name=N'Weight'
GO
/****** Object:  Table [dbo].[C_Product]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Product](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[ProductName] [varchar](50) NOT NULL,
	[Brand] [varchar](50) NULL,
	[Title] [varchar](50) NULL,
	[MenuId] [int] NOT NULL,
	[OptionStatus] [int] NULL,
	[CreateTime] [datetime] NULL,
	[Unit] [varchar](50) NULL,
	[PropetyIds] [varchar](20) NULL,
	[UpdateTime] [datetime] NULL,
	[Sorft] [int] NULL,
	[InitSallNum] [int] NULL,
	[IsDel] [int] NULL,
	[ProductDetail] [text] NULL,
	[CurrentSkuType] [int] NULL,
	[ShippingTemplateId] [int] NULL,
	[IsHot] [int] NULL,
	[SallCount] [int] NULL,
 CONSTRAINT [PK_C_Product] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1正常，0下架' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product', @level2type=N'COLUMN',@level2name=N'OptionStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'销售属性ID集合，0为单规格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product', @level2type=N'COLUMN',@level2name=N'PropetyIds'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1删除，0正常' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product', @level2type=N'COLUMN',@level2name=N'IsDel'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'当前商品的规格类型，1单规格，2多规格' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Product', @level2type=N'COLUMN',@level2name=N'CurrentSkuType'
GO
/****** Object:  Table [dbo].[C_OrderSearchExtend]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_OrderSearchExtend](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NULL,
	[ProductName] [varchar](50) NULL,
	[ProductId] [int] NULL,
	[ProppetyCombineName] [varchar](50) NULL,
	[BuyNum] [int] NULL,
	[SalePrice] [decimal](18, 2) NULL,
	[ImagePath] [varchar](150) NULL,
 CONSTRAINT [PK_C_OrderSearchExtend] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_OrderDetails]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_OrderDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderId] [int] NOT NULL,
	[ProductId] [int] NOT NULL,
	[Product_SkuId] [int] NOT NULL,
	[ProductName] [varchar](50) NOT NULL,
	[BuyNum] [int] NOT NULL,
	[SalePrice] [decimal](18, 2) NULL,
	[Points] [int] NULL,
	[CreateTime] [datetime] NULL,
	[PropetyCombineId] [varchar](50) NULL,
	[ProppetyCombineName] [varchar](50) NULL,
	[SkuCode] [varchar](30) NULL,
	[Weight] [float] NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_C_OrderDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_Order]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Order](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OrderNo] [varchar](50) NOT NULL,
	[ShopAppId] [int] NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[UserId] [int] NOT NULL,
	[ConsigneeName] [varchar](20) NOT NULL,
	[ConsigneePhone] [varchar](20) NOT NULL,
	[ZipCode] [varchar](20) NULL,
	[Address] [varchar](100) NOT NULL,
	[PayOderNo] [varchar](50) NULL,
	[RealityAmount] [decimal](18, 2) NOT NULL,
	[OrderStatus] [int] NULL,
	[PayStatus] [int] NULL,
	[SendStatus] [int] NULL,
	[CreateTime] [datetime] NULL,
	[PayTime] [datetime] NULL,
	[SendTime] [datetime] NULL,
	[Remark] [varchar](100) NULL,
	[FailReason] [varchar](50) NULL,
	[ServiceTime] [datetime] NULL,
	[ServiceStatus] [int] NULL,
	[AppType] [int] NULL,
	[OrderAmount] [decimal](18, 2) NULL,
	[CouponId] [int] NULL,
	[CouponDelAmount] [decimal](18, 2) NULL,
	[ShippingAmount] [decimal](18, 2) NULL,
	[CouponName] [varchar](50) NULL,
	[LoginName] [varchar](50) NULL,
	[PayType] [int] NULL,
	[DeliveryName] [varchar](50) NULL,
	[DeliveryNo] [varchar](50) NULL,
 CONSTRAINT [PK_C_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'实付金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'RealityAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0，待付款，1，（已发货）订单成功，2订单失败（15分钟未支付），3已付款，4申请售后订单，' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'OrderStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0,待支付，1支付成功' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'PayStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0未发货，1已发货' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'SendStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'订单失败原因' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'FailReason'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'申请售后时间' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'ServiceTime'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0，售后处理中，1已处理' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'ServiceStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'商品金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'OrderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'使用的优惠券ID' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'CouponId'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠券低金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'CouponDelAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'运费' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'ShippingAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'优惠券名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'CouponName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'收款方式,1H5商城的公司代收，2.自有支付' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'PayType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'快递名称' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'DeliveryName'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'快递单号' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Order', @level2type=N'COLUMN',@level2name=N'DeliveryNo'
GO
/****** Object:  Table [dbo].[C_Menu]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Menu](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MenuName] [varchar](20) NOT NULL,
	[Sorft] [int] NULL,
	[CreateTime] [datetime] NULL,
	[OptionStatus] [int] NULL,
	[IsDel] [int] NULL,
	[ShopAdminId] [int] NOT NULL,
	[ImgPath] [varchar](150) NULL,
	[IsAll] [int] NULL,
 CONSTRAINT [PK_C_Menu] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_ManagerAccount]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_ManagerAccount](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[OpenId] [varchar](50) NULL,
	[PhoneNumber] [varchar](50) NULL,
	[TrueName] [varchar](50) NULL,
	[Amount] [decimal](18, 2) NULL,
	[OptionStatus] [int] NULL,
	[CreateTime] [datetime] NULL,
	[HeadImgUrl] [varchar](300) NULL,
	[NickName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_C_ManagerAccount] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_FullAmountSet]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_FullAmountSet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[Amount] [int] NULL,
	[OptionStatus] [int] NULL,
	[NotHasProductIds] [varchar](500) NULL,
	[NotHasProvinceIds] [varchar](500) NULL,
 CONSTRAINT [PK_C_FullAmountSet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'单笔满多少金额包邮,0全场包邮' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_FullAmountSet', @level2type=N'COLUMN',@level2name=N'Amount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'0关闭，1开启' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_FullAmountSet', @level2type=N'COLUMN',@level2name=N'OptionStatus'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'不参与满额包邮的商品' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_FullAmountSet', @level2type=N'COLUMN',@level2name=N'NotHasProductIds'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'不参与满额包邮的地区' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_FullAmountSet', @level2type=N'COLUMN',@level2name=N'NotHasProvinceIds'
GO
/****** Object:  Table [dbo].[C_Decorate]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[C_Decorate](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[DecorateDesign] [nvarchar](2000) NULL,
	[CreateTime] [datetime] NULL,
	[UpdateTime] [datetime] NULL,
 CONSTRAINT [PK_C_Decorate] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C_Coupon]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Coupon](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[CouponName] [varchar](50) NULL,
	[CouponType] [int] NULL,
	[DelAmount] [decimal](18, 2) NULL,
	[Discount] [int] NULL,
	[MinOrderAmount] [decimal](18, 2) NULL,
	[Days] [int] NULL,
	[TotalCount] [int] NULL,
	[CreateTime] [datetime] NULL,
	[IsDel] [int] NULL,
 CONSTRAINT [PK_C_Coupon] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'1.满减，2，折扣' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Coupon', @level2type=N'COLUMN',@level2name=N'CouponType'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'几折。' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Coupon', @level2type=N'COLUMN',@level2name=N'Discount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'最低消费金额' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Coupon', @level2type=N'COLUMN',@level2name=N'MinOrderAmount'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'有效天数' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Coupon', @level2type=N'COLUMN',@level2name=N'Days'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'总发放量' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Coupon', @level2type=N'COLUMN',@level2name=N'TotalCount'
GO
/****** Object:  Table [dbo].[C_Consignee]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Consignee](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ZipCode] [varchar](50) NULL,
	[ProvinceCode] [varchar](10) NULL,
	[CityCode] [varchar](10) NULL,
	[AreaCode] [varchar](10) NULL,
	[IsDel] [int] NULL,
	[IsDefault] [int] NULL,
	[CreateTime] [datetime] NULL,
	[ConsigneeName] [varchar](50) NULL,
	[ConsigneePhone] [varchar](20) NULL,
	[Address] [varchar](100) NULL,
 CONSTRAINT [PK_C_Consignee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_Comment]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Comment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[ShopAppId] [int] NOT NULL,
	[ShopAdminId] [int] NOT NULL,
	[LoginName] [varchar](50) NOT NULL,
	[ProductId] [int] NOT NULL,
	[CommentLevel] [int] NOT NULL,
	[CommentMsg] [varchar](1000) NOT NULL,
	[CreateTime] [datetime] NULL,
	[IsDel] [int] NULL,
	[IsEdit] [int] NULL,
	[OrderId] [int] NOT NULL,
 CONSTRAINT [PK_C_Comment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'是否修改过，可以修改一次' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'TABLE',@level1name=N'C_Comment', @level2type=N'COLUMN',@level2name=N'IsEdit'
GO
/****** Object:  Table [dbo].[C_BannerIndex]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_BannerIndex](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ProductId] [int] NOT NULL,
	[OptionStatus] [int] NULL,
	[ImgPath] [varchar](500) NOT NULL,
	[IsDel] [int] NULL,
	[Sorft] [int] NULL,
	[ShopAdminId] [int] NOT NULL,
	[CreateTime] [datetime] NULL,
 CONSTRAINT [PK_C_BannerIndex] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_Areas]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Areas](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Area_Code] [int] NOT NULL,
	[Parent_Id] [int] NOT NULL,
	[Area_Name] [varchar](100) NULL,
	[Sort] [int] NULL,
 CONSTRAINT [PK_C_Areas] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_Admin]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_Admin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[LoginName] [varchar](50) NULL,
	[PassWord] [varchar](50) NULL,
 CONSTRAINT [PK_C_Admin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[C_AccountMangerOrderLog]    Script Date: 11/15/2021 16:27:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C_AccountMangerOrderLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AccountManagerId] [int] NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[CreateTime] [datetime] NULL,
	[PayCode] [varchar](50) NULL,
	[PayTime] [datetime] NULL,
	[OrderStatus] [int] NULL,
	[OrderNo] [varchar](50) NULL,
	[Remark] [varchar](100) NULL,
	[NickName] [varchar](50) NULL,
 CONSTRAINT [PK_C_AccountMangerOrderLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  StoredProcedure [dbo].[WxRechareCall]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[WxRechareCall]
@orderno varchar(50),
@payCode varchar(50),
@state int,
@rtn_err int output
as
begin
	if not exists(select Id from C_RechargeOrder where RechargeNo=@orderno and OrderStatus=0)
	begin
		set @rtn_err = 2--订单不存在或已完成
		return	
	end
	
		--开始事务
	SET XACT_ABORT  ON 
	begin tran tran_wxcall
	--更新充值订单状态
	update C_RechargeOrder set OrderStatus = @state,PayCode = @payCode,PayTime=GETDATE() where RechargeNo=@orderno
	--如果是成功，更新商户该类型的商城时间
	if(@state=1)
	begin
		declare @apptype int
		declare @salltype int--月数
		declare @shopadminid int
		declare @ProgrameExpireTime datetime
		declare @H5ExpireTime datetime
		declare @tempTime datetime
		declare @accountManagerId int
		declare @amount decimal(18,2)
		select @apptype=AppType,@salltype=SallType,@shopadminid=ShopAdminId,@accountManagerId=AccountManagerId,@amount=Amount from C_RechargeOrder where RechargeNo=@orderno 
		--查询当前商户的小程序商城和H5商城的过期时间,未充值的过 的是null，给当前时间	
		select @ProgrameExpireTime=isnull(ProgrameExpireTime,GETDATE()),@H5ExpireTime=isnull(H5ExpireTime,GETDATE())  from C_ShopAdmin where Id=@shopadminid	
		if(@apptype=1)
		begin
			if(@ProgrameExpireTime<=GETDATE())
			begin
				--到期时间小于等于当前时间，可能的情况1.充值过但早就到期,2未充值过
				set @tempTime = dateadd(month,@salltype,getdate())
				
			end
			else
			begin
				--到期时间大于当前时间，说明当前充值是续充。
				set @tempTime = dateadd(month,@salltype,@ProgrameExpireTime)
			end
			update C_ShopAdmin set ProgrameExpireTime = @tempTime where Id = @shopadminid
		end	
		if(@apptype=2)
		begin
			if(@H5ExpireTime<=GETDATE())
			begin
				--到期时间小于等于当前时间，可能的情况1.充值过但早就到期,2未充值过
				set @tempTime = dateadd(month,@salltype,getdate())
				
			end
			else
			begin
				--到期时间大于当前时间，说明当前充值是续充。
				set @tempTime = dateadd(month,@salltype,@H5ExpireTime)
			end
			update C_ShopAdmin set H5ExpireTime = @tempTime where Id = @shopadminid
		end	
		--给推广人增加提成
		if(@accountManagerId>0)
		begin
			update C_ManagerAccount set Amount = Amount+(@amount*0.2) where Id=@accountManagerId
			update C_RechargeOrder set AccountMangerAmount = @amount*0.2 where RechargeNo = @orderno
		end	
			
	end	
		
	if(@@error<>0)
	begin
		 rollback tran tran_wxcall
		 set @rtn_err =3--事务出错
		 return
	end	
	else
	begin
		commit tran tran_wxcall
		set @rtn_err = 1
		return
	end
	
			
end
GO
/****** Object:  StoredProcedure [dbo].[wx_login_register]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[wx_login_register]
@shopAppId int,
@appType int,
@nickName varchar(50),
@headImgUrl varchar(600),
@sex int,
@openid varchar(100),
@token varchar(50)
as
begin
	
	--是否存在用户，存在更新,不存在插入，H5商城都走得公司服务好授权，可能会同一个用户在多个商城授权，由于OPENID相同，所以判断要加上商城ID。
	if exists(select Id from C_Users where OpenId=@openid and ShopAppId=@shopAppId)
	begin
		update C_Users set NickName=@nickName,HeadImgUrl=@headImgUrl,Sex=@sex,Token=@token where OpenId=@openid and ShopAppId=@shopAppId
	end	
	else
	begin
		declare @shopAdminId int
		declare @loginName varchar(50)
		select @shopAdminId=sa.ShopAdminId,@loginName =csa.LoginName  from C_ShopApp sa left join C_ShopAdmin csa on sa.ShopAdminId=csa.Id where sa.Id=@shopAppId
		insert into C_Users(ShopAdminId,ShopAppId,AppType,NickName,HeadImgUrl,Sex,OpenId,Token,LoginName)values(@shopAdminId,@shopAppId,@appType,@nickName,@headImgUrl,@sex,@openid,@token,@loginName)
	end
	declare @shopName varchar(50)
	select @shopName=ShopName from C_ShopApp where Id=@shopAppId
	select Id,NickName,HeadImgUrl,OptionStatus,CreateTime,Token,@shopName as ShopName from C_Users where OpenId=@openid and Token=@token and ShopAppId=@shopAppId
	
end
GO
/****** Object:  StoredProcedure [dbo].[wx_login_mannager_account]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[wx_login_mannager_account]
@nickName varchar(50),
@headImgUrl varchar(600),
@openid varchar(100)
as
begin
	if exists(select Id from C_ManagerAccount where OpenId=@openid)
	begin
		--存在更新
		update C_ManagerAccount set NickName = @nickName,HeadImgUrl=@headImgUrl,OpenId=@openid where OpenId = @openid
	end
	else
	begin
		--添加
			
		insert into C_ManagerAccount(OpenId,HeadImgUrl,NickName)values(@openid,@headImgUrl,@nickName)
	end
	select * from C_ManagerAccount where OpenId = @openid
end
GO
/****** Object:  StoredProcedure [dbo].[update_consignee_isdefault]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[update_consignee_isdefault]
@id int ,
@userid int,
@isdefault int
as
begin
	if(@isdefault=1)
	begin
		SET XACT_ABORT  ON 
		begin tran tran_updateState
		update  C_Consignee set IsDefault=0 where UserId=@userid and IsDel=0 and IsDefault=1
		update  C_Consignee set IsDefault=1 where UserId=@userid and IsDel=0 and ID= @id  
		if(@@error<>0)
		begin
		     rollback tran tran_updateState
			
		end	
		else
		begin
			commit tran tran_updateState
			
		end
	end
	else
	begin
		update  C_Consignee set IsDefault=0 where UserId=@userid and IsDefault=1 and ID= @id  
		
	end
end
GO
/****** Object:  StoredProcedure [dbo].[transferAmount_managerAccount]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create proc [dbo].[transferAmount_managerAccount]
@id int,
@rtn_err int output
as
begin
	if exists(select Id from C_AccountMangerOrderLog where Id=@id and OrderStatus=1)
	begin
		set @rtn_err = 2--订单已成功,请勿重复提交
		return
	end
	declare @managerAccountId int
	declare @accountAmount decimal(18,2)--推广人当前账号金额
	declare @amount decimal(18,2)--提现的金额
	select @managerAccountId = o.AccountManagerId,@amount=o.Amount ,@accountAmount=a.Amount from C_AccountMangerOrderLog o left join C_ManagerAccount a on o.AccountManagerId=a.Id where o.Id=@id
	if(@accountAmount< @amount)
	begin
		set @rtn_err =3--账户余额不足
		return
	end	
	SET XACT_ABORT  ON 
		begin tran tran_transfer
		update C_AccountMangerOrderLog set OrderStatus=1 where Id=@id
		update C_ManagerAccount set Amount = Amount-@amount where Id=@managerAccountId
		if(@@error<>0)
		begin
		     rollback tran tran_transfer
			set @rtn_err =4--执行出错
			return
		end	
		else
		begin
			commit tran tran_transfer
			set @rtn_err = 1
			return
		end
end
GO
/****** Object:  StoredProcedure [dbo].[programe_create_order_cart]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[programe_create_order_cart]
@orderno varchar(50),
@userid int,
@spid int,
@said int,
@consigneeId int,
@remark varchar(100),
@couponId int,
@shipAmount decimal(18,2),--运费
@totalAmount decimal(18,2),--实付金额
@apptype int,
@productAmount decimal(18,2),--商品金额，订单金额,
@couponAmount decimal(18,2),--優惠券優惠金额
@rtn_err int output
as
begin
	if not exists(select Id from C_Users where Id=@userid and ShopAppId=@spid and ShopAdminId=@said)
	BEGIN
		set @rtn_err=2 --用户不存在
		return
	END
	if not exists(select Id from C_Consignee where Id=@consigneeId)
	BEGIN
		set @rtn_err=3 --地址不存在
		return
	END
	if not exists(select Id from C_ShopAdmin where Id=@said)
	BEGIN
		set @rtn_err=4 --商户不存在
		return
	END
	
	--开启整个订单入库事务
	SET XACT_ABORT  ON 
	begin tran tran_create
	declare @idnew bigint
	declare @consigneeName varchar(20)
	declare @consigneePhone varchar(20)
	declare @zipcode varchar(20)
	declare @address varchar(100)
	declare @provinceCode varchar(10)
	declare @cityCode varchar(10)
	declare @areaCode varchar(10)
	
	declare @provice_name varchar(30)
	declare @city_name varchar(30)
	declare @area_name varchar(50)
	declare @fullAddress varchar(150)
	
	select @consigneeName=ConsigneeName,@consigneePhone = ConsigneePhone,@zipCode=ZipCode,@address=Address,@provinceCode=ProvinceCode,@cityCode=CityCode,@areaCode=AreaCode from C_Consignee where ID = @consigneeId
--拼接完整地址
	select @provice_name= Area_Name from C_Areas where Area_Code=@provinceCode
	select @city_name= Area_Name from C_Areas where Area_Code=@cityCode 
	--判断是直辖市，city给空
	if(@city_name='市辖区' or @city_name='县')
	begin
		set @city_name = ''
	end
	select @area_name= Area_Name from C_Areas where Area_Code=@areaCode
	set @fullAddress =  @provice_name+@city_name+@area_name+@address
	
	declare @couponName varchar(50)
	if(@couponId>0)
	begin
		select @couponName = CouponName from C_Coupon where Id = @couponId
	end
	
	declare @paytype int
	declare @loginname varchar(50)
	select @loginname = LoginName from C_ShopAdmin where Id=@said
	select @paytype = PayType from C_ShopApp where Id= @spid
	
	--入库主订单
	insert into C_Order(OrderNo,ShopAppId,ShopAdminId,UserId,ConsigneeName,ConsigneePhone,ZipCode,Address,RealityAmount,Remark,AppType,OrderAmount,CouponId,CouponDelAmount,ShippingAmount,CouponName,LoginName,PayType)values(@orderno,@spid,@said,@userid,@consigneeName,@consigneePhone,@zipcode,@fullAddress,@totalAmount,@remark,@apptype,@productAmount,@couponId,@couponAmount,@shipAmount,@couponName,@loginname,@paytype)
	set @idnew= @@identity
	
	--入库订单详情
	insert into C_OrderDetails(OrderId,ProductId,Product_SkuId,ProductName,BuyNum,SalePrice,PropetyCombineId,ProppetyCombineName,SkuCode,Weight,UserId) 
	select @idnew,sc.ProductId,sc.Product_SkuId,p.ProductName,sc.SaleNum,ps.SallPrice,ps.PropetyCombineId,ps.ProppetyCombineName,ps.SkuCode,ps.Weight,@userid from C_ShoppingCart sc left join C_Product p on sc.ProductId=p.Id
	                               left join C_Product_Sku ps on sc.Product_SkuId=ps.Id
	                               where UserId=@userid and sc.IsDel=0
	--由于订单列表，要列出来1个订单详情，用1个辅助表来帮助关联查询，取购物车中得第一个
	insert into C_OrderSearchExtend(OrderId,ProductName,ProductId,ProppetyCombineName,BuyNum,SalePrice,ImagePath)
	select top(1) @idnew,p.ProductName,sc.ProductId,ps.ProppetyCombineName,sc.SaleNum,ps.SallPrice,cpi.ImagePath from  C_ShoppingCart sc left join C_Product p on sc.ProductId=p.Id
	                               left join C_Product_Sku ps on sc.Product_SkuId=ps.Id
	                               left join C_ProductImg cpi on sc.ProductId=cpi.ProductId and cpi.IsDefault=1 and cpi.IsDel=0
	                               where UserId=@userid and sc.IsDel=0
	
	--修改sku库存
	update ps set ps.StockNum = ps.StockNum-sc.SaleNum  from C_Product_Sku as ps ,C_ShoppingCart as sc where ps.Id=sc.Product_SkuId and sc.UserId=@userid and  sc.IsDel=0
	
	--销量，多个商品可能
	update p set p.SallCount = p.SallCount+sc.SaleNum from C_Product p ,(select ProductId,SUM(SaleNum) as SaleNum from C_ShoppingCart where UserId=@userid and IsDel=0 group by ProductId ) as sc where p.Id=sc.ProductId   
	
		
	--删除临时购物车
	update C_ShoppingCart set IsDel = 1 where UserId=@userid	
	
	if(@couponId>0)
	begin
		--将当前优惠券标记成已使用
		update C_UserCoupon set IsUsed=1 where CouponId=@couponId and UserId = @userid
	end
	if(@@error<>0)
		begin
		     rollback tran tran_create
			set @rtn_err =5--执行事务出错
			return
		end	
		else
		begin
			commit tran tran_create
			set @rtn_err = 1
			--查询用于微信统一下单数据
			declare @openid varchar(50)
			select @openid = OpenId from C_Users where ID = @userid
			select @orderno as OrderNumber,@idnew as OrderID,PaySecret as Paykey,AppID as Appid,PaymentID as Mch_id,@openid as Openid,@totalAmount as TotalFee,PayType from C_ShopApp  where ID = @spid
			
			return
		end
end
GO
/****** Object:  StoredProcedure [dbo].[program_wx_notice_cart]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[program_wx_notice_cart]
@orderno varchar(50),
@totalFee decimal(18,2),
@status int ,
@transaction_id varchar(50),
@rtn_err int output
as
begin
	if not exists(select Id from C_Order where OrderNo=@orderno and OrderStatus=0)
	begin
		set @rtn_err=2--订单不存在
		return
	end
	if(@status=1)
	begin
		SET XACT_ABORT  ON 
		begin tran tran_updateOrder
		--将订单状态改为已付款
		update C_Order set OrderStatus=3,PayStatus=1,PayOderNo=@transaction_id,PayTime=GETDATE()  where OrderNo=@orderno and OrderStatus=0
		--如果是H5商城，并且是我司代收，给当前商城的商户，增加金额
		declare @apptype int
		declare @paytype int
		declare @shopAdminId int
		select @apptype=AppType,@paytype=PayType,@shopAdminId=ShopAdminId from C_Order where OrderNo = @orderno
		if(@apptype=2 and @paytype=1)
		begin
			update C_ShopAdmin set CanUserAmount = CanUserAmount+@totalFee where Id=@shopAdminId
		end
		if(@@error<>0)
		begin
		     rollback tran tran_updateOrder
			set @rtn_err =3--支付成功事务出错
			return
		end	
		else
		begin
			commit tran tran_updateOrder
			set @rtn_err = 1
			return
		end
	end
	else
	begin
		SET XACT_ABORT  ON 
		begin tran tran_rollbackData
		--将订单状态改为失败
		update C_Order set OrderStatus=2 where OrderNo=@orderno and OrderStatus=0
		--回滚库存
		declare @orderid int
		declare @couponid int
		declare @userid int
		select @orderid=Id,@couponid=CouponId,@userid=UserId from C_Order where OrderNo = @orderno
		update ps set ps.StockNum = ps.StockNum+cd.BuyNum  from C_OrderDetails as cd ,C_Product_Sku as ps where cd.Product_SkuId=ps.Id and cd.OrderId=@orderid 
	
		--回滚销量
		update p set P.SallCount = P.SallCount-cd.BuyNum from C_Product p ,(select ProductId,Sum(BuyNum)as BuyNum from C_OrderDetails where OrderId=@orderid group by ProductId) as cd where p.Id = cd.ProductId
		
		--如果使用优惠券，回滚使用状态
		if(@couponid>0)
		begin
			update C_UserCoupon set IsUsed=0 where CouponId=@couponid and UserId = @userid
		end	
		if(@@error<>0)
		begin
		     rollback tran tran_rollbackData
			set @rtn_err =4--支付失败事务出错
			return
		end	
		else
		begin
			commit tran tran_rollbackData
			set @rtn_err = 1
			return
		end
		
	end
end
GO
/****** Object:  StoredProcedure [dbo].[OptionProduct]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[OptionProduct]
@id int,
@optionStatus int,
@shopAdminId int,
@rtn_err int output
as
begin
	if not exists(select Id from C_Product where Id = @id and ShopAdminId=@shopAdminId)
	begin
		set @rtn_err  = 3--商品不存在
		return
	end
	
	if(@optionStatus=0)--下架情况的判断
	begin
		--如果有正在交易的订单，不允许下架，待定
		select 1
	end
    if(@optionStatus=1)--上架情况判断
	begin
		--如果没有设置SKU，不允许上架
		declare @skuCount int
		select @skuCount=COUNT(*) from C_Product_Sku where ProductId=@id and IsDel=0
		if(@skuCount<=0)
		begin
			set @rtn_err=4--请先设置商品SKU
			return
		end
	end
	
	update C_Product set OptionStatus=@optionStatus where Id=@id and ShopAdminId=@shopAdminId
	set @rtn_err = 1
	return 
end
GO
/****** Object:  StoredProcedure [dbo].[OptionMenu]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[OptionMenu]
@id int,
@optionStatus int,
@shopAdminId int,
@rtn_err int output
as
begin
	if not exists(select Id from C_Menu where Id = @id and ShopAdminId=@shopAdminId)
	begin
		set @rtn_err  = 3--分类不存在
		return
	end
	
	if(@optionStatus=0)
	begin
		if exists(select Id from C_Product where MenuId=@id and OptionStatus=1 and IsDel=0 and ShopAdminId=@shopAdminId)
		begin
			set @rtn_err = 2--当前分类下有正在上架的商品
			return 
		end
		if exists(select Id from C_SpecialPlace where MenuId=@id and OptionStatus=1 and IsDel=0 and ShopAdminId=@shopAdminId)
		begin
			set @rtn_err = 4--当前分类下有正在上架的专场
			return 
		end
	end	
	
	update C_Menu set OptionStatus=@optionStatus where Id=@id and ShopAdminId=@shopAdminId
	set @rtn_err = 1
	return 
end
GO
/****** Object:  StoredProcedure [dbo].[InsertShopApp]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertShopApp]
@AppId varchar(50),
@AppSecret varchar(50),
@PaymentId varchar(50),
@PaySecret varchar(50),
@ShopAdminId varchar(50),
@AppType int,
@ServiceQQ varchar(50),
@ServiceWx varchar(50),
@ServicePhone varchar(50),
@ShopName varchar(50),
@PayType int,
@rtn_err int output
as
begin
	if(@AppType=1)
	begin
		if exists(select Id from C_ShopApp where AppId=@AppId)
	begin
		set @rtn_err = 2--当前小程序APPID已存在
		return
	end
	end
	
	if exists(select Id from C_ShopApp where ShopName=@ShopName and ShopAdminId=@ShopAdminId)
	begin
		set @rtn_err = 3--当前店主账号下已存在店铺名称
		return
	end
	
	insert into C_ShopApp(AppId,AppSecret,PaymentId,PaySecret,ShopAdminId,AppType,ServiceQQ,ServiceWx,ServicePhone,ShopName,PayType)values(@AppId,@AppSecret,@PaymentId,@PaySecret,@ShopAdminId,@AppType,@ServiceQQ,@ServiceWx,@ServicePhone,@ShopName,@PayType)
	set @rtn_err = 1
	return	
	
end
GO
/****** Object:  StoredProcedure [dbo].[InsertShop]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertShop]
@LoginName varchar(50),
@PassWord varchar(50),
@Introduce varchar(200),
@PhoneNumber varchar(20),
@qqCode varchar(20),
@WxCode varchar(20),
@Contact varchar(10),
@AccountManagerId int,
@rtn_err int output
as
begin
	if exists(select Id from C_ShopAdmin where LoginName =@LoginName and OptionStatus>0)
	begin
		set @rtn_err = 2--用户名已存在
		return
	end
	if exists(select Id from C_ShopAdmin where PhoneNumber =@PhoneNumber and OptionStatus>0)
	begin
		set @rtn_err = 3--手机号已经存在
		return
	end
		--开始事务
	SET XACT_ABORT  ON 
	begin tran tran_add
	declare @idnew_shopAdmin int
	declare @idnew_shipping int
	declare @expressTime datetime
	--新注册送15天使用
	set @expressTime = dateadd(DAY,15,getdate())
	insert into C_ShopAdmin(LoginName,PassWord,Introduce,PayType,PhoneNumber,QqCode,WxCode,Contact,OptionStatus,AccountManagerId,H5ExpireTime,ProgrameExpireTime)values(@LoginName,@PassWord,@Introduce,1,@PhoneNumber,@qqCode,@WxCode,@Contact,0,@AccountManagerId,@expressTime,@expressTime)
	set @idnew_shopAdmin= @@identity 	
	--初始化运费模板
	insert into C_ShippingTemplates(ShippingTemplatesName,ChargType,ShopAdminId)values('全国包邮',1,@idnew_shopAdmin)
	set @idnew_shipping= @@identity 
	insert into C_ShippingTemplateItem(ShippingTemplateId,ProvinceIds,FirstValue,FirstAmount,NextValue,NextAmount,ProvinceStr)values(@idnew_shipping,'110000,120000,130000,140000,150000,210000,220000,230000,310000,320000,330000,340000,350000,360000,370000,410000,420000,430000,440000,450000,460000,500000,510000,520000,530000,540000,610000,620000,630000,640000,650000',1,0,0,0,'全国')
	--初始化满减
	insert into C_FullAmountSet(ShopAdminId)values(@idnew_shopAdmin)
	
		
	if(@@error<>0)
	begin
		 rollback tran tran_add
		 set @rtn_err =4--事务出错
		 return
	end	
	else
	begin
		commit tran tran_add
		set @rtn_err=1
		return	
	end
	
	
end
GO
/****** Object:  StoredProcedure [dbo].[InsertMenu]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[InsertMenu]
@menuName varchar(20),
@shopAdminId int,
@sorft int,
@isAll int,
@imgpath varchar(150),
@rtn_err int output
as
begin	
	if exists(select Id from C_Menu where ShopAdminId=@shopAdminId and IsDel=0 and MenuName=@menuName)
	begin
		set @rtn_err = 2--已存在同名分类
		return 
	end
	insert into C_Menu(MenuName,Sorft,ShopAdminId,ImgPath,IsAll)values(@menuName,@sorft,@shopAdminId,@imgpath,@isAll)
	set @rtn_err = 1
	return 
end
GO
/****** Object:  StoredProcedure [dbo].[getorderlist_program]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getorderlist_program]
@spid int,
@userid int,
@offset int,
@size int,
@orderstatus int
as
begin
	--查出所有主订单集合
	if(@orderstatus=999)
	begin
		select top(@size) * from
		(select ROW_NUMBER() over(order by o.Id desc)as rownum,
		o.Id,
		o.OrderNo,
		o.RealityAmount,
		o.OrderStatus,
		o.ShopAdminId,
		os.ProductId,
		os.ProductName,
		os.ProppetyCombineName,
		os.BuyNum,
		os.SalePrice,
		os.ImagePath,
		isnull((select top(1) Id from C_Comment with(nolock) where OrderId=os.OrderId order by Id),0)as CommentId,
		(select COUNT(*) from C_OrderDetails where OrderId=o.Id) as ProductNum 
		from C_Order o with(nolock) 
		left join C_OrderSearchExtend os on o.Id= os.OrderId
		where o.UserId=@userid and o.ShopAppId=@spid   ) tt
		 where tt.rownum>@size*@offset
		 
	end
	else
	begin
		select top(@size) * from
		(select ROW_NUMBER() over(order by o.Id desc)as rownum,
		o.Id,
		o.OrderNo,
		o.RealityAmount,
		o.OrderStatus,
		o.ShopAdminId,
		os.ProductId,
		os.ProductName,
		os.ProppetyCombineName,
		os.BuyNum,
		os.SalePrice,
		os.ImagePath,
		isnull((select top(1) Id from C_Comment with(nolock) where OrderId=os.OrderId order by Id),0)as CommentId,
		(select COUNT(*) from C_OrderDetails where OrderId=o.Id) as ProductNum 
		from C_Order o with(nolock) 
		left join C_OrderSearchExtend os on o.Id= os.OrderId
		where o.UserId=@userid and o.ShopAppId=@spid and o.OrderStatus=@orderstatus   ) tt
		 where tt.rownum>@size*@offset
	end
	
	
end
GO
/****** Object:  StoredProcedure [dbo].[GetHomeData]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[GetHomeData]
@shopAppId int
as
begin
	declare @shopAdminId int
	select @shopAdminId=ShopAdminId from C_ShopApp where Id=@shopAppId
	--获取轮播商品
	select ProductId,ImgPath,ShopAdminId from C_BannerIndex  where IsDel=0 and ShopAdminId=@shopAdminId and OptionStatus=1 order by Sorft
	--获取最新推荐商品，根据上架时间，和排序，来取。
	select p.Id,p.ProductName,p.Brand,p.Title,(select ImagePath from C_ProductImg where ProductId=p.Id and IsDel=0 and IsDefault=1) as ImgPath,(select top(1) SallPrice from C_Product_Sku where ProductId=p.Id and IsDel=0) as SallPrice,p.ShopAdminId from C_Product p  where p.ShopAdminId=@shopAdminId and p.OptionStatus=1 and p.IsDel=0 order by p.Id desc,p.Sorft asc
	--获取热门推荐商品，
	select p.Id,p.ProductName,p.Brand,p.Title,(select ImagePath from C_ProductImg where ProductId=p.Id and IsDel=0 and IsDefault=1) as ImgPath,(select top(1) SallPrice from C_Product_Sku where ProductId=p.Id and IsDel=0) as SallPrice,p.ShopAdminId from C_Product p  where p.ShopAdminId=@shopAdminId and p.OptionStatus=1 and p.IsDel=0 and IsHot = 1 order by p.Id desc,p.Sorft asc	
end
GO
/****** Object:  StoredProcedure [dbo].[getcoupon]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[getcoupon] 
@couponId int,
@userId int,
@rtn_err int output
as
begin
	if not exists(select Id from C_Coupon where Id=@couponId and IsDel=0)
	begin
		set @rtn_err = 2--优惠券不存在或已删除
		return 
	end
	declare @totalCount int 
	select @totalCount=TotalCount from C_Coupon where Id=@couponId
	if(@totalCount<=0)
	begin
		set @rtn_err=3--优惠券数量不足
		return 
	end
	if exists(select Id from C_UserCoupon where UserId=@userId and CouponId=@couponId)
	begin
		set @rtn_err=4--该优惠券已领取过了
		return
	end
	--开启事务
	SET XACT_ABORT  ON 
	begin tran tran_create
	insert into C_UserCoupon(CouponId,UserId,IsUsed)values(@couponId,@userId,0)
	update C_Coupon set TotalCount = TotalCount-1 where Id=@couponId
	if(@@error<>0)
		begin
		     rollback tran tran_create
			set @rtn_err =5--执行事务出错
			return
		end	
		else
		begin
			commit tran tran_create
			set @rtn_err = 1
			
			
			return
		end
	

end
GO
/****** Object:  StoredProcedure [dbo].[EditShopRoleAccount]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[EditShopRoleAccount]
@Account varchar(50),
@Id int,
@PassWord varchar(50),
@RoleId int,
@rtn_err int output
as
begin
	if exists(select Id from C_ShopRoleAccount where Account=@Account and IsDel=0 and Id<>@Id)
	begin
		set @rtn_err =2--已存在账号
		return
	end
	if exists(select Id from C_ShopAdmin where LoginName=@Account and OptionStatus>=0)
	begin
		set @rtn_err =2--已存在账号
		return
	end
	update C_ShopRoleAccount set Account=@Account,PassWord=@PassWord,RoleId=@RoleId where Id=@Id
	set @rtn_err=1
	return
end
GO
/****** Object:  StoredProcedure [dbo].[EditShopRole]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[EditShopRole]
@Id int,
@RoleName varchar(50),
@rtn_err int output
as
begin
	if exists(select Id from C_ShopRole where RoleName=@RoleName and IsDel=0 and Id<>@Id)
	begin
		set @rtn_err =2--角色名称已存在
		return
	end
	update C_ShopRole set RoleName=@RoleName where Id=@Id
	set @rtn_err=1
	return
end
GO
/****** Object:  StoredProcedure [dbo].[EditShopApp]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[EditShopApp]
@id int,
@AppId varchar(50),
@AppSecret varchar(50),
@PaymentId varchar(50),
@PaySecret varchar(50),
@ShopAdminId int,
@ServiceQQ varchar(50),
@ServiceWx varchar(50),
@ServicePhone varchar(50),
@ShopName varchar(50),
@PayType int,
@rtn_err int output
as
begin
	if not exists(select Id from C_ShopApp where  Id=@id)
	begin
		set @rtn_err = 4--当前店铺不存在
		return
	end
	if(@AppId<>'')
	begin
		if exists(select Id from C_ShopApp where AppId=@AppId and Id<>@id)
		begin
			set @rtn_err = 2--小程序APPID已存在
			return
		end
	end
	
	if exists(select Id from C_ShopApp where ShopName=@ShopName and ShopAdminId=@ShopAdminId and Id<>@id)
	begin
		set @rtn_err = 3--当前店主账号下已存在店铺名称
		return
	end
	
	update C_ShopApp set AppId=@AppId,AppSecret = @AppSecret,PaymentId=@PaymentId,PaySecret=@PaySecret,UpdateTime=GETDATE(),ServicePhone=@ServicePhone,ServiceQQ=@ServiceQQ,ServiceWx=@ServiceWx,ShopName = @ShopName,PayType=@PayType where Id =@id
	set @rtn_err = 1
	return	
	
end
GO
/****** Object:  StoredProcedure [dbo].[EditMenu]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[EditMenu]
@id int,
@menuName varchar(20),
@sorft int,
@isAll int,
@imgpath varchar(150),
@rtn_err int output
as
begin
	if exists(select Id from C_Menu where IsDel=0 and MenuName=@menuName and Id<>@id)
	begin
		set @rtn_err = 2--已存在同名分类
		return 
	end
	if not exists(select Id from C_Menu where Id=@id)
	begin
		set @rtn_err = 3--分类不存在
		return 
	end
	
	update C_Menu set MenuName=@menuName,Sorft=@sorft,ImgPath=@imgpath,IsAll = @isAll where Id=@id
	set @rtn_err = 1
	return 
end
GO
/****** Object:  StoredProcedure [dbo].[DelShopRole]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DelShopRole]
@Id int,
@rtn_err int output
as
begin
	if exists(select Id from C_ShopRoleAccount where  IsDel=0 and RoleId=@Id)
	begin
		set @rtn_err =2--当前角色正在被员工使用
		return
	end
	update C_ShopRole set IsDel=1 where Id=@Id
	set @rtn_err=1
	return
end
GO
/****** Object:  StoredProcedure [dbo].[DelShippingTemplate]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[DelShippingTemplate]
@id int,
@rtn_err int output
as
begin
	if exists(select Id from C_Product where IsDel=0 and ShippingTemplateId=@id)
	begin
		set @rtn_err = 2--当前模板正在被商品使用
		return
	end
	update C_ShippingTemplates set IsDel=1 where Id= @id
	set @rtn_err=1
	return
end
GO
/****** Object:  StoredProcedure [dbo].[DeleteProduct]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DeleteProduct]
@id int,
@shopAdminId int,
@rtn_err int output
as
begin	
	if not exists(select Id from C_Product where Id = @id and ShopAdminId=@shopAdminId)
	begin
		set @rtn_err  = 3--商品不存在
		return
	end
	
	if exists(select Id from C_Product where Id = @id and ShopAdminId=@shopAdminId and OptionStatus=1)
	begin
		set @rtn_err=4--请先下架该商品
		return
	end	
	
	--删除商品，要判断是否有正在交易的订单，待定
	
	update C_Product set IsDel=1 where Id=@id and ShopAdminId=@shopAdminId
	set @rtn_err = 1
	return 
end
GO
/****** Object:  StoredProcedure [dbo].[DeleteMenu]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[DeleteMenu]
@id int,
@shopAdminId int,
@rtn_err int output
as
begin	
	if not exists(select Id from C_Menu where Id = @id and ShopAdminId=@shopAdminId)
	begin
		set @rtn_err  = 3--分类不存在
		return
	end
	
	if exists(select Id from C_Product where MenuId=@id and IsDel=0 and ShopAdminId=@shopAdminId)
	begin
		set @rtn_err = 2--当前分类下有未删除的商品
		return 
	end
	if exists(select Id from C_SpecialPlace where MenuId=@id and IsDel=1 and ShopAdminId=@shopAdminId)
	begin
		set @rtn_err = 4--当前分类下有正在使用的专场
		return 
	end	
	update C_Menu set IsDel=1 where Id=@id and ShopAdminId=@shopAdminId
	set @rtn_err = 1
	return 
end
GO
/****** Object:  StoredProcedure [dbo].[deal_orders]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[deal_orders]
as
begin

		---1.声明游标
			declare orderID_cursor cursor scroll
			for select ID,ShopAppId,UserId,CouponId from C_Order where OrderStatus=0 and CreateTime < DATEADD(MINUTE,-15,GETDATE())
			--2.打开游标
			open orderID_cursor
			--3.声明游标提取数据所要存放的变量
			declare @OrderId int ,@shopAppId int,@userid int,@couponId int
			--4.定位游标到哪一行
			fetch First from orderID_cursor into @OrderId,@shopAppId,@userid,@couponId --into的变量数量必须与游标查询结果集的列数相同
			while @@fetch_status=0  --提取成功，进行下一条数据的提取操作 
			 begin
				SET XACT_ABORT  ON 
				begin tran tran_rollbackData
			   --业务逻辑
			   --更新订单状态为失败
					update C_Order set OrderStatus=2 where ID = @OrderId AND OrderStatus=0
			   --回滚库存
					update cps set cps.StockNum = cps.StockNum+od.BuyNum  from C_OrderDetails as od,C_Product_Sku as cps where od.Product_SkuId = cps.Id and od.OrderID = @OrderId
			   --回滚销量
					update p set P.SallCount = P.SallCount-cd.BuyNum from C_Product p ,(select ProductId,Sum(BuyNum)as BuyNum from C_OrderDetails where OrderId=@orderid group by ProductId) as cd where p.Id = cd.ProductId
			   				
			   --如果使用优惠券，回滚优惠券的使用状态
					if(@couponId>0)
					begin
						update C_UserCoupon set IsUsed=0 where CouponId=@couponId and UserId = @userid
					end
		
				
		
				if(@@error<>0)
				begin
					insert into C_ZyLog(OrderId,Msg)values(@OrderId,'作业事务失败')
					 rollback tran tran_rollbackData
					
				end	
				else
				begin
					insert into C_ZyLog(OrderId,Msg)values(@OrderId,'作业事务成功')
					commit tran tran_rollbackData
				end
				--移动游标 
				fetch next from orderID_cursor into @OrderId,@shopAppId,@userid,@couponId
			 end  
			 --关闭游标 
				CLOSE orderID_cursor
				--释放资源 
				DEALLOCATE orderID_cursor
 

end
GO
/****** Object:  StoredProcedure [dbo].[CreateRechareOrder]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[CreateRechareOrder]
@shopAdminId int,
@rechargeId int,
@orderno varchar(50),
@rtn_err int output
as
begin
	if not exists(select Id from C_Recharge where Id=@rechargeId)
	begin
		set @rtn_err = 2--充值产品不存在
		return	
	end
	declare @loginName varchar(50)
	declare @amount decimal(18,2)
	declare @title varchar(50)
	declare @apptype int
	declare @salltype int
	declare @accountManagerId int
	select @loginName=LoginName,@accountManagerId=isnull(AccountManagerId,0) from C_ShopAdmin where Id= @shopAdminId
	select @amount=Amount,@title=Title,@apptype=AppType,@salltype=SallType from C_Recharge where Id=@rechargeId
	declare @idnew bigint	
	insert into C_RechargeOrder(ShopAdminId,RechargeId,Amount,Title,AppType,SallType,RechargeNo,OrderStatus,LoginName,AccountManagerId)values(@shopAdminId,@rechargeId,@amount,@title,@apptype,@salltype,@orderno,0,@loginName,@accountManagerId)
	set @idnew= @@identity
	select Id,RechargeNo,Amount from C_RechargeOrder where Id= @idnew	
	set @rtn_err=1
	return		
end
GO
/****** Object:  StoredProcedure [dbo].[create_shop_cashout_order]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[create_shop_cashout_order]
@shopAdminId int,
@orderno varchar(50),
@amount decimal(18,2),
@rtn_err int output
as
begin
	declare @nickName varchar(50)
	declare @openid varchar(50)
	declare @headImgUrl varchar(150)
	declare @canUserAmount decimal(18,2)
	declare @loginName  varchar(50)
	select @nickName =ISNULL(NickName,''),@openid= ISNULL(OpenId,''),@headImgUrl=ISNULL(HeadImgUrl,'') ,@canUserAmount = CanUserAmount,@loginName = LoginName from C_ShopAdmin where Id = @shopAdminId
	
	if(@openid='')
	begin
		set @rtn_err=2--参数异常
		return
	end	
	if(@canUserAmount<@amount)
	begin
		set @rtn_err = 3--余额不足
		return
	end	
	SET XACT_ABORT  ON 
		begin tran tran_transfer
		update C_ShopAdmin set CanUserAmount = CanUserAmount-@amount where Id=@shopAdminId
		insert into C_ShopCashOutOrder(ShopAdminId,NickName,OpenId,HeadImgUrl,OrderNo,Amount,LoginName)values(@shopAdminId,@nickName,@openid,@headImgUrl,@orderno,@amount,@loginName)
		if(@@error<>0)
		begin
		     rollback tran tran_transfer
			set @rtn_err =4--执行出错
			return
		end	
		else
		begin
			commit tran tran_transfer
			select @openid as OpenId,(@canUserAmount-@amount) as ExpressAmount from C_ShopAdmin where Id=@shopAdminId
			set @rtn_err = 1
			return
		end		
end
GO
/****** Object:  StoredProcedure [dbo].[create_manageraccount_cashorder]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[create_manageraccount_cashorder]
@managerAccountId int,
@orderno varchar(50),
@amount decimal(18,2),
@rtn_err int output
as
begin
	declare @accountAmount decimal(18,2)--推广人当前账号金额
	declare @nickName varchar(50)
	declare @openid varchar(50)
	declare @optionStatus int
	select @accountAmount = Amount,@nickName=NickName,@openid= ISNULL(OpenId,''),@optionStatus = OptionStatus  from C_ManagerAccount where Id=@managerAccountId
	if(@optionStatus<>1)
	begin
		set @rtn_err = 2--账号已被冻结
		return
	end	
	if(@accountAmount< @amount)
	begin
		set @rtn_err =3--账户余额不足
		return
	end
	if(@openid='')
	BEGIN
		set @rtn_err=4--参数异常 
		return
	END
	SET XACT_ABORT  ON 
		begin tran tran_create
	
		update C_ManagerAccount set Amount = Amount-@amount where Id=@managerAccountId
		insert into C_AccountMangerOrderLog(AccountManagerId,Amount,OrderNo,NickName)values(@managerAccountId,@amount,@orderno,@nickName)
		
		if(@@error<>0)
		begin
		     rollback tran tran_create
			set @rtn_err =5--执行出错
			return
		end	
		else
		begin
			commit tran tran_create
			set @rtn_err = 1
			select @openid as OpenId,(@accountAmount-@amount) as ExpressAmount from C_ManagerAccount where Id=@managerAccountId
			return
		end
	
end
GO
/****** Object:  StoredProcedure [dbo].[api_getcommentpagedlist]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  proc [dbo].[api_getcommentpagedlist]
@offset int,
@size int,
@productId int
as
begin

		select top(@size) * from
		(select ROW_NUMBER() over(order by c.Id desc)as rownum,
		c.Id,c.CommentLevel,
		c.CommentMsg,
		u.NickName,
		u.HeadImgUrl,
		c.CreateTime
		from C_Comment c with(nolock) 
		left join C_Users u with(nolock)  on c.UserId=u.Id
		where  c.ProductId=@productId and c.IsDel=0   ) tt
		 where tt.rownum>@size*@offset
		 

end
GO
/****** Object:  StoredProcedure [dbo].[AddShopRoleAccount]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AddShopRoleAccount]
@Account varchar(50),
@ShopAdminId int,
@PassWord varchar(50),
@RoleId int,
@rtn_err int output
as
begin
	if exists(select Id from C_ShopRoleAccount where Account=@Account and IsDel=0)
	begin
		set @rtn_err =2--已存在账号
		return
	end
	if exists(select Id from C_ShopAdmin where LoginName=@Account and OptionStatus>=0)
	begin
		set @rtn_err =2--已存在账号
		return
	end
	insert into C_ShopRoleAccount(Account,PassWord,RoleId,ShopAdminId)values(@Account,@PassWord,@RoleId,@ShopAdminId)
	set @rtn_err=1
	return
end
GO
/****** Object:  StoredProcedure [dbo].[AddShopRole]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[AddShopRole]
@RoleName varchar(50),
@ShopAdminId int,
@rtn_err int output
as
begin
	if exists(select Id from C_ShopRole where RoleName=@RoleName and IsDel=0)
	begin
		set @rtn_err =2--角色名称已存在
		return
	end
	insert into C_ShopRole(RoleName,ShopAdminId)values(@RoleName,@ShopAdminId)
	set @rtn_err=1
	return
end
GO
/****** Object:  StoredProcedure [dbo].[AddPropetyValue]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[AddPropetyValue]
@SallPropetyId int,
@PropetyValue varchar(50),
@ProductId int,
@rtn_err int output
as
begin
	if exists(select Id from C_Sall_Propety_Value where ProductId=@ProductId and SallPropetyId=@SallPropetyId and PropetyValue=@PropetyValue)
	begin
		set @rtn_err = 2--当前规格已存在该规格值
		return
	end
	insert into C_Sall_Propety_Value(SallPropetyId,PropetyValue,ProductId)values(@SallPropetyId,@PropetyValue,@ProductId)
	set @rtn_err = 1
	return	
end
GO
/****** Object:  StoredProcedure [dbo].[AddPropety]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[AddPropety]
@PropetyName varchar(50),
@PropetyValue varchar(50),
@ProductId int,
@ShopAdminId int,
@rtn_err int output
as
begin
	declare @count int
	select @count= COUNT(*) from C_Sall_Propety where ProductId=@ProductId and ShopAdminId=@ShopAdminId and IsDel=0
	if(@count>=3)
	begin
		set @rtn_err=2--规格最多三种
		return
	end	
	if exists(select Id from C_Sall_Propety where ProductId=@ProductId and ShopAdminId=@ShopAdminId and IsDel=0 and PropetyName=@PropetyName )
	begin
		set @rtn_err = 3 --已存在规格名称
		return
	end
	--开始事务
	SET XACT_ABORT  ON 
	begin tran tran_add
	
	--由于新加了规格，之前的所有组合都没用了，全部删除
	update C_Product_Sku set IsDel = 0 where ProductId=@ProductId and ShopAdminId = @ShopAdminId
	
	declare @idnew int
	insert into C_Sall_Propety(PropetyName,ShopAdminId,ProductId)values(@PropetyName,@ShopAdminId,@ProductId)
	set @idnew= @@identity 
	insert into C_Sall_Propety_Value(SallPropetyId,PropetyValue,ProductId)values(@idnew,@PropetyValue,@ProductId)
		
	if(@@error<>0)
	begin
		 rollback tran tran_add
		 set @rtn_err =4--事务出错
		 return
	end	
	else
	begin
		commit tran tran_add
		set @rtn_err = 1
		return
	end
	
end
GO
/****** Object:  StoredProcedure [dbo].[add_shopcart]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[add_shopcart]
@userid int,
@productId int,
@product_skuId int,
@saleNum int,
@rtn_err int output
as
begin
	if not exists(select Id from C_Users where Id=@userid and OptionStatus=0)
	BEGIN
		set @rtn_err=2 --用户不存在或用户异常
		return
	END
	if not exists(select Id from C_Product where Id=@productID and OptionStatus=1 and IsDel=0)
	BEGIN
		set @rtn_err=3 --商品不存在或商品已下架
		return
	END
	if not exists(select Id from C_Product_Sku where Id=@product_skuId and IsDel=0)
	BEGIN
		set @rtn_err=4 --该规格商品不存在或已下架
		return
	END 
	declare @stockNum int
	select @stockNum=StockNum from C_Product_Sku where Id=@product_skuId
	if(@stockNum<@saleNum)
	begin
		set @rtn_err=5 --库存不足
		return
	end
	--验证当前用户当前购物车是否存在当前要加入的版本，如果存在，更改数量
	if exists(select Id from C_ShoppingCart where UserId=@userid and ProductId=@productId and Product_SkuId=@product_skuId and IsDel=0)
	begin
		--存在
		update C_ShoppingCart set SaleNum = SaleNum+@saleNum where UserId=@userid and ProductId=@productId and Product_SkuId=@product_skuId and IsDel=0
	end	
	else
	begin
		insert into C_ShoppingCart(UserId,ProductId,Product_SkuId,SaleNum)values(@userid,@productId,@product_skuId,@saleNum)
	end
	set @rtn_err=1
	return
end
GO
/****** Object:  StoredProcedure [dbo].[add_consignee]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[add_consignee]
@UserId int,
@ConsigneeName varchar(50),
@ConsigneePhone varchar(20),
@ZipCode varchar(50),
@ProvinceCode varchar(10),
@CityCode varchar(10),
@AreaCode varchar(10),
@Address varchar(100),
@rtn_err int output
as
begin
	if not exists(select Id from C_Users where Id=@UserId and OptionStatus=0)
	begin
		set @rtn_err = 2--用户异常
		return
	end
	--添加地址，如果这是用户得第一个地址，自动设为默认
	declare @isDefault int
	if not exists(select Id from C_Consignee where UserId=@UserId and IsDel=0)
	begin
		set @isDefault=1
	end
	else
	begin
		set @isDefault=0
	end
	insert into C_Consignee(UserId,ZipCode,ProvinceCode,CityCode,AreaCode,IsDefault,ConsigneeName,ConsigneePhone,Address)values(@UserId,@ZipCode,@ProvinceCode,@CityCode,@AreaCode,@isDefault,@ConsigneeName,@ConsigneePhone,@Address)
	set @rtn_err =1
	return 	
end
GO
/****** Object:  StoredProcedure [dbo].[add_comment]    Script Date: 11/15/2021 16:27:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[add_comment]
@OrderId int,
@Spid int, 
@UserId int,
@Said int,
@CommentLevel int,
@CommentMsg varchar(1000),
@rtn_err int output
as	
begin
	if not exists(select Id from C_Users where Id=@UserId and ShopAdminId=@Said and ShopAppId = @Spid)
	begin
		set @rtn_err = 2--用户信息不匹配
		return
	end	
	if exists(select Id from C_Comment where UserId=@UserId and OrderId=@OrderId and ShopAppId=@Spid)
	begin
		set @rtn_err =3--该订单已经评论过了
		return
	end
	if not exists(select Id from C_Order where Id=@OrderId and UserId=@UserId and ShopAppId=@Spid and OrderStatus=1)
	begin
		set @rtn_err = 4--订单信息错误
		return
	end	
	declare @loginName varchar(50)
	select @loginName = LoginName from C_Users where Id=@UserId
	--因为订单是多商品可能，所以可能是多条评价
	Insert into C_Comment(UserId,ShopAppId,ShopAdminId,LoginName,ProductId,CommentLevel,CommentMsg,OrderId) select @UserId as UserId,@Spid as ShopAppId,@Said as ShopAdminId, @loginName as LoginName, ProductId, @CommentLevel as CommentLevel,@CommentMsg as CommentMsg,@OrderId as OrderId from C_OrderDetails where OrderId=@OrderId
	set @rtn_err = 1
	return	
end
GO
/****** Object:  Default [DF_C_AccountMangerOrderLog_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_AccountMangerOrderLog] ADD  CONSTRAINT [DF_C_AccountMangerOrderLog_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_AccountMangerOrderLog_OrderStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_AccountMangerOrderLog] ADD  CONSTRAINT [DF_C_AccountMangerOrderLog_OrderStatus]  DEFAULT ((0)) FOR [OrderStatus]
GO
/****** Object:  Default [DF_C_BannerIndex_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_BannerIndex] ADD  CONSTRAINT [DF_C_BannerIndex_OptionStatus]  DEFAULT ((1)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_BannerIndex_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_BannerIndex] ADD  CONSTRAINT [DF_C_BannerIndex_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_BannerIndex_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_BannerIndex] ADD  CONSTRAINT [DF_C_BannerIndex_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Comment_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Comment] ADD  CONSTRAINT [DF_C_Comment_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Comment_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Comment] ADD  CONSTRAINT [DF_C_Comment_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_Comment_IsEdit]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Comment] ADD  CONSTRAINT [DF_C_Comment_IsEdit]  DEFAULT ((0)) FOR [IsEdit]
GO
/****** Object:  Default [DF_C_Consignee_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Consignee] ADD  CONSTRAINT [DF_C_Consignee_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_Consignee_IsDefault]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Consignee] ADD  CONSTRAINT [DF_C_Consignee_IsDefault]  DEFAULT ((0)) FOR [IsDefault]
GO
/****** Object:  Default [DF_C_Consignee_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Consignee] ADD  CONSTRAINT [DF_C_Consignee_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Coupon_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Coupon] ADD  CONSTRAINT [DF_C_Coupon_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Coupon_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Coupon] ADD  CONSTRAINT [DF_C_Coupon_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_Decorate_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Decorate] ADD  CONSTRAINT [DF_C_Decorate_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_FullAmountSet_Amount]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_FullAmountSet] ADD  CONSTRAINT [DF_C_FullAmountSet_Amount]  DEFAULT ((0)) FOR [Amount]
GO
/****** Object:  Default [DF_C_FullAmountSet_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_FullAmountSet] ADD  CONSTRAINT [DF_C_FullAmountSet_OptionStatus]  DEFAULT ((0)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_FullAmountSet_NotHasProductIds]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_FullAmountSet] ADD  CONSTRAINT [DF_C_FullAmountSet_NotHasProductIds]  DEFAULT ('999999999') FOR [NotHasProductIds]
GO
/****** Object:  Default [DF_C_FullAmountSet_NotHasProvinceIds]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_FullAmountSet] ADD  CONSTRAINT [DF_C_FullAmountSet_NotHasProvinceIds]  DEFAULT ('999999999') FOR [NotHasProvinceIds]
GO
/****** Object:  Default [DF_C_ManagerAccount_Amount]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ManagerAccount] ADD  CONSTRAINT [DF_C_ManagerAccount_Amount]  DEFAULT ((0)) FOR [Amount]
GO
/****** Object:  Default [DF_C_ManagerAccount_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ManagerAccount] ADD  CONSTRAINT [DF_C_ManagerAccount_OptionStatus]  DEFAULT ((1)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_ManagerAccount_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ManagerAccount] ADD  CONSTRAINT [DF_C_ManagerAccount_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Menu_Sorft]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Menu] ADD  CONSTRAINT [DF_C_Menu_Sorft]  DEFAULT ((10)) FOR [Sorft]
GO
/****** Object:  Default [DF_C_Menu_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Menu] ADD  CONSTRAINT [DF_C_Menu_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Menu_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Menu] ADD  CONSTRAINT [DF_C_Menu_OptionStatus]  DEFAULT ((1)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_Menu_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Menu] ADD  CONSTRAINT [DF_C_Menu_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_Order_OrderStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Order] ADD  CONSTRAINT [DF_C_Order_OrderStatus]  DEFAULT ((0)) FOR [OrderStatus]
GO
/****** Object:  Default [DF_C_Order_PayStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Order] ADD  CONSTRAINT [DF_C_Order_PayStatus]  DEFAULT ((0)) FOR [PayStatus]
GO
/****** Object:  Default [DF_C_Order_SendStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Order] ADD  CONSTRAINT [DF_C_Order_SendStatus]  DEFAULT ((0)) FOR [SendStatus]
GO
/****** Object:  Default [DF_C_Order_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Order] ADD  CONSTRAINT [DF_C_Order_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Order_ServiceStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Order] ADD  CONSTRAINT [DF_C_Order_ServiceStatus]  DEFAULT ((0)) FOR [ServiceStatus]
GO
/****** Object:  Default [DF_C_Order_CouponId]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Order] ADD  CONSTRAINT [DF_C_Order_CouponId]  DEFAULT ((0)) FOR [CouponId]
GO
/****** Object:  Default [DF_C_Order_CouponDelAmount]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Order] ADD  CONSTRAINT [DF_C_Order_CouponDelAmount]  DEFAULT ((0)) FOR [CouponDelAmount]
GO
/****** Object:  Default [DF_C_Order_ShippingAmount]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Order] ADD  CONSTRAINT [DF_C_Order_ShippingAmount]  DEFAULT ((0)) FOR [ShippingAmount]
GO
/****** Object:  Default [DF_C_Order_PayType]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Order] ADD  CONSTRAINT [DF_C_Order_PayType]  DEFAULT ((2)) FOR [PayType]
GO
/****** Object:  Default [DF_C_OrderDetails_Points]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_OrderDetails] ADD  CONSTRAINT [DF_C_OrderDetails_Points]  DEFAULT ((0)) FOR [Points]
GO
/****** Object:  Default [DF_C_OrderDetails_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_OrderDetails] ADD  CONSTRAINT [DF_C_OrderDetails_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Product_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product] ADD  CONSTRAINT [DF_C_Product_OptionStatus]  DEFAULT ((0)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_Product_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product] ADD  CONSTRAINT [DF_C_Product_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Product_Sorft]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product] ADD  CONSTRAINT [DF_C_Product_Sorft]  DEFAULT ((100)) FOR [Sorft]
GO
/****** Object:  Default [DF_C_Product_InitSallNum]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product] ADD  CONSTRAINT [DF_C_Product_InitSallNum]  DEFAULT ((0)) FOR [InitSallNum]
GO
/****** Object:  Default [DF_C_Product_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product] ADD  CONSTRAINT [DF_C_Product_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_Product_CurrentSkuType]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product] ADD  CONSTRAINT [DF_C_Product_CurrentSkuType]  DEFAULT ((1)) FOR [CurrentSkuType]
GO
/****** Object:  Default [DF_C_Product_IsHot]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product] ADD  CONSTRAINT [DF_C_Product_IsHot]  DEFAULT ((0)) FOR [IsHot]
GO
/****** Object:  Default [DF_C_Product_SallCount]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product] ADD  CONSTRAINT [DF_C_Product_SallCount]  DEFAULT ((0)) FOR [SallCount]
GO
/****** Object:  Default [DF_C_Product_Sku_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product_Sku] ADD  CONSTRAINT [DF_C_Product_Sku_OptionStatus]  DEFAULT ((1)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_Product_Sku_StockNum]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product_Sku] ADD  CONSTRAINT [DF_C_Product_Sku_StockNum]  DEFAULT ((0)) FOR [StockNum]
GO
/****** Object:  Default [DF_C_Product_Sku_LinePrice]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product_Sku] ADD  CONSTRAINT [DF_C_Product_Sku_LinePrice]  DEFAULT ((0.00)) FOR [LinePrice]
GO
/****** Object:  Default [DF_C_Product_Sku_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Product_Sku] ADD  CONSTRAINT [DF_C_Product_Sku_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_ProductImg_Sorft]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ProductImg] ADD  CONSTRAINT [DF_C_ProductImg_Sorft]  DEFAULT ((0)) FOR [Sorft]
GO
/****** Object:  Default [DF_C_ProductImg_IsDefault]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ProductImg] ADD  CONSTRAINT [DF_C_ProductImg_IsDefault]  DEFAULT ((0)) FOR [IsDefault]
GO
/****** Object:  Default [DF_C_ProductImg_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ProductImg] ADD  CONSTRAINT [DF_C_ProductImg_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_Question_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Question] ADD  CONSTRAINT [DF_C_Question_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_RechargeOrder_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_RechargeOrder] ADD  CONSTRAINT [DF_C_RechargeOrder_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_RechargeOrder_OrderStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_RechargeOrder] ADD  CONSTRAINT [DF_C_RechargeOrder_OrderStatus]  DEFAULT ((0)) FOR [OrderStatus]
GO
/****** Object:  Default [DF_C_RechargeOrder_AccountManager]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_RechargeOrder] ADD  CONSTRAINT [DF_C_RechargeOrder_AccountManager]  DEFAULT ((0)) FOR [AccountManagerId]
GO
/****** Object:  Default [DF_C_RechargeOrder_AccountMangerAmount]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_RechargeOrder] ADD  CONSTRAINT [DF_C_RechargeOrder_AccountMangerAmount]  DEFAULT ((0)) FOR [AccountMangerAmount]
GO
/****** Object:  Default [DF_C_Sall_Propety_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Sall_Propety] ADD  CONSTRAINT [DF_C_Sall_Propety_OptionStatus]  DEFAULT ((1)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_Sall_Propety_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Sall_Propety] ADD  CONSTRAINT [DF_C_Sall_Propety_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_Sall_Propety_Value_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Sall_Propety_Value] ADD  CONSTRAINT [DF_C_Sall_Propety_Value_OptionStatus]  DEFAULT ((1)) FOR [OptionStatus]
GO
/****** Object:  Default [DF__C_Sall_Pr__IsDel__1DE57479]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Sall_Propety_Value] ADD  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_SearchWord_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_SearchWord] ADD  CONSTRAINT [DF_C_SearchWord_OptionStatus]  DEFAULT ((1)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_SearchWord_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_SearchWord] ADD  CONSTRAINT [DF_C_SearchWord_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_SearchWord_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_SearchWord] ADD  CONSTRAINT [DF_C_SearchWord_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_ShippingTemplateItem_FirstAmount]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShippingTemplateItem] ADD  CONSTRAINT [DF_C_ShippingTemplateItem_FirstAmount]  DEFAULT ((0)) FOR [FirstAmount]
GO
/****** Object:  Default [DF_C_ShippingTemplateItem_NextValue]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShippingTemplateItem] ADD  CONSTRAINT [DF_C_ShippingTemplateItem_NextValue]  DEFAULT ((0)) FOR [NextValue]
GO
/****** Object:  Default [DF_C_ShippingTemplateItem_NextAmount]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShippingTemplateItem] ADD  CONSTRAINT [DF_C_ShippingTemplateItem_NextAmount]  DEFAULT ((0)) FOR [NextAmount]
GO
/****** Object:  Default [DF_C_ShippingTemplateItem_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShippingTemplateItem] ADD  CONSTRAINT [DF_C_ShippingTemplateItem_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_ShippingTemplateItem_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShippingTemplateItem] ADD  CONSTRAINT [DF_C_ShippingTemplateItem_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_ShippingTemplates_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShippingTemplates] ADD  CONSTRAINT [DF_C_ShippingTemplates_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_ShippingTemplates_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShippingTemplates] ADD  CONSTRAINT [DF_C_ShippingTemplates_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_Table_1_ShopType]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopAdmin] ADD  CONSTRAINT [DF_Table_1_ShopType]  DEFAULT ((1)) FOR [PayType]
GO
/****** Object:  Default [DF_C_Shop_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopAdmin] ADD  CONSTRAINT [DF_C_Shop_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Shop_AccountManagerId]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopAdmin] ADD  CONSTRAINT [DF_C_Shop_AccountManagerId]  DEFAULT ((0)) FOR [AccountManagerId]
GO
/****** Object:  Default [DF_C_ShopAdmin_CanUserAmount]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopAdmin] ADD  CONSTRAINT [DF_C_ShopAdmin_CanUserAmount]  DEFAULT ((0)) FOR [CanUserAmount]
GO
/****** Object:  Default [DF_C_ShopExtend_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopApp] ADD  CONSTRAINT [DF_C_ShopExtend_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_ShopApp_AppType]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopApp] ADD  CONSTRAINT [DF_C_ShopApp_AppType]  DEFAULT ((1)) FOR [AppType]
GO
/****** Object:  Default [DF_C_ShopApp_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopApp] ADD  CONSTRAINT [DF_C_ShopApp_OptionStatus]  DEFAULT ((1)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_ShopApp_PayType]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopApp] ADD  CONSTRAINT [DF_C_ShopApp_PayType]  DEFAULT ((1)) FOR [PayType]
GO
/****** Object:  Default [DF_C_ShopCashOutOrder_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopCashOutOrder] ADD  CONSTRAINT [DF_C_ShopCashOutOrder_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_ShopCashOutOrder_OrderStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopCashOutOrder] ADD  CONSTRAINT [DF_C_ShopCashOutOrder_OrderStatus]  DEFAULT ((0)) FOR [OrderStatus]
GO
/****** Object:  Default [DF_C_ShoppingCart_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShoppingCart] ADD  CONSTRAINT [DF_C_ShoppingCart_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_ShoppingCart_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShoppingCart] ADD  CONSTRAINT [DF_C_ShoppingCart_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Role_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopRole] ADD  CONSTRAINT [DF_C_Role_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_ShopRoleAccount_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopRoleAccount] ADD  CONSTRAINT [DF_C_ShopRoleAccount_OptionStatus]  DEFAULT ((1)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_ShopRoleAccount_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopRoleAccount] ADD  CONSTRAINT [DF_C_ShopRoleAccount_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_ShopRoleAccount_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_ShopRoleAccount] ADD  CONSTRAINT [DF_C_ShopRoleAccount_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_SpecialPlace_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_SpecialPlace] ADD  CONSTRAINT [DF_C_SpecialPlace_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_SpecialPlace_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_SpecialPlace] ADD  CONSTRAINT [DF_C_SpecialPlace_OptionStatus]  DEFAULT ((1)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_SpecialPlace_IsDel]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_SpecialPlace] ADD  CONSTRAINT [DF_C_SpecialPlace_IsDel]  DEFAULT ((0)) FOR [IsDel]
GO
/****** Object:  Default [DF_C_UserCoupon_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_UserCoupon] ADD  CONSTRAINT [DF_C_UserCoupon_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_UserCoupon_IsUsed]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_UserCoupon] ADD  CONSTRAINT [DF_C_UserCoupon_IsUsed]  DEFAULT ((0)) FOR [IsUsed]
GO
/****** Object:  Default [DF_C_Users_Points]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Users] ADD  CONSTRAINT [DF_C_Users_Points]  DEFAULT ((0)) FOR [Points]
GO
/****** Object:  Default [DF_C_Users_CreateTime]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Users] ADD  CONSTRAINT [DF_C_Users_CreateTime]  DEFAULT (getdate()) FOR [CreateTime]
GO
/****** Object:  Default [DF_C_Users_OptionStatus]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Users] ADD  CONSTRAINT [DF_C_Users_OptionStatus]  DEFAULT ((0)) FOR [OptionStatus]
GO
/****** Object:  Default [DF_C_Users_LoginType]    Script Date: 11/15/2021 16:27:45 ******/
ALTER TABLE [dbo].[C_Users] ADD  CONSTRAINT [DF_C_Users_LoginType]  DEFAULT ((1)) FOR [H5UserType]
GO
