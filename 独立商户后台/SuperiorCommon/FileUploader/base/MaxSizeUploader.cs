﻿using System.IO;
using System.Linq;

namespace SuperiorCommon
{
    /// <summary>
    /// 限制最大size的Uploader
    /// </summary>
    public abstract class MaxSizeUploader : FileUploaderBase
    {
        public override bool Validate(string fileName, Stream stream, out string reason)
        {
            reason = string.Empty;

            if (!AllowedImageTypes.Any(i => i == Path.GetExtension(fileName).ToLower()))
            {
                reason = string.Format("上传失败！请选择{0}类型的文件", string.Join(",", AllowedImageTypes));
                return false;
            }

            if (stream.Length > MaxBytesLength)
            {
                reason = string.Format("上传失败！图片大小必须小于{0}k", MaxBytesLength / 1024);
                return false;
            }
            return true;
        }
    }
}
