﻿using SuperiorCommon;
using SuperiorModel;
using System;
using System.Web.Mvc;

namespace SuperiorShopkeeper
{
    /// <summary>
    /// 权限验证过滤器
    /// </summary>
    [AttributeUsage(AttributeTargets.All, Inherited = true, AllowMultiple = true)]
    public class ShopAuthorityAttribute: ActionFilterAttribute
    {
        private object e = null;
        public ShopAuthorityAttribute(AuthorityModuleEnum e)
        {
            this.e = e;
        }
        public ShopAuthorityAttribute(AuthorityMenuEnum e)
        {
            this.e = e;
        }
        public ShopAuthorityAttribute(AuthorityOptionEnum e)
        {
            this.e = e;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            if (!AuthorityController.IsHasAuthority(this.e))
            {
                filterContext.Result = new JsonResult
                {
                    Data = new ResultModel(false, "暂无权限"),
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };
                
                return;
            }

        }
    }
}