﻿using SuperiorCommon;
using SuperiorModel;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SuperiorShopkeeper
{
    public class AuthorityController
    {
        /// <summary>
        /// 获取所有权限字典
        /// </summary>\
        /// 
        private static List<AuthorityModel> _InitAuthorityList = null;
        public static List<AuthorityModel> InitAuthorityList
        {
            get
            {
                if (_InitAuthorityList != null)
                    return _InitAuthorityList;
                var res = new List<AuthorityModel>();
                //系统模块权限集合初始化
                var moduleDic = ToolManager.ForEnum<AuthorityModuleEnum>();
                foreach (var item in moduleDic)
                {
                    var model = new AuthorityModel();
                    model.id = item.Value;
                    model.parentId = 0;
                    model.title = item.Key;
                    model.checkArr.Add(new AuthorityCheckItem() {
                        type="0",
                        isChecked="1"
                    });
                    res.Add(model);
                
                }
                //菜单模块权限集合初始化]
                var menueDic = ToolManager.ForEnum<AuthorityMenuEnum>();
                foreach (var item in menueDic)
                {
                    switch (item.Value)
                    {
                        case (int)AuthorityMenuEnum.RoleList:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value,item.Key, (int)AuthorityModuleEnum.SytemManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.RoleSee, ToolManager.GetDescription(AuthorityOptionEnum.RoleSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddRole, ToolManager.GetDescription(AuthorityOptionEnum.AddRole), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.EditRole, ToolManager.GetDescription(AuthorityOptionEnum.EditRole), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DeleteRole, ToolManager.GetDescription(AuthorityOptionEnum.DeleteRole), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.SetRoleAuthority, ToolManager.GetDescription(AuthorityOptionEnum.SetRoleAuthority), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            break;
                        case (int)AuthorityMenuEnum.ShopAccountList:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.SytemManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.RoleAccountSee, ToolManager.GetDescription(AuthorityOptionEnum.RoleAccountSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddOrEditRoleAccount, ToolManager.GetDescription(AuthorityOptionEnum.AddOrEditRoleAccount), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.OptionAccount, ToolManager.GetDescription(AuthorityOptionEnum.OptionAccount), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DeleteRoleAccount, ToolManager.GetDescription(AuthorityOptionEnum.DeleteRoleAccount), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            break;
                        case (int)AuthorityMenuEnum.ShopAdmin:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.SytemManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点 
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.ShopAdminInfoSee, ToolManager.GetDescription(AuthorityOptionEnum.ShopAdminInfoSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.EditShopAdminInfo, ToolManager.GetDescription(AuthorityOptionEnum.EditShopAdminInfo), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                          
                            break;
                        case (int)AuthorityMenuEnum.ShopAppList:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.MallManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.ShopAppSee, ToolManager.GetDescription(AuthorityOptionEnum.ShopAppSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddShopApp, ToolManager.GetDescription(AuthorityOptionEnum.AddShopApp), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.EditShopApp, ToolManager.GetDescription(AuthorityOptionEnum.EditShopApp), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));

                            break;
                        case (int)AuthorityMenuEnum.ProductMenu:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.ProductManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.MenuSee, ToolManager.GetDescription(AuthorityOptionEnum.MenuSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddMenu, ToolManager.GetDescription(AuthorityOptionEnum.AddMenu), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.EditMenu, ToolManager.GetDescription(AuthorityOptionEnum.EditMenu), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.OptionMenu, ToolManager.GetDescription(AuthorityOptionEnum.OptionMenu), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DeleteMenu, ToolManager.GetDescription(AuthorityOptionEnum.DeleteMenu), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            break;
                        case (int)AuthorityMenuEnum.ProductList:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.ProductManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.ProductSee, ToolManager.GetDescription(AuthorityOptionEnum.ProductSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.SearchList, ToolManager.GetDescription(AuthorityOptionEnum.SearchList), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddProduct, ToolManager.GetDescription(AuthorityOptionEnum.AddProduct), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DeleteProduct, ToolManager.GetDescription(AuthorityOptionEnum.DeleteProduct), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.OptionProduct, ToolManager.GetDescription(AuthorityOptionEnum.OptionProduct), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.ProductEdit, ToolManager.GetDescription(AuthorityOptionEnum.ProductEdit), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.EditSingleSku, ToolManager.GetDescription(AuthorityOptionEnum.EditSingleSku), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.EditMoreSku, ToolManager.GetDescription(AuthorityOptionEnum.EditMoreSku), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddPropety, ToolManager.GetDescription(AuthorityOptionEnum.AddPropety), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddPropetyValue, ToolManager.GetDescription(AuthorityOptionEnum.AddPropetyValue), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DelPropety, ToolManager.GetDescription(AuthorityOptionEnum.DelPropety), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DelPropetyValue, ToolManager.GetDescription(AuthorityOptionEnum.DelPropetyValue), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            break;
                        case (int)AuthorityMenuEnum.ProductBannerList:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.ProductManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.BannerSee, ToolManager.GetDescription(AuthorityOptionEnum.BannerSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddBanner, ToolManager.GetDescription(AuthorityOptionEnum.AddBanner), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.EditBanner, ToolManager.GetDescription(AuthorityOptionEnum.EditBanner), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.OptionBanner, ToolManager.GetDescription(AuthorityOptionEnum.OptionBanner), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DelBanner, ToolManager.GetDescription(AuthorityOptionEnum.DelBanner), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            break;
                        case (int)AuthorityMenuEnum.UserList:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.UserManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.UserSee, ToolManager.GetDescription(AuthorityOptionEnum.UserSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.UserList, ToolManager.GetDescription(AuthorityOptionEnum.UserList), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.OptionUser, ToolManager.GetDescription(AuthorityOptionEnum.OptionUser), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                           
                            break;
                        case (int)AuthorityMenuEnum.OrderList:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.OrderManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.OrderSee, ToolManager.GetDescription(AuthorityOptionEnum.OrderSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.OrderList, ToolManager.GetDescription(AuthorityOptionEnum.OrderList), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.SendProduct, ToolManager.GetDescription(AuthorityOptionEnum.SendProduct), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.FinishService, ToolManager.GetDescription(AuthorityOptionEnum.FinishService), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            break;
                        case (int)AuthorityMenuEnum.FullAmountSendSet:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.MarketingManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.SetFullSendSee, ToolManager.GetDescription(AuthorityOptionEnum.SetFullSendSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.SetNotPids, ToolManager.GetDescription(AuthorityOptionEnum.SetNotPids), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.SetNotAreas, ToolManager.GetDescription(AuthorityOptionEnum.SetNotAreas), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.SetFullSendModel, ToolManager.GetDescription(AuthorityOptionEnum.SetFullSendModel), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            break;
                        case (int)AuthorityMenuEnum.CouponList:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.MarketingManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.CouponSee, ToolManager.GetDescription(AuthorityOptionEnum.CouponSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddCoupon, ToolManager.GetDescription(AuthorityOptionEnum.AddCoupon), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DelCoupon, ToolManager.GetDescription(AuthorityOptionEnum.DelCoupon), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                           
                            break;
                        case (int)AuthorityMenuEnum.UserCouponList:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.MarketingManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.UserCouponSee, ToolManager.GetDescription(AuthorityOptionEnum.UserCouponSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.UserCouponList, ToolManager.GetDescription(AuthorityOptionEnum.UserCouponList), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                           

                            break;
                        case (int)AuthorityMenuEnum.SpecialPlace:
                             //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.ProductManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.SpecialPlaceSee, ToolManager.GetDescription(AuthorityOptionEnum.SpecialPlaceSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.OptionSpecialPlace, ToolManager.GetDescription(AuthorityOptionEnum.OptionSpecialPlace), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DeleteSepecial, ToolManager.GetDescription(AuthorityOptionEnum.DeleteSepecial), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            break;
                        case (int)AuthorityMenuEnum.SearchWord:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.ProductManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.SearchWordSee, ToolManager.GetDescription(AuthorityOptionEnum.SearchWordSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddSearchWord, ToolManager.GetDescription(AuthorityOptionEnum.AddSearchWord), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DelSearchWord, ToolManager.GetDescription(AuthorityOptionEnum.DelSearchWord), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.OptionSearchWord, ToolManager.GetDescription(AuthorityOptionEnum.OptionSearchWord), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            break;
                        case (int)AuthorityMenuEnum.ShippingTemplateList:
                            //添加系统模块的子节点
                            res.Add(new AuthorityModel(item.Value, item.Key, (int)AuthorityModuleEnum.SetManager, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            //添加当前菜单模块的多个操作子节点  
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.TemplateSee, ToolManager.GetDescription(AuthorityOptionEnum.TemplateSee), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.AddTemplate, ToolManager.GetDescription(AuthorityOptionEnum.AddTemplate), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.DelTemplate, ToolManager.GetDescription(AuthorityOptionEnum.DelTemplate), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            res.Add(new AuthorityModel((int)AuthorityOptionEnum.EditTemplate, ToolManager.GetDescription(AuthorityOptionEnum.EditTemplate), item.Value, new AuthorityCheckItem() { type = "0", isChecked = "1" }));
                            break;
                    }
                }
                _InitAuthorityList = res;
                return res;
            }
        }
        /// <summary>
        /// 判断当前账户是否有某项系统模块权限
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        public static bool IsHasAuthority<T>(T e)
        {
            //如果是后台主账户，直接返回true
            if (UserContext.User.ShopAdminType == 1)
            {
                return true;
            }
            else
            {
                var model= UserContext.User.ShopLoginRoleModel.AuthoritysList.Where(p => p.id == Convert.ToInt32(e)).FirstOrDefault();
                if (model == null)
                    return false;//可能是新家的功能，还没有投权限。
                else
                    return model.checkArr[0].isChecked == "1";
            }

        }
    }
}