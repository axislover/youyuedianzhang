﻿(function () {
    LoginClass = {};
    var _type = 1;
    LoginClass.Instance = {
        Init: function () {
            //检测URL，是否是因为登陆过期跳转过来
            if (ToolManager.Common.UrlParms("notice") == "timeout")
                layer.alert("登录已失效，请重新登录");
            //读取用户COOKIE
            if ($.cookie("ISREMANBER") == "1") {
                $("#LAY-user-login-username").val($.cookie("PHONENUMBER"));
                $("#LAY-user-login-password").val($.cookie("TIANHONGPSD") == "null" ? "" : $.cookie("TIANHONGPSD"));
                $("#remanber").attr("checked", true);
            } else if ($.cookie("ISREMANBER") == "0") {
                $("#remanber").attr("checked", false);
            }

            $(document).on("click", "#captchaimg", this.CaptchaImage);
            $(document).on('click', '#btn_getty',this.GetTy);
            //layui验证表单写法-----begin
            var form;
            layui.use(['form'], function () {
                form = layui.form;
                form.on('submit(LAY-user-login-submit)', LoginClass.Instance.Login);
                //回车提交
                $(document).on('keypress', '#LAY-user-login input', function (e) {
                    if (e.which == 13) {
                        LoginClass.Instance.Login();
                    }
                })

            });
            //-----end

        },
        
        CaptchaImage: function () {
            $(this).attr("src", $(this).attr("imgsrc") + "?a=" + Math.random());
        },

        Login: function () {
            var username = $("#LAY-user-login-username").val();
            var password = $("#LAY-user-login-password").val();
            var code = $("#newcaptcha").val();
            var _url = "";
            $("input[name=text_loginType]").each(function (index, item) {
                if (item.checked)
                    _type = item.value;
            });
            if (_type == "1") {
                _url = "/Login/Login";
            } else if (_type == "2") {
                _url = "/Login/Login_Role";
            }
           
            var index = layer.load(1);
            RequestManager.Ajax.Post(_url, { "UserName": username, "PassWord": password, "Code": code }, true, function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    LoginClass.Instance.ControlCookie();
                    window.location.href = "/Home/Index";
                } else {
                    layer.alert(data.Message);
                }
            })
        },
        ControlCookie: function () {
            if ($("#remanber").prop("checked")) {
                $.cookie('PHONENUMBER', $("#LAY-user-login-username").val(), { expires: 90 });
                $.cookie('TIANHONGPSD', $("#LAY-user-login-password").val(), { expires: 90 });
                $.cookie('ISREMANBER', "1", { expires: 90 });
            } else {
                $.cookie('PHONENUMBER', null);
                $.cookie('TIANHONGPSD', null);
                $.cookie('ISREMANBER', "0", { expires: 90 });
            }
        },

    };
})()