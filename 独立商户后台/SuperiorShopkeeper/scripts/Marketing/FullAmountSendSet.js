﻿(function () {
    FullAmountSendSetClass = {};

    FullAmountSendSetClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
                //自定义验证规则
                form.verify({
                    //验证大于等于0的规则
                    greaterThanZero: function (value, item) {
                        var r = parseFloat(value);
                        if (r < 0)
                            return "必须大于等于0";
                    },
                });
                //验证提交
                form.on('submit(add)', FullAmountSendSetClass.Instance.Submit);
            });
            $(document).on('click', '#btn_choseProudct', this.ChoseProducts);
            $(document).on('click', '#btn_choseAreas', this.ChoseAreas);
        },
        Submit: function () {
            var model = {};
            model.Amount = $.trim($("#text_amount").val());
            var _optionStatus;
            $("input[name=text_status]").each(function (index, item) {
                if (item.checked)
                    _optionStatus = item.value;
            });
            //$("#input[name=text_status]:checked").val();草他妹的非得用layui的方法来取值
            model.OptionStatus = _optionStatus;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Marketing/SetFullSendModel", model, true, function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("设置成功");
                } else {
                    layer.alert(data.Message);
                }
            })
        },
        ChoseProducts: function () {
            var iframeIndex = layer.open({
                type: 2,
                area: ['700px', '450px'],
                fixed: false, //不固定
                maxmin: true,
                btn: ['确认选择'],
                yes: function (index, layero) {
                    var arr = [];
                    var imgArr = [];
                    var iframeid = "layui-layer-iframe" + iframeIndex; //获取iframe分配的ID
                    //获取子页面所有勾选框
                    var contens = $("#" + iframeid + "").contents();
                    var checkProducts = $("#" + iframeid + "").contents().find("input[name=check_product]");
                    checkProducts.each(function (index, item) {
                        if (item.checked) {
                            arr.push(item.value);
                            imgArr.push(contens.find("#img_" + item.value + "").attr("src"));
                        }
                    });
                    if (arr.length == 0)
                        arr.push('999999999');//防止in查询报错
                    var index = layer.load(1);
                    RequestManager.Ajax.Post("/Marketing/SetNotPids", {
                        "ids": arr.join(',')
                    }, true, function (data) {
                        layer.close(index);
                        if (data.IsSuccess) {
                            var htmlArr = [];
                            for (var i = 0; i < imgArr.length; i++) {
                                htmlArr.push('<div><img src="' + imgArr[i] + '" /></div>');
                            }
                            $(".layui-upload-productlist").html(htmlArr.join(''));
                            layer.closeAll('iframe');


                        } else {
                            layer.alert(data.Message);
                        }
                    })
                },
                content: '/Marketing/CurrentProducts'
            });
        },
        ChoseAreas: function () {
            var iframeIndex = layer.open({
                type: 2,
                area: ['700px', '450px'],
                fixed: false, //不固定
                maxmin: true,
                btn: ['确认选择'],
                yes: function (index, layero) {
                    var arr = [];
                    var areaArr = [];
                    var iframeid = "layui-layer-iframe" + iframeIndex; //获取iframe分配的ID
                    //获取子页面所有勾选框
                    var contens = $("#" + iframeid + "").contents();
                    var checkAreas = $("#" + iframeid + "").contents().find("input[name=check_area]");
                    
                    checkAreas.each(function (index, item) {
                        if (item.checked) {
                            arr.push(item.value);
                            areaArr.push(item.attributes.area.value);
                        }
                    });
                    if (arr.length == 0)
                        arr.push('999999999');//防止in查询报错
                    var index = layer.load(1);
                    RequestManager.Ajax.Post("/Marketing/SetNotAreas", {
                        "ids": arr.join(',')
                    }, true, function (data) {
                        layer.close(index);
                        if (data.IsSuccess) {
                            var htmlArr = [];
                            for (var i = 0; i < areaArr.length; i++) {
                                htmlArr.push('<div><span class="layui-badge-rim">' + areaArr[i] + '</span></div>');
                            }
                            $(".layui-upload-arealist").html(htmlArr.join(''));
                            layer.closeAll('iframe');


                        } else {
                            layer.alert(data.Message);
                        }
                    })
                },
                content: '/Marketing/CurrentAreas'
            });
        }
    };
})();
