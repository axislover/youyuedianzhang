﻿(function () {
    UserCouponListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 30 }, CouponType:999 };
    UserCouponListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });
           
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, UserCouponListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);

        },
        
        Search: function () {
            _searchCriteria.NickName = $("#search_NickName").val();
            _searchCriteria.CouponType = $("#search_type").val();
            
            UserCouponListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/Marketing/UserCouponList";
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Marketing/UserCouponList", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()