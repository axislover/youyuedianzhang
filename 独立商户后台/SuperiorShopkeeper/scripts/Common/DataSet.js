﻿(function () {
    DataSet.Province = {
        List: function () {
            var json = '[{"Id":1,"Area_Code":110000,"Parent_Id":0,"Area_Name":"北京市"},{"Id":2,"Area_Code":120000,"Parent_Id":0,"Area_Name":"天津市"},{"Id":3,"Area_Code":130000,"Parent_Id":0,"Area_Name":"河北省"},{"Id":4,"Area_Code":140000,"Parent_Id":0,"Area_Name":"山西省"},{"Id":5,"Area_Code":150000,"Parent_Id":0,"Area_Name":"内蒙古自治区"},{"Id":6,"Area_Code":210000,"Parent_Id":0,"Area_Name":"辽宁省"},{"Id":7,"Area_Code":220000,"Parent_Id":0,"Area_Name":"吉林省"},{"Id":8,"Area_Code":230000,"Parent_Id":0,"Area_Name":"黑龙江省"},{"Id":9,"Area_Code":310000,"Parent_Id":0,"Area_Name":"上海市"},{"Id":10,"Area_Code":320000,"Parent_Id":0,"Area_Name":"江苏省"},{"Id":11,"Area_Code":330000,"Parent_Id":0,"Area_Name":"浙江省"},{"Id":12,"Area_Code":340000,"Parent_Id":0,"Area_Name":"安徽省"},{"Id":13,"Area_Code":350000,"Parent_Id":0,"Area_Name":"福建省"},{"Id":14,"Area_Code":360000,"Parent_Id":0,"Area_Name":"江西省"},{"Id":15,"Area_Code":370000,"Parent_Id":0,"Area_Name":"山东省"},{"Id":16,"Area_Code":410000,"Parent_Id":0,"Area_Name":"河南省"},{"Id":17,"Area_Code":420000,"Parent_Id":0,"Area_Name":"湖北省"},{"Id":18,"Area_Code":430000,"Parent_Id":0,"Area_Name":"湖南省"},{"Id":19,"Area_Code":440000,"Parent_Id":0,"Area_Name":"广东省"},{"Id":20,"Area_Code":450000,"Parent_Id":0,"Area_Name":"广西壮族自治区"},{"Id":21,"Area_Code":460000,"Parent_Id":0,"Area_Name":"海南省"},{"Id":22,"Area_Code":500000,"Parent_Id":0,"Area_Name":"重庆市"},{"Id":23,"Area_Code":510000,"Parent_Id":0,"Area_Name":"四川省"},{"Id":24,"Area_Code":520000,"Parent_Id":0,"Area_Name":"贵州省"},{"Id":25,"Area_Code":530000,"Parent_Id":0,"Area_Name":"云南省"},{"Id":26,"Area_Code":540000,"Parent_Id":0,"Area_Name":"西藏自治区"},{"Id":27,"Area_Code":610000,"Parent_Id":0,"Area_Name":"陕西省"},{"Id":28,"Area_Code":620000,"Parent_Id":0,"Area_Name":"甘肃省"},{"Id":29,"Area_Code":630000,"Parent_Id":0,"Area_Name":"青海省"},{"Id":30,"Area_Code":640000,"Parent_Id":0,"Area_Name":"宁夏回族自治区"},{"Id":31,"Area_Code":650000,"Parent_Id":0,"Area_Name":"新疆维吾尔自治区"}]';
            return JSON.parse(json);
        },
        //判断是否全国
        IsAllProvince: function (ids) {
            var areas = DataSet.Province.List();
            var res = true;
            for (var i = 0; i < areas.length; i++) {
                if (!DataSet.Province.IsExist(areas[i].Area_Code, ids)) {
                    res = false;
                    break;
                }
            }
            return res;
        },
        IsExist: function (id,ids) {
            var res = false;
            for (var i = 0; i < ids.length; i++) {
                if (ids[i] == id) {
                    res = true;
                    break;
                }
            }
            return res;
        }
        
    }
})();