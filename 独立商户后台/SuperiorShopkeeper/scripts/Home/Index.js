﻿(function () {
    IndexClass = {};
    IndexClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });
            this.CreateUserCountChart();
            //this.CreateTypeUserCountChart();
            this.CreateSallChart();
            $(document).on('click', '.FastJump a', this.FastJump);
            $(document).on('click', '#optionWord', this.FastJump);
            $(document).on('click', '#gotobuy', this.FastJump);
            $(document).on('click', '#btn_openWord', this.OpenWord);
            $(document).on('click', '#btn_openQuestion', this.OpenQuestion);
        },
        OpenQuestion:function(){
            var url = $(this).attr("lay-href");
            window.open(url);
        },
        OpenWord: function () {
            var url = $(this).attr("lay-href");
            window.open(url);
        },
        FastJump: function () {
            window.parent.location.href = $(this).attr("lay-href");
        },
        CreateUserCountChart: function () {
            $.get("/Home/GetAddUserChart?r=" + new Date().getTime(), function (data) {
                if (data.IsSuccess) {
                    var myChart = echarts.init(document.getElementById('addUserCountChart'));
                    var option = {
                        color: ['#3398DB'],
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                            }
                        },
                        grid: {
                            left: '3%',
                            right: '4%',
                            bottom: '3%',
                            containLabel: true
                        },
                        xAxis: [
                            {
                                type: 'category',
                                data: data.Data.DateList,
                                axisTick: {
                                    alignWithLabel: true
                                }
                            }
                        ],
                        yAxis: [
                            {
                                type: 'value'
                            }
                        ],
                        series: [
                            {
                                name: '用户增长',
                                type: 'bar',
                                barWidth: '60%',
                                data: data.Data.DataList
                            }
                        ]

                    }
                    myChart.setOption(option);

                } else {
                    layer.alert(data.Message);
                }
            })
        },        
        CreateSallChart: function () {
            $.get("/Home/GetSallChart?r=" + new Date().getTime(), function (data) {
                if (data.IsSuccess) {
                    var myChart = echarts.init(document.getElementById('sallPriceChart'));
                    var myChart1 = echarts.init(document.getElementById('sallCountChart'));
                    var option = {
                        tooltip: {
                            trigger: 'axis'
                        },
                        xAxis: {
                            type: 'category',
                            data: data.Data.DateList
                        },
                        yAxis: {
                            type: 'value'
                        },
                        series: [{
                            name: '销售额',
                            data: data.Data.OrderAmountList,
                            type: 'line',
                            smooth: true
                        }]
                    };
                    var option1 = {
                        tooltip: {
                            trigger: 'axis'
                        },
                        xAxis: {
                            type: 'category',
                            data: data.Data.DateList
                        },
                        yAxis: {
                            type: 'value'
                        },
                        series: [{
                            name: '销售量',
                            data: data.Data.OrderCountList,
                            type: 'line',
                            smooth: true
                        }]
                    };
                    myChart.setOption(option);
                    myChart1.setOption(option1);
                } else {
                    layer.alert(data.Message);
                }
            })
        }

    };
})()