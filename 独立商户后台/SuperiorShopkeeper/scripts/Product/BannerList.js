﻿(function () {
    BannerListClass = {};
    var _id = "";//当前操作的分类ID
    BannerListClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_addBanner', this.Add);
            $(document).on('click', '.DeleteBanner', this.Delete);
            $(document).on('click', '.OptionBanner', this.OptionStatus);
        },
        Add: function () {
            window.location.href = "/Product/AddBanner";
        },
        OptionStatus: function () {
            var id = $(this).attr("OptionId");
            var opt = $(this).attr("Opt");
            var index = layer.load(1);
            $.get("/Product/OptionBanner?id=" + id + "&optionStatus=" + opt + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/Product/BannerList";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        Delete: function () {
            var id = $(this).attr("OptionId");
            layer.confirm('删除后数据不可恢复，确定要执行吗？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/Product/DelBanner?id=" + id + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("执行成功", function () {
                            window.location.href = "/Product/BannerList";
                        })

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {

            });
        },
       
        
    };
})();