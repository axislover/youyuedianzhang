﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SuperiorModel
{
    public class HomeModel
    {
        public int TotalUserCount { get; set; }
        public int TotalProductCount { get; set; }
        public int TotalOrderCount { get; set; }
        public decimal TotalOrderAmount { get; set; }
        public int WaitSendCount { get; set; }
        public int WaitServiceCount { get; set; }

    }

    

    public class Chart_AddUserModel : ChartDateBase
    {
        public Chart_AddUserModel()
        {
            this.DataList = new List<int>();
        }
        public List<int> DataList { get; set; }
    }

    public class Chart_SallItem
    {
        public int TotalCount { get; set; }
        public decimal TotalAmount { get; set; }

        public string CreateTime { get; set; }
    }

    public class Chart_SallModel : ChartDateBase
    {
        public Chart_SallModel()
        {
            this.OrderCountList = new List<int>();
            this.OrderAmountList = new List<decimal>();
        }
        public List<int> OrderCountList { get; set; }
        public List<decimal> OrderAmountList { get; set; }
    }


    public class ChartDateBase
    {
        public List<string> DateList
        {
            get
            {
                var now = DateTime.Now;
                var res = new List<string>();
                for (int i = 7; i > 0; i--)
                {
                    res.Add(now.AddDays(-i).ToString("yyyy-MM-dd"));
                }
                return res;
            }
        }
    }

    public class RechargeModel
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int AppType { get; set; }
        public int SallType { get; set; }
        public decimal Amount { get; set; }
    }

    public class CreateRechargeOrderResult
    {
        public int Id { get; set; }
        public string OrderNo { get; set; }
        public double Amount { get; set; }
    }

    public class RechargeOrderModel
    {
        public int Id { get; set; }
        public int ShopAdminId { get; set; }
        public int RechargeId { get; set; }
        public decimal Amount { get; set; }
        public string Title { get; set; }
        public int AppType { get; set; }
        public int SallType { get; set; }
        public string RechargeNo { get; set; }
        public string PayCode { get; set; }
        public DateTime CreateTime { get; set; }
        public DateTime PayTime { get; set; }
        public int OrderStatus { get; set; }
        public string LoginName { get; set; }
        public int AccountManagerId { get; set; }
        public string AccountMangerAmount { get; set; }

    }

    public class VersionItem
    {
        [Required(ErrorMessage = "版本号不能为空")]
        public string Version { get; set; }
        [Required(ErrorMessage = "更新内容不能为空")]
        public string Msg { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
