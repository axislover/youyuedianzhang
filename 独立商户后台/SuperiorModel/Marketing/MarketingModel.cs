﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace SuperiorModel
{
    public class FullSetModel
    {
        public int Id { get; set; }

        public int Amount { get; set; }
        public int OptionStatus { get; set; }
        public string NotHasProductIds { get; set; }
        public string NotHasProvinceIds { get; set; }
        public List<string> NotHasProductImgs { get; set; }
        public List<string> NotHasAreaNames { get; set; }

        public List<string> NotHasProductIdList
        {
            get
            {
                if (string.IsNullOrEmpty(this.NotHasProductIds))
                {
                    return new List<string>();
                }
                else
                {
                    return this.NotHasProductIds.Split(',').ToList();
                }


            }
        }

        public List<string> NotHasProvinceIdList
        {
            get
            {
                if (string.IsNullOrEmpty(this.NotHasProvinceIds))
                {
                    return new List<string>();
                }
                else
                {
                    return this.NotHasProvinceIds.Split(',').ToList();
                }


            }
        }
    }

    public class CouponModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "优惠券名称不能为空")]
        public string CouponName { get; set; }
        /// <summary>
        /// 优惠券类型，1满减，2折扣
        /// </summary>
        public int CouponType { get; set; }

        /// <summary>
        /// 满减金额
        /// </summary>
        /// 
        [RegularExpression(@"(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)", ErrorMessage = "满减金额不合法")]
        public decimal DelAmount { get; set; }
        /// <summary>
        /// 折扣率，1-100，70就是7折
        /// </summary>
        /// 

        public int Discount { get; set; }
        /// <summary>
        /// 最低消费金额，才可以使用
        /// </summary>
        /// 
        [RegularExpression(@"(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)", ErrorMessage = "最低消费金额不合法")]
        public decimal MinOrderAmount { get; set; }

        /// <summary>
        /// 有效天数
        /// </summary>
        /// 
        [RegularExpression(@"^\+?[1-9][0-9]*$", ErrorMessage = "有效天数必须是正整数")]
        public int Days { get; set; }
        /// <summary>
        /// 总发放量
        /// </summary>
        /// 
        [RegularExpression(@"^\+?[1-9][0-9]*$", ErrorMessage = "总发放量必须是正整数")]
        public int TotalCount { get; set; }
        /// <summary>
        /// 已被领取的数量
        /// </summary>
        public int GetCount { get; set; }
        public int ShopAdminId { get; set; }
        public string LoginName { get; set; }
        public DateTime CreateTime { get; set; }

    }

    public class UserCouponModel
    {
        public string NickName { get; set; }
        public int CouponId { get; set; }
        public string CouponName { get; set; }
        public int CouponType { get; set; }
        public DateTime CreateTime { get; set; }
    }
}
