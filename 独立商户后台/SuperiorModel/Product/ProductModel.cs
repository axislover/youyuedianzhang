﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SuperiorModel
{

    public class ProductModel
    {
        public ProductModel()
        {
            this.DetailImgs = new List<string>();
            this.BannerImgs = new List<string>();
        }
        public string Id { get; set; }
        public int ShopAdminId { get; set; }
        public string LoginName { get; set; }
        [Required(ErrorMessage = "商品名称不能为空")]
        public string ProductName { get; set; }
        public string Brand { get; set; }
        [Required(ErrorMessage = "商品简介不能为空")]
        public string Title { get; set; }
        public int MenuId { get; set; }
        public int IsHot { get; set; }

        public int ShipTemplateId { get; set; }

        public string MenuName { get; set; }
        public int OptionStatus { get; set; }
        public string CreateTime { get; set; }
        [Required(ErrorMessage = "商品单位不能为空")]
        public string Unit { get; set; }
        [RegularExpression(@"^\+?[1-9][0-9]*$", ErrorMessage = "排序必须是正整数")]
        public int Sorft { get; set; }
        [RegularExpression(@"^\d+$", ErrorMessage = "初始销量必须是非负整数")]
        public int InitSallNum { get; set; }
        [Required(ErrorMessage = "商品详情不能为空")]
        public string Detail { get; set; }
        public int SallCount { get; set; }
        public List<string> DetailImgs { get; set; }
        public List<string> BannerImgs { get; set; }
        /// <summary>
        /// 封面图
        /// </summary>
        public string ProductImg { get; set; }
    }
    
    public class ProductImgModel
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }
        public int ImageType { get; set; }
        public int IsDefault { get; set; }

    }


    
    public class MenuModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "分类名称不能为空")]
        public string MenuName { get; set; }
        [RegularExpression(@"^\+?[1-9][0-9]*$", ErrorMessage = "排序必须是正整数")]
        public int Sorft { get; set; }
        public int IsAll { get; set; }
        public int OptionStatus { get; set; }
        public string CreateTime { get; set; }
        [Required(ErrorMessage = "分类图标不能为空")]
        public string ImgPath { get; set; }
        public int ShopAdminId { get; set; }
    }
    
    public class SingleSkuModel
    {
        public int Id { get; set; }
        public string ProductId { get; set; }
        [RegularExpression(@"^\d+$", ErrorMessage = "库存必须是非负整数")]
        public int StockNum { get; set; }
        [RegularExpression(@"(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)", ErrorMessage = "商品金额不合法")]
        public decimal SallPrice { get; set; }
        public int SkuType { get; set; }
        public string SkuCode { get; set; }
        public int ShopAdminId { get; set; }
        public float Weight { get; set; }

    }
    
    public class MoreSkuModel
    {
        public int Id { get; set; }
        public string ProductId { get; set; }
        /// <summary>
        /// 属性值ID组合如：100-200-1000
        /// </summary>
        /// 
        [Required(ErrorMessage = "属性值ID组合不能为空")]
        public string PropetyCombineId { get; set; }
        /// <summary>
        /// 销售属性值组合红色+36cm+4G
        /// </summary>
        /// 
        [Required(ErrorMessage = "属性值组合不能为空")]
        public string ProppetyCombineName { get; set; }

        [RegularExpression(@"^\d+$", ErrorMessage = "库存必须是非负整数")]
        public int StockNum { get; set; }
        [RegularExpression(@"(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)", ErrorMessage = "商品金额不合法")]
        public decimal SallPrice { get; set; }
        public int SkuType { get; set; }
        public string SkuCode { get; set; }
        public float Weight { get; set; }
        public int ShopAdminId { get; set; }
    }
    
    public class Product_Sall_PropetyModel
    {
        public Product_Sall_PropetyModel()
        {
            this.Product_Sall_Propety_ValueList = new List<Product_Sall_Propety_ValueModel>();
        }
        public int Id { get; set; }
        public string PropetyName { get; set; }
        public string ProductId { get; set; }
        public List<Product_Sall_Propety_ValueModel> Product_Sall_Propety_ValueList { get; set; }

    }
    
    public class Product_Sall_Propety_ValueModel
    {
        public int Id { get; set; }
        public int SallPropetyId { get; set; }
        public string PropetyValue { get; set; }
        public string ProductId { get; set; }
    }

    /// <summary>
    /// 商品规格设置页面业务模型，这里一个模型供2个业务用，懒着拆了。API也走这个
    /// </summary>
    /// 
    
    public class ProductSkuManagerModel
    {
        public ProductSkuManagerModel()
        {
            this.SingleSkuModel = new SingleSkuModel();
            this.MoreSkuModelList = new List<MoreSkuModel>();
            this.Product_Sall_PropetyModelList = new List<Product_Sall_PropetyModel>();
            this.Product_Sall_Propety_ValueModelList = new List<Product_Sall_Propety_ValueModel>();
        }
        public string ProductId { get; set; }
        /// <summary>
        /// 当前商品是单规格还是多规格
        /// </summary>
        public int SkuType { get; set; }
        /// <summary>
        /// 单规格实体模型
        /// </summary>
        public SingleSkuModel SingleSkuModel { get; set; }
        /// <summary>
        /// 多规格实体模型集合
        /// </summary>
        public List<MoreSkuModel> MoreSkuModelList { get; set; }
        /// <summary>
        /// 当前商品多规格下的属性模型集合
        /// </summary>
        public List<Product_Sall_PropetyModel> Product_Sall_PropetyModelList { get; set; }
        /// <summary>
        /// 当前商品多规格下的属性值模型集合
        /// </summary>
        public List<Product_Sall_Propety_ValueModel> Product_Sall_Propety_ValueModelList { get; set; }
    }

    public class AddPropetyParamsModel
    {
        [Required(ErrorMessage = "规格名称不能为空")]
        public string PropetyName { get;set;}
        [Required(ErrorMessage = "规格值不能为空")]
        public string PropetyValue { get; set; }
        public string ProductId { get; set; }
        public int ShopAdminId { get; set; }
    }

    public class AddPropetyValueParamsModel
    {
        [Required(ErrorMessage = "规格值不能为空")]
        public string SkuValue { get; set; }
        public string ProductId { get; set; }
        public int PropetyId { get; set; }
    }
    
    public class ProductBannerModel
    {
        public int Id { get; set; }
        /// <summary>
        ///
        /// </summary>
        /// 
        [Required(ErrorMessage = "商品ID不能为空")]
        public string ProductId { get; set; }
        public int OptionStatus { get; set; }
        [Required(ErrorMessage = "图片不能为空")]
        public string ImgPath { get; set; }
        [RegularExpression(@"^\+?[1-9][0-9]*$", ErrorMessage = "排序必须是正整数")]
        public int Sorft { get; set; }
        public int ShopAdminId { get; set; }
    }
    /// <summary>
    /// 专场模型
    /// </summary>
    /// 
    
    public class SpecialPlaceModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "专场不能为空")]
        public string PlaceName { get; set; }
        [Required(ErrorMessage = "图片不能为空")]
        public string ImgPath { get; set; }
        public int OptionStatus { get; set; }
        public int MenuId { get; set; }
        public int SpecialType { get; set; }
        public int ShopAdminId { get; set; }
        public string MenuName { get; set; }
        [RegularExpression(@"^\+?[1-9][0-9]*$", ErrorMessage = "排序必须是正整数")]
        public int Sorft { get; set; }
        
    }
}
