﻿using System;
using System.Collections.Generic;

namespace SuperiorModel
{
    public class PagingResult
    {
        public PagingResult()
        {

        }
        public PagingResult(int pageIndex, int pageSize)
        {
            this.PageIndex = pageIndex;
            this.PageSize = pageSize;
        }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }

        public int TotalCount { get; set; }
        public int RecordCount { get; set; }
        public int TotalPages
        {
            get
            {
                var val = (float)TotalCount / PageSize;
                var count = (int)Math.Ceiling(val);
                return count;
            }
        }

        public bool HasPreviousPage
        {
            get { return (PageIndex > 0); }
        }

        public bool HasNextPage
        {
            get { return (PageIndex + 1 < TotalPages); }

        }
    }

    public class TModelManager<T>
    {
        public TPagedModelList<T> TPagedModelList { get; set; }
    }
    public class TPagedModelList<T>
    {
        public TPagedModelList(List<T> list, PagingResult PagingResult)
        {
            this.TList = list;
            this.PagingResult = PagingResult;
        }
        public List<T> TList { get; set; }
        public PagingResult PagingResult { get; set; }
    }
}
