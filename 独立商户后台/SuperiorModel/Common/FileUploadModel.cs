﻿namespace SuperiorModel
{
    public class FileUploadModel
    {
        public FileUploadModel(bool isSuccess, string message)
            : this(isSuccess, message, string.Empty)
        {
        }

        public FileUploadModel(bool isSuccess, string message, string fileName)
        {
            IsSuccess = isSuccess;
            Message = message;
            UploadedFileName = fileName;
        }
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
        public string UploadedFileName { get; set; }
    }

    public class LayuiUploadModel
    {
        public LayuiUploadModel(string code, string msg, LayuiUploadImgItem item)
        {
            this.code = code;
            this.msg = msg;
            this.data = item;
        }
        public string code { get; set; }
        public string msg { get; set; }
        public LayuiUploadImgItem data { get; set; }

    }
    public class LayuiUploadImgItem
    {
        public string src { get; set; }
        public string title { get; set; }

    }
}
