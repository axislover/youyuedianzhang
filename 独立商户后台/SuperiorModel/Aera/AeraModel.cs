﻿namespace SuperiorModel
{

    public class AreaModel
    {
        public int Id { get; set; }
        public int Area_Code { get; set; }
        public int Parent_Id { get; set; }
        public string Area_Name { get; set; }

    }
}
