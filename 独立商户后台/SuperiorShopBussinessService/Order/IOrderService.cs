﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IOrderService:IService
    {
        PagedSqlList<OrderModel> Search(OrderCriteria criteria);
        OrderDetailModel GetDetail(int orderid);
        void SendProduct(SendProductParms model);
        void FinishService(int orderid);
        
        void UpdateDelivery(SendProductParms model);
    }
}
