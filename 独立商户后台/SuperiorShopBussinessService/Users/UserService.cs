﻿using SuperiorCommon;
using SuperiorModel;
using SuperiorShopDataAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class UserService : IUserService
    {
        public PagedSqlList<UserListItem> Search(UserCriteria criteria)
        {
            var queryList = new List<UserListItem>();
            var totalCount = 0;
            var ds = UserDataAccess.Search(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new UserListItem()
                        {
                            Id = SecretClass.EncryptQueryString(dr["Id"].ToString()),
                            CreateTime =Convert.ToDateTime(dr["CreateTime"]),
                            ShopName = dr["ShopName"].ToString(),
                            NickName = dr["NickName"].ToString(),
                            OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                            LoginName = dr["LoginName"].ToString(),
                            HeadImgUrl = dr["HeadImgUrl"].ToString()
                           
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<UserListItem>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public int OptionUser(int id, int optionStatus)
        {
            return UserDataAccess.OptionUser(id,optionStatus);
        }

        
        public void Dispose()
        {

        }
    }
}
