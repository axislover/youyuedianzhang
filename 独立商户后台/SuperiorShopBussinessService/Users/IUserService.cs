﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IUserService : IService
    {
        PagedSqlList<UserListItem> Search(UserCriteria criteria);
        int OptionUser(int id, int optionStatus);
       
    }
}
