﻿using SuperiorCommon;
using SuperiorModel;
using SuperiorModel.Criteria.Marketing;
using SuperiorShopDataAccess;
using System;
using System.Collections.Generic;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class MarketingService : IMarketingService
    {

        public FullSetModel GetSetModel(int shopAdminId)
        {
            var res = new FullSetModel();
            res.NotHasAreaNames = new List<string>();
            res.NotHasProductImgs = new List<string>();
            var ds = MarketingDataAccess.GetSetModel(shopAdminId);
            if (DataManager.CheckDs(ds, 3))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id = Convert.ToInt32(dr["Id"]);
                    res.Amount = Convert.ToInt32(dr["Amount"]);
                    res.OptionStatus = Convert.ToInt32(dr["OptionStatus"]);
                    res.NotHasProductIds = dr["NotHasProductIds"].ToString();
                    res.NotHasProvinceIds = dr["NotHasProvinceIds"].ToString();
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        res.NotHasProductImgs.Add(ConfigSettings.Instance.ImageServiceUrl + dr["ImagePath"].ToString());
                    }
                }
                if (DataManager.CheckHasRow(ds.Tables[2]))
                {
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        res.NotHasAreaNames.Add(dr["Area_Name"].ToString());
                    }
                }
            }

            return res;
        }

        public void SetNotPids(string ids, int shopAdminId)
        {
            MarketingDataAccess.SetNotPids(ids, shopAdminId);
        }
        public void SetNotAreas(string ids, int shopAdminId)
        {
            MarketingDataAccess.SetNotAreas(ids, shopAdminId);
        }
        public void SetFullAmount(int optionStatus, int amount, int shopAdminId)
        {
            MarketingDataAccess.SetFullAmount(optionStatus, amount, shopAdminId);
        }

        public List<CouponModel> GetCouponList(int shopAdminId)
        {
            var res = new List<CouponModel>();
            var ds = MarketingDataAccess.GetCouponList(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new CouponModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            CouponName = dr["CouponName"].ToString(),
                            CouponType = Convert.ToInt32(dr["CouponType"]),
                            DelAmount = DataManager.GetRowValue_Decimal(dr["DelAmount"]),
                            Discount = DataManager.GetRowValue_Int(dr["Discount"]),
                            MinOrderAmount = DataManager.GetRowValue_Decimal(dr["MinOrderAmount"]),
                            Days = Convert.ToInt32(dr["Days"]),
                            GetCount = Convert.ToInt32(dr["GetCount"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                            TotalCount = Convert.ToInt32(dr["TotalCount"])
                        });
                    }
                }
            }
            return res;
        }
    

        public void AddCoupon(CouponModel model)
        {
            MarketingDataAccess.AddCoupon(model);
        }
        public void DelCoupon(int id)
        {
            MarketingDataAccess.DelCoupon(id);
        }

        public PagedSqlList<UserCouponModel> SearchUserCoupon(UserCouponCriteria criteria)
        {
            var queryList = new List<UserCouponModel>();
            var totalCount = 0;
            var ds = MarketingDataAccess.SearchUserCouponList(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new UserCouponModel()
                        {
                            NickName = dr["NickName"].ToString(),
                            CouponName = dr["CouponName"].ToString(),
                            CouponId = Convert.ToInt32(dr["CouponId"]),
                            CouponType = Convert.ToInt32(dr["CouponType"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<UserCouponModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

     

        public void Dispose()
        {

        }
    }
}
