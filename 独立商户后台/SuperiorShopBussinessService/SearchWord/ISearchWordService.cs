﻿using SuperiorModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperiorShopBussinessService
{
    public interface ISearchWordService:IService
    {
        List<SearchWordModel> GetList(int shopAdminId);
        void Add(SearchWordModel model);
        void Del(int id);
        void Option(int id, int option);
    }
}
