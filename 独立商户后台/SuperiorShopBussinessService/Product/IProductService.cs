﻿using SuperiorModel;
using System.Collections.Generic;

namespace SuperiorShopBussinessService
{
    public interface IProductService : IService
    {
        List<MenuModel> MenuList(int shopAdminId);
        int AddMenu(MenuModel model, out string msg);
        int EditMenu(MenuModel model, out string msg);

        int OptionMenu(int id, int optionStatus, int shopAdminId, out string msg);
        int DeleteMenu(int id, int shopAdminId, out string msg);

        int OptionProduct(int id, int optionStatus, int shopAdminId, out string msg);
        int DeleteProduct(int id, int shopAdminId, out string msg);

        int AddPropety(AddPropetyParamsModel model, out string msg);

        PagedSqlList<ProductModel> ProductList(ProductCriteria criteria);

        bool AddProduct(ProductModel model);

        bool EditProduct(ProductModel model);


        ProductModel GetProduct(int id);

        ProductSkuManagerModel GetProductSkuManagerModel(int shopAdminId, string productId);
        ProductSkuManagerModel GetProductSkuManagerModel_AJAX(int shopAdminId, string productId);

        bool EditSingleSku(SingleSkuModel model);
        bool EditMoreSku(ProductSkuManagerModel model, int shopAdminId);
        int AddPropetyValue(Product_Sall_Propety_ValueModel model, out string msg);
        bool DelPropetyValue(Product_Sall_Propety_ValueModel model);
        bool DelPropety(int id, string productId, int shopAdminId);
        bool AddProductBanner(ProductBannerModel model);
        bool EditProductBanner(ProductBannerModel model);
        void OptionProductBanner(int id, int optionStatus);
        void DelProductBanner(int id);
        List<ProductBannerModel> GetProductBannerList(int shopAdminId);
        ProductBannerModel GetProductBanner(int id);

        List<SpecialPlaceModel> SpecialPlaceList(int shopAdminId);
        void DelSpecialPlace(int id);
        bool InsertOrEditSpecialPlace(SpecialPlaceModel model);
        SpecialPlaceModel GetSpecialPlaceModel(int id);
    }
}
