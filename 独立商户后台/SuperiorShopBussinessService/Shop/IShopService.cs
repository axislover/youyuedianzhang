﻿using SuperiorModel;
using System.Collections.Generic;

namespace SuperiorShopBussinessService
{
    public interface IShopService :IService
    {
        ShopInfoModel GetShopModel(string username, string password);
        ShopInfoModel GetShopModel_Role(string username, string password);
        int InsertShop(ShopInfoModel model,out string msg);
        List<int> GetShopAppIdsByShopAdminId(int shopAdminId);
    }
}
