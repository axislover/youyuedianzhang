﻿using SuperiorModel;
using SuperiorSqlTools;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;


namespace SuperiorShopDataAccess
{
    public class ShopAppDataAccess
    {
        public static DataSet ShopAppList(ShopAppCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            if (!string.IsNullOrEmpty(criteria.ShopName))
            {
                sb.Append(" and sa.ShopName=@shopname");
                parms.Add(new SqlParameter("@shopname", criteria.ShopName));
            }
            if (!string.IsNullOrEmpty(criteria.ShopAdminId))
            {
                sb.Append(" and sa.ShopAdminId=@shopid ");
                parms.Add(new SqlParameter("@shopid", criteria.ShopAdminId.ToInt()));
            }
            if (!string.IsNullOrEmpty(criteria.LoginName))
            {
                sb.Append(" and a.LoginName=@LoginName ");
                parms.Add(new SqlParameter("@LoginName", criteria.LoginName));
            }
            if (!string.IsNullOrEmpty(criteria.StartTime))
            {
                sb.Append(" and sa.CreateTime>=@starttime ");
                parms.Add(new SqlParameter("@starttime", criteria.StartTime));
            }
            if (!string.IsNullOrEmpty(criteria.EndTime))
            {
                sb.Append(" and sa.CreateTime<=@endtime ");
                parms.Add(new SqlParameter("@endtime", criteria.EndTime));
            }
            var sql = string.Format("select top({0}) * from(select ROW_NUMBER() over(order by sa.Id desc)as rownum,sa.*,a.LoginName  from C_ShopApp sa left join C_ShopAdmin a on sa.ShopAdminId=a.Id  where {1} )tt where tt.rownum>{2};select count(*) as totalCount from C_ShopApp sa where {1}", criteria.PagingResult.PageSize,sb.ToString(),criteria.PagingResult.PageSize* criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }

        public static int Add(ShopAppModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@AppId", model.AppId.IsNull()),
                new SqlParameter("@AppSecret",model.AppSecret.IsNull()),
                new SqlParameter("@PaymentId",model.PaymentId.IsNull()),
                new SqlParameter("@PaySecret",model.PaySecret.IsNull()),
                new SqlParameter("@ShopAdminId",model.ShopAdminId.ToInt()),
                new SqlParameter("@ServiceQQ",model.ServiceQQ.IsNull()),
                new SqlParameter("@ServiceWx",model.ServiceWx.IsNull()),
                new SqlParameter("@ServicePhone",model.ServicePhone),
                new SqlParameter("@ShopName",model.ShopName),
                rtn_err 
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "InsertShopApp", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public static int Edit(ShopAppModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                 new SqlParameter("@id", model.Id.ToInt()),
                new SqlParameter("@AppId", model.AppId.IsNull()),
                new SqlParameter("@AppSecret",model.AppSecret.IsNull()),
                new SqlParameter("@PaymentId",model.PaymentId.IsNull()),
                new SqlParameter("@PaySecret",model.PaySecret.IsNull()),
                new SqlParameter("@ShopAdminId",model.ShopAdminId.ToInt()),
                new SqlParameter("@ServiceQQ",model.ServiceQQ.IsNull()),
                new SqlParameter("@ServiceWx",model.ServiceWx.IsNull()),
                new SqlParameter("@ServicePhone",model.ServicePhone),
                new SqlParameter("@ShopName",model.ShopName),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "EditShopApp", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public static DataSet GetShopAppModel(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@id",id)
            };
            var sql = "select * from C_ShopApp where Id = @id";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static DataSet GetPaykeyByAppid(string appid)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@appid",appid)
            };
            var sql = "select PaySecret from C_ShopApp where AppId=@appid";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static void OptionShopAppStatus(int id, int option)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
          {
                 new SqlParameter("@id", id),
                 new SqlParameter("@option", option)
            };
            var sql = "update C_ShopApp set OptionStatus=@option where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }
        
    }
}
