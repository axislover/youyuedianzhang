// pages/menu/menulist.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
      menus:[],
    getListParam: {
      keyWord: '',
      OffSet: 0,
      Size: 8,
      menuId: 0,
      list: []
    },
    lastRequestCount: 8
  },
  choseMenu:function(e){
    var menuid = e.currentTarget.dataset.menuid;
    this.setData({
      'getListParam.menuId': menuid,
      'getListParam.OffSet': 0,
      lastRequestCount: 8
    });
    this.initgoodsList();
  },
  btnSearch: function () {
    this.setData({
      'getListParam.OffSet': 0,
      lastRequestCount: 8
    });
    this.initgoodsList();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    this.getMenus();
    
    this.setData({
      'getListParam.keyWord':app.globalData.keyword,
      'getListParam.menuId': app.globalData.menuid//跳转tapbar不能带参数，利用全局
    })
    this.initgoodsList();
  },
  /**
  * 页面相关事件处理函数--监听用户下拉动作
  */
  onPullDownRefresh: function () {
    this.setData({
      'getListParam.OffSet': 0,
      lastRequestCount: 8
    });
    this.initgoodsList('stopPullDownRefresh');
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({
      'getListParam.OffSet': this.data.getListParam.OffSet + 1
    });
    this.initgoodsList('onReachBottom');
  },

  keyWordChange: function (ev) {
    this.setData({
      'getListParam.keyWord': ev.detail.value
    });
  },


  initgoodsList: function (type) {
    if (this.data.lastRequestCount < this.data.getListParam.Size)
      return false;//防止到最后一页继续请求
    var that = this;
    wx.showLoading({
      title: '读取商品中',
    })
    wx.request({
      url: app.appSetting.host + "/product/list",
      data: {
        Spid: app.appSetting.spid,
        keyWord: that.data.getListParam.keyWord,
        OffSet: that.data.getListParam.OffSet,
        Size: that.data.getListParam.Size,
        MenuId: that.data.getListParam.menuId
      },
      method: 'POST',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        app.globalData.menuid=0;
        console.log(res);
        wx.hideLoading();
        if (res.statusCode == 200) {
          that.setData({
            lastRequestCount: res.data.length
          });
          if (type == 'stopPullDownRefresh') {
            that.setData({
              'getListParam.list': res.data
            });
            wx.stopPullDownRefresh()
          }
          if (type == 'onReachBottom') {
            that.setData({
              'getListParam.list': that.data.getListParam.list.concat(res.data)
            });
          } else {
            that.setData({
              'getListParam.list': res.data
            });
          }
        } else {
          wx.showToast({
            title: res.data,
            icon: 'none'
          })
        }
      }
    })

  },
  getMenus: function () {
    var that = this;
    wx.request({
      url: app.appSetting.host + "/Product/menuList?said=" + app.appSetting.said,
      data: {
      },
      method:"GET",
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        if (res.statusCode == 200) {
            that.setData({
              menus: res.data
            })

        } else {
          wx.showToast({
            title: res.data,
            icon: 'none'
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      'getListParam.OffSet': 0,
      lastRequestCount: 8,
      'getListParam.keyWord':app.globalData.keyword,
      'getListParam.menuId': app.globalData.menuid//跳转tapbar不能带参数，利用全局
    })
    this.initgoodsList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },



  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})