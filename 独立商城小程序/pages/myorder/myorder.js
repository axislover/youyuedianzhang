// pages/mall/myorder/myorder.js
const app = getApp();
import TestData from '../../mock/testData.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orders: [],
    getListParam: {
      Spid: app.appSetting.spid,
      UserId: '',
      OffSet: 0,
      Size: 8,
      OrderStatus:999
    },
    statusArr:[1,0,0,0,0,0],
    shopname:'',
    said: app.appSetting.said,
    lastRequestCount:8,
    currentChoseStatus:999,
    canClick: true
  },
  clickOrderStatus:function(e){
    console.log(e);
    var tempArr = [0,0,0,0,0,0];
    tempArr[e.currentTarget.dataset.index]=1;
    this.setData({
      currentChoseStatus: e.currentTarget.dataset.orderstatus,
      statusArr:tempArr,
      orders:[],
      lastRequestCount:8,
      'getListParam.OffSet':0
    });
    this.initOrderList();
  },
  btnComment: function (e) {
    console.log(e);
    wx.navigateTo({
      url: '/pages/comment/add?orderid=' + e.currentTarget.dataset.orderid,
    })
  },
  btnViewOrder: function (ev) {
    setTimeout(() => {
      this.data.canClick = true;
    }, 1000);
    let self = this;
    if (this.data.canClick) {
      this.data.canClick = false;
      TestData.MobileOrderGetPayData(ev.currentTarget.dataset.orderinfo.OrderNo).then((res) => {
        if (res.statusCode == 200) {
          wx.requestPayment({
            'timeStamp': res.data.PayParamater.TimeStamp,
            'nonceStr': res.data.PayParamater.NonceStr,
            'package': res.data.PayParamater.Package,
            'signType': res.data.PayParamater.SignType,
            'paySign': res.data.PayParamater.PaySign,
            success: function (res) {
              wx.showToast({
                title: '支付成功',
                icon: 'none',
                success: function () {
                    self.initOrderList();
                }
              })
            },
            'fail': function (res) {
              wx.showToast({
                title: '取消微信支付',
                icon: 'none'
              })
            },
            'complete': function (res) {
              self.setData({
                canClick: true
              });
            }
          })
        } else {
          this.data.canClick = true;
          wx.showToast({
            title: res.data,
            icon: 'none'
          })
        }
      });
    }
  },

  goToOrderDetail(ev) {
    wx.navigateTo({
      url: '/pages/myorderdetail/myorderdetail?orderId=' + ev.currentTarget.dataset.orderinfo.OrderID,
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu();
   
    this.setData({
      shopname: app.globalData.userInfo.ShopName
    });
  },
  initOrderList: function (type) {
    if (this.data.lastRequestCount < this.data.getListParam.Size)
      return false;//防止到最后一页继续请求
    this.setData({
      'getListParam.UserId': app.globalData.userInfo.Id,
      'getListParam.Spid': app.appSetting.spid,
      'getListParam.OrderStatus':this.data.currentChoseStatus
    });
    var that = this;
    wx.showLoading({
      title: '读取中订单中...',
    })
    wx.request({
      url: app.appSetting.host + "/order/search/list",
      data: this.data.getListParam,
      method: 'POST',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.statusCode == 200) {
          that.setData({
            lastRequestCount: res.data.length
          });
          if (type == 'stopPullDownRefresh') {
            that.setData({
              orders: res.data
            });
            wx.stopPullDownRefresh()
          }
          if (type == 'onReachBottom') {
            that.setData({
              orders: that.data.orders.concat(res.data)
            });
          } else {
            that.setData({
              orders: res.data
            });
          }
        } else {
          wx.showToast({
            title: res.data,
            icon: 'none'
          })
        }
      }
    })

  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.initOrderList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.setData({
      'getListParam.OffSet': 0,
      lastRequestCount: 8
    });
    this.initOrderList('stopPullDownRefresh');
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.setData({
      'getListParam.OffSet': this.data.getListParam.OffSet + 1
    });
    this.initOrderList('onReachBottom');
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})