﻿using System;

namespace SuperiorModel
{
    public static class NullTool
    {
        public static string IsNull(this string str)
        {
            return str == null ? "" : str;
        }

        public static int ToInt(this string str)
        {
            return Convert.ToInt32(str);
        }
    }
}
