﻿namespace SuperiorModel
{
    public class TestModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }
}
