﻿namespace SuperiorModel
{
    public class ShopAppCriteria:PagedBaseModel
    {
        public string ShopName { get; set; }
        public int AppType { get; set; }
        public string ShopAdminId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string LoginName { get; set; }

    }
}
