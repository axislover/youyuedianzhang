﻿namespace SuperiorModel
{
    class ShopCartCriteria
    {
    }
    public class Api_AddShopCartCriteria
    {
        public string UserId { get; set; }
        public string Token { get; set; }
        public string ProductId { get; set; }
        public int SaleNum { get; set; }
        public int Product_SkuId { get; set; }
    }
}
