﻿namespace SuperiorModel
{
    public class Api_CommentModel
    {
        public int Id { get; set; }
        public string NickName { get; set; }
        public string HeadImgUrl { get; set; }
        public int CommentLevel { get; set; }
        public string CommentMsg { get; set; }
        public string CreateTime { get; set; }
    }

    public class Api_AddCommentParms
    {
        public int OrderId { get; set; }
        public string Spid { get; set; }
        public string UserId { get; set; }
        public string Said { get; set; }
        public int CommentLevel { get; set; }
        public string CommentMsg { get; set; }

    }

    public class Api_CommentCriteria
    {
        public string ProductId { get; set; }
        public int OffSet { get; set; }
        public int Size { get; set; }
        
    }
}
