﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SuperiorModel
{
    public class ShopLoginParams
    {
        [Required(ErrorMessage = "用户名不能为空")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "密码不能为空")]
        public string PassWord { get; set; }
        [Required(ErrorMessage = "验证码不能为空")]
        public string Code { get; set; }
    }

    [Serializable]
    public class AdminInfoModel
    {
        public string Id { get; set; }
        public string LoginName { get; set; }
        public string PassWord { get; set; }
        public int ShopAdminType { get; set; }

    }

    
    [Serializable]
    public class ShopInfoModel
    {
        public ShopInfoModel()
        {
            this.ShopLoginRoleModel = new ShopLoginRoleModel();
        }
        public string Id { get; set; }
        [Required(ErrorMessage = "账户不能为空")]
        public string LoginName { get; set; }
        [Required(ErrorMessage = "密码不能为空")]
        public string PassWord { get; set; }
        [Required(ErrorMessage = "再次密码不能为空")]
        public string ResetPassWord { get; set; }
        [Required(ErrorMessage = "介绍不能为空")]
        public string Introduce { get; set; }
        [Required(ErrorMessage = "验证码不能为空")]
        public string Code { get; set; }
        public int PayType { get; set; }
        [Required(ErrorMessage = "手机号不能为空")]
        public string PhoneNumber { get; set; }
        private string _qqcode = "";
        public string QqCode
        {
            get
            {
                return _qqcode;
            }
            set
            {
                _qqcode = value;
            }
        }
        private string _wxcode = "";
        public string WxCode
        {
            get
            {
                return _wxcode;
            }
            set
            {
                _wxcode = value;
            }
        }
        [Required(ErrorMessage = "联系人不能为空")]
        public string Contact { get; set; }
        public string SerectId { get; set; }
        public int OptionStatus { get; set; }
        public DateTime CreateTime { get; set; }
        public string CanUserAmount { get; set; }
        public string AccountManagerId { get; set; }
        /// <summary>
        /// 账号类型，1主账号登录，2员工角色账号登录
        /// </summary>
        public int ShopAdminType { get;set; }
    
        /// <summary>
        /// 员工角色登录，会有这项，用于记录操作日志
        /// </summary>
        public ShopLoginRoleModel ShopLoginRoleModel { get; set; }
        /// <summary>
        /// 小程序商城过期时间
        /// </summary>

        public DateTime ProgrameExpireTime { get; set; }
        /// <summary>
        /// H5商城过期2时间
        /// </summary>
        public DateTime H5ExpireTime { get; set; }

        public string NickName { get; set; }
        public string HeadImgUrl { get; set; }
        /// <summary>
        /// 小程序版本号
        /// </summary>
        public string ProgramVersion { get; set; }

        /// <summary>
        /// 小程序商城是否过期
        /// </summary>
        public bool ProgrameIsExpire
        {
            get
            {
                if (this.ProgrameExpireTime == null)
                {
                    return true;
                }
                else
                {
                    return DateTime.Now > this.ProgrameExpireTime;
                }
            }
        }
        /// <summary>
        /// H5商城是否过期
        /// </summary>
        public bool H5IsExpire
        {
            get
            {
                if (this.H5ExpireTime == null)
                {
                    return true;
                }
                else
                {
                    return DateTime.Now > this.H5ExpireTime;
                }
            }
        }

    }
    [Serializable]
    public class ShopLoginRoleModel
    {
        public string RoleName { get; set; }
        public string Account { get; set; }
        public int OptionStatus { get; set; }
        /// <summary>
        /// 账号权限JSON字符串
        /// </summary>
        public string Authoritys { get; set; }


        private List<AuthorityModel> _AuthoritysList = null;
        public List<AuthorityModel> AuthoritysList
        {
            get
            {
                if (_AuthoritysList == null)
                {
                    if (string.IsNullOrEmpty(this.Authoritys))
                    {
                        _AuthoritysList = new List<AuthorityModel>();
                        return _AuthoritysList;
                    }
                    else
                    {
                        _AuthoritysList = JsonConvert.DeserializeObject<List<AuthorityModel>>(this.Authoritys);
                        return _AuthoritysList;
                    }
                }
                return _AuthoritysList;



            }
            set
            {
                _AuthoritysList = value;
            }
        }

    }

    public class ShopRoleAccountModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "员工账号不能为空")]
        public string Account { get; set; }
        [Required(ErrorMessage = "员工密码不能为空")]
        public string PassWord { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public int ShopAdminId { get; set; }
        public int OptionStatus { get; set; }
        public DateTime CreateTime { get; set; }
    }

    public class ShopRoleModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "角色不能为空")]
        public string RoleName { get; set; }
        public string Authoritys { get; set; }
        public int ShopAdminId { get; set; }
        private List<AuthorityModel> _AuthoritysList = null;
        public List<AuthorityModel> AuthoritysList
        {
            get
            {
                if (_AuthoritysList == null)
                {
                    if (string.IsNullOrEmpty(this.Authoritys))
                    {
                        _AuthoritysList= new List<AuthorityModel>();
                        return _AuthoritysList;
                    }
                    else
                    {
                        _AuthoritysList = JsonConvert.DeserializeObject<List<AuthorityModel>>(this.Authoritys);
                        return _AuthoritysList;
                    }
                }
                return _AuthoritysList;
               


            }
            set {
                _AuthoritysList = value;
            }
        }

    }

 
}
