﻿namespace SuperiorModel
{
    public class AdminMenuModel
    {
        public int Id { get; set; }
        public string MenuName { get; set; }
        public int Sorft { get; set; }
        public int ShopAdminId { get; set; }
        public int OptionStatus { get; set; }
        public string LoginName { get; set; }
        public string CreateTime { get; set; }
    }
}
