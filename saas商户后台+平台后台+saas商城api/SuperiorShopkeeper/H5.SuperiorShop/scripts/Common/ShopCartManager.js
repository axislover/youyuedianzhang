﻿(function () {
    ShopCartManagerClass = {};
    ShopCartManagerClass.Instance = {
        //本地购物车缓存中，是否存在当前sku
        IsExist: function (list, skuid) {
            var res = false;
            for (var i = 0; i < list.length; i++) {
                if (list[i].Product_SkuId == skuid) {
                    res = true;
                    break;
                }
            }
            return res;
        },
        //通过skuid获取模型
        GetModelBySkuId: function (list, skuid) {
            var res = null;
            for (var i = 0; i < list.length; i++) {
                if (list[i].Product_SkuId == skuid) {
                    res = list[i];
                    break;
                }
            }
            return res;
        },
        //更换购物车模型集合中得某个模型
        UpdateSkuModel: function (list, model) {
            for (var i = 0; i < list.length; i++) {
                if (list[i].Product_SkuId == model.Product_SkuId) {
                    list[i] = model;
                    break;
                }
            }
            return list;
        },
        //购物车模型集合，转成skuids,如23,221
        ConvertToSkuids: function (list) {
            var res = '';
            for (var i = 0; i < list.length; i++) {
                if (i != list.length - 1)
                    res += list[i].Product_SkuId + ',';
                else
                    res += list[i].Product_SkuId;
            }
            return res;
        },
        //将购物车sku模型完善赋值
        GetFullSkuModel: function (list, requestList) {
            for (var i = 0; i < list.length; i++) {
                for (var j = 0; requestList.length; j++) {
                    if (list[i].Product_SkuId == requestList[j].Product_SkuId) {
                        list[i].SkuDetail = requestList[j];
                        break;
                    }
                }
            }
            return list;
        },
        //删除指定skuid得模型
        DeleteSkuBySkuId: function (list, skuid) {
            var res = [];
            for (var i = 0; i < list.length; i++) {
                if (list[i].Product_SkuId != skuid) {
                    res.push(list[i]);
                }
            }
            return res;
        },
        //删除指定skuid[]的模型
        DeleteSkuBySkuIdArr: function (list, skuidArr) {
            var res = list;
            for (var i = 0; i < skuidArr.length; i++) {
                res = ShopCartManagerClass.Instance.DeleteSkuBySkuId(res, skuidArr[i])
            }
            return res;
        },
        //将完整模型得集合简化
        GetEasySkuModelList: function (list) {
            for (var i = 0; i < list.length; i++) {
                list[i].SkuDetail = {};
            }
            return list;
        }

    };
})();