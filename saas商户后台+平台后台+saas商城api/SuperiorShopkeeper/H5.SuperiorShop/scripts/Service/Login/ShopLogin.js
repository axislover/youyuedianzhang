﻿(function () {
    LoginClass = {};
    LoginClass.Instance = {
        Init: function () {
            //读取用户COOKIE
                $("#LAY-user-login-username").val($.cookie("PHONENUMBER"));
                $("#LAY-user-login-password").val($.cookie("TIANHONGPSD") == "null" ? "" : $.cookie("TIANHONGPSD"));
              

            $(document).on("click", "#captchaimg", this.CaptchaImage);
            //layui验证表单写法-----begin
            var form;
            layui.use(['form'], function () {
                form = layui.form;
                form.on('submit(LAY-user-login-submit)', LoginClass.Instance.Login);
                //回车提交
                $(document).on('keypress', '#LAY-user-login input', function (e) {
                    if (e.which == 13) {
                        LoginClass.Instance.Login();
                    }
                })

            });
            //-----end
            
        },
        ShowTxDiv:function(){
            $("#txt_code").val("");
            layer.open({
                type: 1,
                skin: 'layui-layer-rim', //加上边框
                area: ['420px', '440px'], //宽高
                content: $("#showMsgDiv").html()
            });
        },
        CaptchaImage: function () {
            $(this).attr("src", $(this).attr("imgsrc") + "?a=" + Math.random());
        },

        Login: function () {
            var username = $("#LAY-user-login-username").val();
            var password = $("#LAY-user-login-password").val();
            var code = $("#newcaptcha").val();
            var _url = "/ServiceShop/Login";
            var index = layer.load(1);
            RequestManager.Ajax.Post(_url, { "UserName": username, "PassWord": password, "Code": code }, true, function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    LoginClass.Instance.ControlCookie();
                    window.location.href = "/ServiceShop/ShopWxLogin";//测试时候注释
                    //window.location.href = "/ServiceShop/Index";
                } else {
                    layer.alert(data.Message);
                }
            })
        },
        ControlCookie: function () {
                $.cookie('PHONENUMBER', $("#LAY-user-login-username").val(), { expires: 90 });
                $.cookie('TIANHONGPSD', $("#LAY-user-login-password").val(), { expires: 90 });
                $.cookie('ISREMANBER', "1", { expires: 90 });
        },

    };
})()