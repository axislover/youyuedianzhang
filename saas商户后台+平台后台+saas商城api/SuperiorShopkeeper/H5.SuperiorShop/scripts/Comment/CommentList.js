﻿(function () {
    CommentListClass = {};
    var isScrollLoad = false;
    var loadType = 0;//请求数据方式，0，下拉分页加载，1搜索
    var winH = $(window).height(); //页面可视区域高度
    var lastRequestCount = 0;
    var _spid = ToolManager.Common.UrlParms("spid");
    var _said = ToolManager.Common.UrlParms("said");
    var _id = ToolManager.Common.UrlParms("id");
    var criteria = {    
        ProductId: _id,
        OffSet: 1,
        Size: 8,
    };
    CommentListClass.Instance = {
        Init: function () {
            lastRequestCount = $("#commentList").attr("LoadRequstCount");
            $(window).scroll(CommentListClass.Instance.ScrollHandler);
        },
        ScrollHandler: function () {
            if (isScrollLoad || lastRequestCount < criteria.Size) {
                return false;
            }

            var pageH = $(document.body).height();
            var scrollT = $(window).scrollTop(); //滚动条top   
            var aa = (pageH - winH - scrollT) / winH;
            if (aa < 0.3) {//0.02是个参数  
                isScrollLoad = true;
                loadType = 0;
                CommentListClass.Instance.Request();
            }
        },
        Request: function () {
            if (loadType == 0)
                $(".weui-loadmore").show();
            RequestManager.Ajax.Post("/Comment/SearchList?spid=" + _spid, criteria, true, function (data) {
                $(".weui-loadmore").hide();
                if (data.IsSuccess) {
                    lastRequestCount = data.Data.length;
                    if (data.Data.length > 0) {
                        criteria.OffSet = criteria.OffSet + 1;
                        var htmlArr = [];
                        data.Data.forEach((v) => {
                            htmlArr.push('<div class="weui-panel__bd">');
                            htmlArr.push(' <div class="Comment_wy-media-box weui-media-box_text">');
                            htmlArr.push('<div class="weui-cell nopd weui-cell_access">');
                            htmlArr.push('<div class="weui-cell__hd"><img src="' + v.HeadImgUrl + '" alt="" style="width:20px;margin-right:5px;display:block"></div>');
                            htmlArr.push('<div class="weui-cell__bd weui-cell_primary"><p>' + v.NickName + '</p></div>');
                            htmlArr.push(' <span class="weui-cell__time">' + v.CreateTime + '</span>');
                            htmlArr.push(' </div>');
                            htmlArr.push(' <div class="comment-item-star"><span class="real-star comment-stars-width' + v.CommentLevel + '"></span></div>');
                            htmlArr.push('<p class="Comment_weui-media-box__desc">' + v.CommentMsg + '</p>');
                            htmlArr.push(' </div>');
                            htmlArr.push(' </div>');
                        });
                        if (loadType == 0)
                            $("#commentList").append(htmlArr.join(''));
                        else
                            $("#commentList").html(htmlArr.join(''));
                    } else {
                        //搜索
                       
                    }
                    isScrollLoad = false;
                } else {
                    layer.alert(data.Message);
                }
            })

        },


    };

})();