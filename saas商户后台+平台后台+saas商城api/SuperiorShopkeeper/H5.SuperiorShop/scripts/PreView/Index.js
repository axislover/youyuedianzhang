﻿(function () {
    IndexClass = {};
    var said = ToolManager.Common.UrlParms("said");
    var spid = ToolManager.Common.UrlParms("spid");
    IndexClass.Instance = {
        Init: function () {
            $(".swiper-banner").swiper({
                loop: true,
                autoplay: 3000
            });
            //让轮播图片自适应高度
            $(function () {
                var height = $(".swiper-wrapper").height();
                $(".swiper-slide img").each(function () {
                    $(this).css("height", height + "px");
                })
                IndexClass.Instance.InitSpecialPlace();
            })
           
        },
        InitSpecialPlace: function () {

            $.get("/Priveiw/GetSpecialPlaceData?said=" + said + "&spid=" + spid + "&r=" + new Date().getTime(), function (data) {
                if (data.IsSuccess) {
                    if (data.Data.length > 0) {
                        var arr = [];
                        data.Data.forEach((value, index) => {
                            arr.push('<div class="meui-guang">');
                            arr.push('<a href="/Product/MenuList?menuid=' + value.MenuId + '&spid=' + spid + '&said=' + said + '"><img style="height: 130px;" src="' + value.ImgPath + '" alt="" /></a>');
                            arr.push('</div>');
                        })
                        $("#zc_div").html(arr.join(''));
                        $("#zc_div").show();
                    }
                } else {
                    layer.alert(data.Message);
                }
            })
        }

    };
})()