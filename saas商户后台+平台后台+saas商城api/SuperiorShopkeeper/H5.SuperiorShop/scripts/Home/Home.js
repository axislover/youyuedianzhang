﻿
$(function () {
    var said = ToolManager.Common.UrlParms("said");
    var spid = ToolManager.Common.UrlParms("spid");
    new Vue({
        el: '#layout_view',
        data: {
            banners: [],
            hots: [],
            commands: [],
            menus: [],
            specialPlace: [],
            specialArea: [],
            words: [],
            keyWord: ""
        },
        //挂载后
        mounted: function () {
            //this.initHomeData();
            this.initBanners();
            this.initCommandProducts();
            this.initHotProducts();
            this.initMenuList();
            this.initSpecialPlace();
            this.initSpecialArea();
            this.initWords();
            $(document).on('keypress', '#searchInput', function (e) {
                var keycode = e.keyCode;
                if (keycode == '13') {
                    e.preventDefault();
                    var keyword = $.trim($("#searchInput").val());
                    if (keyword == "")
                        return false;
                    window.location.href = "/Product/MenuList?menuid=0&spid=" + spid + "&said=" + said + "&keyword=" + keyword;

                }
            });

        },
        watch: {
            'banners': function () {
                this.$nextTick(function () {

                    $(".swiper-banner").swiper({
                        loop: true,
                        autoplay: 3000
                    });
                    var height = $(".swiper-wrapper").height();
                    $(".swiper-slide img").each(function () {
                        $(this).css("height", height + "px");
                    })
                })
            }
        },
        //定义方法
        methods: {
            btnSearch: function () {
                var _keyWord = $.trim(this.keyWord);
                if (_keyWord == "")
                    return false;
                window.location.href = "/Product/MenuList?menuid=0&spid=" + spid + "&said=" + said + "&keyword=" + _keyWord;
            },
            spanSearch: function (e) {
                var word = e.target.getAttribute('data-hot');
                window.location.href = "/Product/MenuList?menuid=0&spid=" + spid + "&said=" + said + "&keyword=" + word;
            },
            jumpMenu: function (e) {
                var menuid = e.target.getAttribute('data-menuid');
                window.location.href = "/Product/MenuList?menuid=" + menuid + "&spid=" + spid + "&said=" + said + "&keyword=";
            },
            initSpecialPlace: function () {
                var that = this;
                $.get("/Product/GetSpecialPlaceData?said=" + said + "&spid=" + spid + "&type=1&r=" + new Date().getTime(), function (data) {
                    if (data.IsSuccess) {

                        if (data.Data.length > 0) {
                            that.specialPlace = data.Data;
                        }

                    } else {
                        layer.alert(data.Message);
                    }
                })
            },
            initSpecialArea: function () {
                var that = this;
                $.get("/Product/GetSpecialPlaceData?said=" + said + "&spid=" + spid + "&type=2&r=" + new Date().getTime(), function (data) {
                    if (data.IsSuccess) {

                        if (data.Data.length > 0) {
                            that.specialArea = data.Data;
                        }

                    } else {
                        layer.alert(data.Message);
                    }
                })
            },
            initMenuList: function () {
                var that = this;
                $.get("/Home/GetMenuList?said=" + said + "&spid=" + spid + "&r=" + new Date().getTime(), function (data) {
                    if (data.IsSuccess) {
                        if (data.Data.length > 0) {
                            that.menus = data.Data;
                        }

                    } else {
                        layer.alert(data.Message);
                    }
                })
            },
            initWords: function () {
                var that = this;
                $.get("/Home/GetWords?said=" + said + "&spid=" + spid + "&r=" + new Date().getTime(), function (data) {
                    if (data.IsSuccess) {
                        if (data.Data.length > 0) {
                            that.words = data.Data;
                        }

                    } else {
                        layer.alert(data.Message);
                    }
                })
            },
            initHomeData: function () {

                var that = this;
                $.get("/Home/GetHomeData?said=" + said + "&spid=" + spid + "&r=" + new Date().getTime(), function (data) {
                    if (data.IsSuccess) {
                        that.mallIndexData = data.Data;

                    } else {
                        layer.alert(data.Message);
                    }
                })
            },
            initBanners: function () {
                var that = this;
                $.get("/Home/GetBanners?said=" + said + "&spid=" + spid + "&r=" + new Date().getTime(), function (data) {
                    if (data.IsSuccess) {
                        that.banners = data.Data;

                    } else {
                        layer.alert(data.Message);
                    }
                })
            },
            initHotProducts: function () {
                var that = this;
                $.get("/Home/GetHotProducts?said=" + said + "&spid=" + spid + "&r=" + new Date().getTime(), function (data) {
                    if (data.IsSuccess) {
                        that.hots = data.Data;
                    } else {
                        layer.alert(data.Message);
                    }
                })
            },
            initCommandProducts: function () {
                var that = this;
                $.get("/Home/GetCommandProducts?said=" + said + "&spid=" + spid + "&r=" + new Date().getTime(), function (data) {
                    if (data.IsSuccess) {
                        that.commands = data.Data;
                    } else {
                        layer.alert(data.Message);
                    }
                })
            }
        }
    })
})


    //HomeClass = {};
    //var said = ToolManager.Common.UrlParms("said");
    //var spid = ToolManager.Common.UrlParms("spid");
    //HomeClass.Instance = {
    //    Init: function () {
    //        $(".swiper-banner").swiper({
    //            loop: true,
    //            autoplay: 3000
    //        });
    //        //让轮播图片自适应高度
    //        $(function () {
    //            var height = $(".swiper-wrapper").height();
    //            $(".swiper-slide img").each(function () {
    //                $(this).css("height", height + "px");
    //            })
    //            HomeClass.Instance.InitSpecialPlace();
    //        })
    //        $(document).on('keypress', '#searchInput', function (e) {
    //            var keycode = e.keyCode;
    //            if (keycode == '13') {
    //                e.preventDefault();
    //                var keyword = $.trim($("#searchInput").val());
    //                if (keyword == "")
    //                    return false;
    //                window.location.href = "/Product/MenuList?menuid=0&spid=" + spid + "&said=" + said + "&keyword=" + keyword;

    //            }
    //        });
    //    },
    //    InitSpecialPlace: function () {

    //        $.get("/Product/GetSpecialPlaceData?said=" + said +"&spid="+spid+"&r=" + new Date().getTime(), function (data) {
    //            if (data.IsSuccess) {
    //                if (data.Data.length > 0) {
    //                    var arr = [];
    //                    data.Data.forEach((value, index) => {
    //                        arr.push('<div class="meui-guang">');
    //                        arr.push('<a href="/Product/MenuList?menuid=' + value.MenuId + '&spid='+spid+'&said='+said+'"><img style="height: 130px;" src="' + value.ImgPath + '" alt="" /></a>');
    //                        arr.push('</div>');
    //                    })
    //                    $("#zc_div").html(arr.join(''));
    //                    $("#zc_div").show();
    //                }

    //            } else {
    //                layer.alert(data.Message);
    //            }
    //        })
    //    }

    //};
