﻿using Redis;
using SuperiorModel;
using SuperiorShopBussinessService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace H5.SuperiorShop.Controllers
{
    [CheckLoginFitler]
    [JsonException]
    public class HomeController : BaseController
    {
        private readonly IProductService _IProductService;
        private readonly IShopAppService _IShopAppService;
        private readonly ISearchWordService _ISearchWordService;
        private readonly IDecorateService decorateService;

        public HomeController(IProductService IProductService, IShopAppService IShopAppService, ISearchWordService ISearchWordService, IDecorateService decorateService)
        {
            _IProductService = IProductService;
            _IShopAppService = IShopAppService;
            _ISearchWordService = ISearchWordService;
            this.decorateService = decorateService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IProductService.Dispose();
            this._IShopAppService.Dispose();
            this._ISearchWordService.Dispose();
            this.decorateService.Dispose();
            base.Dispose(disposing);
        }

        [CheckUrlFitler]
        [CheckShopStatus]
        // GET: Home
        public ActionResult Index()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            var decorate = decorateService.GetDecorateModel(parms.Said);
            var headerDesignItem = decorate.DecorateDesign.FirstOrDefault(p => p.Name == "head");
            ViewBag.Decorate = decorate;
            ViewBag.HeaderDesignItem = headerDesignItem;
            return View();
        }

        /// <summary>
        /// 获取首页数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetHomeData()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<Api_HomeDataModel> fucc = delegate ()
            {
                return _IProductService.Api_GetHomeData(parms.Spid);
            };
            var res = DataIntegration.GetData<Api_HomeDataModel>("GetHomeData_" + parms.Said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取轮播图数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetBanners()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetBanners(parms.Said);
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetBanners_" + parms.Said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取热门商品数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetHotProducts()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetHotProducts(parms.Said);
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetHotProducts_" + parms.Said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取推荐商品数据
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetCommandProducts()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<List<Api_ProductItem>> fucc = delegate ()
            {
                return _IProductService.Api_GetCommandProducts(parms.Said);
            };
            var res = DataIntegration.GetData<List<Api_ProductItem>>("GetCommandProducts_" + parms.Said, fucc, 1);
            return Success(res);
        }

        /// <summary>
        /// 获取目录
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetMenuList()
        {
            var parms = base.GetParms();
            if (parms.Spid == 0)
                return Error("非法参数");
            Func<List<Api_MenuItemModel>> fuccMenu = delegate ()
            {
                return _IProductService.Api_GetMenuList(parms.Said);
            };
            var menuList = DataIntegration.GetData<List<Api_MenuItemModel>>("GetMenuList_" + parms.Said, fuccMenu);
            return Success(menuList);
        }

        /// <summary>
        /// 获取搜索词列表
        /// </summary>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetWords(string said)
        {
            var _said = base.ValidataParms(said);
            if (_said == null)
                return Error("非法参数");
            Func<List<SearchWordModel>> fucc = delegate ()
            {
                return _ISearchWordService.GetList(_said.ToInt());
            };
            var res = DataIntegration.GetData<List<SearchWordModel>>("GetSearchKeyword_" + _said, fucc, 24);
            res = res.Where(p => p.OptionStatus == 1).ToList();
            return Success(res);
        }
    }
}