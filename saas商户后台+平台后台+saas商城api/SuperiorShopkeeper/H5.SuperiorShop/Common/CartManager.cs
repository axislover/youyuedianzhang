﻿using System;
using System.Collections.Generic;
using Redis;
using SuperiorModel;

namespace H5.SuperiorShop
{
    public class CartManager
    {
        private CartManager() { }
        private static CartManager _instance = null;
        private static readonly object locker = new object();
        public static CartManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (locker)
                    {
                        if (_instance == null)
                        {
                            _instance = new CartManager();
                        }
                    }
                }
                return _instance;
            }
        }
        /// <summary>
        /// 获取购物车数量
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public int Count(string key)
        {
            return this.GetList(key).Count;
        }
        /// <summary>
        /// 读取购物车缓存
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<CartItem> GetList(string key)
        {
            var res = RedisManager.Get<List<CartItem>>(key);
            if (res == null)
                res = new List<CartItem>();
            return res;
        }
        /// <summary>
        /// 设置/覆盖购物车数据
        /// </summary>
        /// <param name="key"></param>
        /// <param name="val"></param>
        public void Set(string key, List<CartItem> val)
        {
            RedisManager.Set<List<CartItem>>(key, val,DateTime.Now.AddMonths(1));
        }


    }
}