﻿(function () {
    QuestionListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 } };
    QuestionListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });
           
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, QuestionListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
        },
  

        Search: function () {

            _searchCriteria.ShopName = $("#search_ShopName").val();
            _searchCriteria.AppType = $("#search_AppType").val();
            _searchCriteria.LoginName = $.trim($("#search_LoginName").val());
            QuestionListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/Question/QuestionList"; 
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post(url, _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()