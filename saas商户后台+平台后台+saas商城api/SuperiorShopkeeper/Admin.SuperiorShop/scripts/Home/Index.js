﻿(function () {
    IndexClass = {};
    IndexClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });
            this.CreateUserCountChart();
            this.CreateTypeUserCountChart();
            this.CreateSallChart();
        },
        CreateUserCountChart: function () {
            $.get("/Home/GetAdminAddUserChart?r=" + new Date().getTime(), function (data) {
                if (data.IsSuccess) {
                    var myChart = echarts.init(document.getElementById('addUserCountChart'));
                    var option = {
                        color: ['#3398DB'],
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                                type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                            }
                        },
                        grid: {
                            left: '3%',
                            right: '4%',
                            bottom: '3%',
                            containLabel: true
                        },
                        xAxis: [
                            {
                                type: 'category',
                                data: data.Data.DateList,
                                axisTick: {
                                    alignWithLabel: true
                                }
                            }
                        ],
                        yAxis: [
                            {
                                type: 'value'
                            }
                        ],
                        series: [
                            {
                                name: '用户增长',
                                type: 'bar',
                                barWidth: '60%',
                                data: data.Data.DataList
                            }
                        ]

                    }
                    myChart.setOption(option);

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        CreateTypeUserCountChart: function () {
            $.get("/Home/GetAdminTypeUserChart?r=" + new Date().getTime(), function (data) {
                if (data.IsSuccess) {
                    var myChart = echarts.init(document.getElementById('typeUserCountChart'));
                    var option = {
                        tooltip: {
                            trigger: 'item',
                            formatter: "{a} <br/>{b} : {c} ({d}%)"
                        },
                        legend: {
                            orient: 'vertical',
                            left: 'left',
                            data: ['小程序', 'H5']
                        },
                        series: [
                            {
                                name: '当前用户量',
                                type: 'pie',
                                radius: '55%',
                                center: ['50%', '60%'],
                                data: [
                                    { value: data.Data.XcxCount, name: '小程序' },
                                    { value: data.Data.H5Count, name: 'H5' }
                                ],
                                itemStyle: {
                                    emphasis: {
                                        shadowBlur: 10,
                                        shadowOffsetX: 0,
                                        shadowColor: 'rgba(0, 0, 0, 0.5)'
                                    }
                                }
                            }
                        ]
                    };
                    myChart.setOption(option);

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        CreateSallChart: function () {
            $.get("/Home/GetAdminSallChart?r=" + new Date().getTime(), function (data) {
                if (data.IsSuccess) {
                    var myChart = echarts.init(document.getElementById('sallPriceChart'));
                    var myChart1 = echarts.init(document.getElementById('sallCountChart'));
                    var option = {
                        tooltip: {
                            trigger: 'axis'
                        },
                        xAxis: {
                            type: 'category',
                            data: data.Data.DateList
                        },
                        yAxis: {
                            type: 'value'
                        },
                        series: [{
                            name: '销售额',
                            data: data.Data.OrderAmountList,
                            type: 'line',
                            smooth: true
                        }]
                    };
                    var option1 = {
                        tooltip: {
                            trigger: 'axis'
                        },
                        xAxis: {
                            type: 'category',
                            data: data.Data.DateList
                        },
                        yAxis: {
                            type: 'value'
                        },
                        series: [{
                            name: '销售量',
                            data: data.Data.OrderCountList,
                            type: 'line',
                            smooth: true
                        }]
                    };
                    myChart.setOption(option);
                    myChart1.setOption(option1);
                } else {
                    layer.alert(data.Message);
                }
            })
        }

    };
})()