﻿(function () {
    ShopAppListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 20 }, AppType:999 };
    ShopAppListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });
            layui.use('laydate', function () {
                var laydate = layui.laydate;
                laydate.render({
                    elem: '#search_BeginTime', //指定元素
                    type: 'datetime'
                    , theme: 'grid'
                });
                laydate.render({
                    elem: '#search_EndTime', //指定元素
                    type: 'datetime'
                    , theme: 'grid'
                });
            });
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, ShopAppListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
            $(document).on('click', '.optionStatus', this.OptionStatus);
            $(document).on('click', '.showUrl', this.ShowUrl);
            $(document).on('click', '.showPriview', this.Priview);
        },
        Priview:function(){
            var spid = $(this).attr("Spid");
            var said = $(this).attr("Said");
            layer.open({
                type: 2,
                title: '预览',
                shadeClose: true,
                shade: 0.8,
                area: ['380px', '90%'],
                content: 'http://wx.youyue1118.com/Priveiw/Index?spid='+spid+'&said='+said
            });
        },
        ShowUrl: function () {
            var url = $(this).attr("url");
            layer.alert(url);
        },
        OptionStatus: function () {
            var id = $(this).attr("spid");
            var opt = $(this).attr("opt");
            var index = layer.load(1);
            $.get("/Shop/OptionShopApp?id=" + id + "&optionStatus=" + opt + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/Shop/ShopAppList";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
  
        Search: function () {

            _searchCriteria.ShopName = $("#search_ShopName").val();
            _searchCriteria.AppType = $("#search_AppType").val();
            _searchCriteria.LoginName = $.trim($("#search_LoginName").val());
  
            _searchCriteria.StartTime = $("#search_BeginTime").val();
            _searchCriteria.EndTime = $("#search_EndTime").val();
            ShopAppListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/Shop/ShopAppList";
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Shop/ShopAppList", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()