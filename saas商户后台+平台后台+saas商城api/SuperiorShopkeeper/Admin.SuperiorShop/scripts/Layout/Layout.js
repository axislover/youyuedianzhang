﻿(function () {
    LayoutClass = {};
    LayoutClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_exit', this.Exit);
        },
        //退出
        Exit: function () {
            $.get("/Login/Exist?tm="+new Date().getSeconds(), null, function (data) {
                if (data.IsSuccess)
                    window.location.href = "/Login/Login";
                else
                    layer.alert(data.Message);
            })
        }
    };
})()