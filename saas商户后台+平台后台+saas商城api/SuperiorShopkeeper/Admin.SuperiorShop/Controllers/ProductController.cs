﻿using SuperiorShopBussinessService;
using System.Linq;
using System.Web.Mvc;
using SuperiorCommon;
using SuperiorModel;

namespace Admin.SuperiorShop.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class ProductController : BaseController
    {
        private readonly IProductService _IProductService;
        private readonly ISetService _ISetService;

        public ProductController(IProductService IProductService, ISetService ISetService)
        {
            _IProductService = IProductService;
            _ISetService = ISetService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IProductService.Dispose();
            this._ISetService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 商户商品分类页面
        /// </summary>
        /// <returns></returns>
        public ActionResult Menu()
        {
            var criteria = new AdminMenuCriteria() { OptionStatus=999,PagingResult=new PagingResult(0,20)};
            var model = GetTpagedMenuModel(criteria);
            return View(model);
        }
        /// <summary>
        /// 搜索分类
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        [AjaxOnly]
        public ActionResult Menu(AdminMenuCriteria criteria)
        {
            var model = GetTpagedMenuModel(criteria);
            return PartialView("_Menu",model);
        }
        private TPagedModelList<AdminMenuModel> GetTpagedMenuModel(AdminMenuCriteria criteria)
        {
            var query = _IProductService.SearchAdminMenu(criteria);
            return new TPagedModelList<AdminMenuModel>(query, query.PagingResult);
        }

        /// <summary>
        /// 商品列表页面
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult ProductList()
        {
            var criteria = new ProductCriteria() { PagingResult = new PagingResult(0, 20),  OptionStatus = 999 };
            var model = GetTpageProductModel(criteria);
            return View(model);
        }
        /// <summary>
        /// 条件查询商品列表
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult ProductList(ProductCriteria criteria)
        {
            var model = GetTpageProductModel(criteria);
            return PartialView("_ProductList", model);
        }

        /// <summary>
        /// 通用获取商品列表分页模型
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public TPagedModelList<ProductModel> GetTpageProductModel(ProductCriteria criteria)
        {
            var query = _IProductService.ProductList(criteria);
            return new TPagedModelList<ProductModel>(query, query.PagingResult);
        }

        /// <summary>
        /// 商品页面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ProductEdit(string id)
        {
            var _id = base.ValidataParms(id);
            if (_id == null)
                return Error("请不要随意篡改参数错误");
            var model = _IProductService.GetProduct(_id.ToInt());
            var MenuList = _IProductService.MenuList(model.ShopAdminId);
            var ships = _ISetService.GetShippingTemplateList(model.ShopAdminId);
            MenuList = MenuList.Where(i => i.OptionStatus == 1).ToList();
            ViewBag.Ships = ships;
            ViewBag.MenuList = MenuList;
            return View(model);
        }

        /// <summary>
        /// 商品sku设置页面
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ProductSku(string id,int shopAdminId)
        {
            var _id = base.ValidataParms(id);
            if (_id == null)
                return Error("请不要随意篡改参数错误");
            var model = _IProductService.GetProductSkuManagerModel(shopAdminId, _id);
            return View(model);
        }
        /// <summary>
        /// 获取多规格数据业务模型
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult GetMoreSkuManager(string id, int shopAdminId)
        {
            var _id = base.ValidataParms(id);
            if (_id == null)
                return Error("请不要随意篡改参数错误");
            var model = _IProductService.GetProductSkuManagerModel_AJAX(shopAdminId, _id);
            return Success(model);
        }

    }
}