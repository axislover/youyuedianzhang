﻿using System.Web.Mvc;
using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;
using Redis;

namespace Admin.SuperiorShop.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class ShopController : BaseController
    {
        private readonly IShopAdminService _IShopAdminService;
        private readonly IShopAppService _IShopAppService;
        private readonly IShopService _IShopService;

        public ShopController(IShopAdminService IShopAdminService, IShopAppService IShopAppService, IShopService IShopService)
        {
            _IShopAdminService = IShopAdminService;
            _IShopAppService = IShopAppService;
            _IShopService = IShopService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IShopAdminService.Dispose();
            this._IShopAppService.Dispose();
            this._IShopService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Shop
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 商户管理页面
        /// </summary>
        /// <returns></returns>
        public ActionResult ShopList()
        {
            var criteria = new ShopAdminCriteria() { OptionStatus = 999, PagingResult = new PagingResult(0, 20) };
            var model = GetShopListPagedListModel(criteria);
            return View(model);
        }
        /// <summary>
        /// 商户管理页面搜索
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult ShopList(ShopAdminCriteria criteria)
        {
            var model = GetShopListPagedListModel(criteria);
            return PartialView("_ShopList",model);
        }
        private TPagedModelList<ShopInfoModel> GetShopListPagedListModel(ShopAdminCriteria criteria)
        {
            var query = _IShopAdminService.GetShopAdminList(criteria);
            return new TPagedModelList<ShopInfoModel>(query,query.PagingResult);
        }
        /// <summary>
        /// 操作商户状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="optionStatus"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult OptionShop(int id, int optionStatus)
        {
            _IShopAdminService.OptionShopAdminStatus(id,optionStatus);
            return Success(true);
        }

        /// <summary>
        /// 操作商城状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="optionStatus"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult OptionShopApp(int id, int optionStatus)
        {
            _IShopAppService.OptionShopAppStatus(id, optionStatus);
            //控制redis
            RedisManager.Remove("GetShopAppModel_" + id);
            return Success(true);
        }

        /// <summary>
        /// 商户详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ShopAdminDetail(int id)
        {
            var model = _IShopAdminService.GetShopAdminModel(id);
            return View(model);
        }
        /// <summary>
        /// 商城列表
        /// </summary>
        /// <returns></returns>
        public ActionResult ShopAppList()
        {
            var criteria = new ShopAppCriteria() { PagingResult=new PagingResult(0,20),AppType=999};
            var model = GetShopAppTpageModel(criteria);
            return View(model);
        }
        /// <summary>
        /// 搜索商城列表
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult ShopAppList(ShopAppCriteria criteria)
        {
            var model = GetShopAppTpageModel(criteria);
            return PartialView("_ShopAppList", model);
        }

        private TPagedModelList<ShopAppModel> GetShopAppTpageModel(ShopAppCriteria criteria)
        {
            var query = _IShopAppService.ShopAppList(criteria);
            return new TPagedModelList<ShopAppModel>(query, query.PagingResult);

        }
        /// <summary>
        /// 小程序商城详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ShopAppDetail(string id)
        {
            var model = _IShopAppService.GetShopAppModel(SecretClass.DecryptQueryString(id).ToInt());
            return View(model);
        }
        /// <summary>
        /// H5商城详情
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult ShopAppDetail_H5(string id)
        {
            var model = _IShopAppService.GetShopAppModel(SecretClass.DecryptQueryString(id).ToInt());
            return View(model);
        }

        /// <summary>
        /// 充值记录
        /// </summary>
        /// <returns></returns>
        public ActionResult RechargeList()
        {
            var criteria = new RechareOrderCriteria() { PagingResult = new PagingResult(0, 20), OrderStatus = 999, AppType = 999, SallType = 999 };
            var model = GetRechargeOrderPagedListManager(criteria);
            return View(model);
        }
        /// <summary>
        /// 搜索充值记录
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult RechargeList(RechareOrderCriteria criteria)
        {
            var model = GetRechargeOrderPagedListManager(criteria);
            return PartialView("_RechargeList", model);
        }
        private TPagedModelList<RechargeOrderModel> GetRechargeOrderPagedListManager(RechareOrderCriteria criteria)
        {
            var query = _IShopAdminService.SearchRechareOrderPageList(criteria);
            return new TPagedModelList<RechargeOrderModel>(query, query.PagingResult);
        }

        /// <summary>
        /// 提现管理列表
        /// </summary>
        /// <returns></returns>
        public ActionResult CashOutList()
        {
            var criteria = new ShopCashOutCriteria() { PagingResult = new PagingResult(0, 15), OrderStatus = 999 };
            var model = GetCashOutPagedModel(criteria);
            return View(model);
        }


        private TPagedModelList<ShopCashOutModel> GetCashOutPagedModel(ShopCashOutCriteria criteria)
        {
            var query = _IShopService.SearchShopCashOutList(criteria);
            return new TPagedModelList<ShopCashOutModel>(query, query.PagingResult);
        }

        [HttpPost]
        [AjaxOnly]
        public ActionResult CashOutList(ShopCashOutCriteria criteria)
        {
            var model = GetCashOutPagedModel(criteria);
            return PartialView("_CashOutList", model);
        }
        /// <summary>
        /// 人工更改提现订单状态为成功
        /// </summary>
        /// <param name="orderno"></param>
        /// <returns></returns>
        [AjaxOnly]
        public JsonResult OptionOrder(string orderno)
        {
            _IShopService.UpdateShopCashCoutOder(orderno,1);
            return Success(true);
        }
    }
}