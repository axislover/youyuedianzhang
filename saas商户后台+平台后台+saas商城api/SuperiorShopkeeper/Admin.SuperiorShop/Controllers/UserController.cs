﻿using SuperiorShopBussinessService;
using System.Web.Mvc;
using SuperiorModel;

namespace Admin.SuperiorShop.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class UserController : BaseController
    {
        private readonly IUserService _IUserService;
        private readonly IShopAppService _IShopAppService;

        public UserController(IUserService IUserService, IShopAppService IShopAppService)
        {
            _IUserService = IUserService;
            _IShopAppService = IShopAppService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IUserService.Dispose();
            this._IShopAppService.Dispose();
            base.Dispose(disposin);
        }
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 用户列表管理页面
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult List()
        {
            var criteria = new UserCriteria() { AppType = 999, OptionStatus = 999, PagingResult = new PagingResult(0, 30) };
            var model = GetUserListManager(criteria);
            return View(model);
        }

        private TPagedModelList<UserListItem> GetUserListManager(UserCriteria criteria)
        {
            criteria.ShopAppId = "unChecke";
            var query = _IUserService.Search(criteria);
            return new TPagedModelList<UserListItem>(query, query.PagingResult);
        }
        /// <summary>
        /// 用户列表，搜索，分页请求
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult List(UserCriteria criteria)
        {
            var model = GetUserListManager(criteria);
            return PartialView("_List", model);
        }
    }
}