﻿using System.Web.Mvc;
using SuperiorModel;
using SuperiorShopBussinessService;

namespace Admin.SuperiorShop.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class QuestionController : BaseController
    {
        private readonly IQuestionService _IQuestionService;

        public QuestionController(IQuestionService IQuestionService)
        {
            _IQuestionService = IQuestionService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IQuestionService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Question
        /// <summary>
        ///投诉列表页面
        /// </summary>
        /// <returns></returns>
        public ActionResult QuestionList()
        {
            var criteria = new QuestionCriteira() { AppType = 999, PagingResult = new PagingResult(0, 20) };
            var model = GetQuestionListPagedListModel(criteria);
            return View(model);
        }
        /// <summary>
        /// 投诉列表页面搜索
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult QuestionList(QuestionCriteira criteria)
        {
            var model = GetQuestionListPagedListModel(criteria);
            return PartialView("_List", model);
        }
        private TPagedModelList<QuestionModel> GetQuestionListPagedListModel(QuestionCriteira criteria)
        {
            var query = _IQuestionService.SearchQuestionList(criteria);
            return new TPagedModelList<QuestionModel>(query, query.PagingResult);
        }


    }
}