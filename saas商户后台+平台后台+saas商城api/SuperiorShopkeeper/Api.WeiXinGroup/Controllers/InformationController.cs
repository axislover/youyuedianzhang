﻿using Redis;
using SuperiorCommon;
using SuperiorModel;
using SuperiorModel.WeiXinGroup.Api;
using SuperiorShopBussinessService;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace Api.WeiXinGroup.Controllers
{
    [ApiExceptionAttribute]
    public class InformationController : ApiControllerBase
    {
        private readonly IGroupInformationService _IGroupInformationService;
        private readonly IGroupTypeService _IGroupTypeService;
        public InformationController(IGroupInformationService IGroupInformationService, IGroupTypeService IGroupTypeService)
        {
            _IGroupInformationService = IGroupInformationService;
            _IGroupTypeService = IGroupTypeService;
        }
        protected override void Dispose(bool disposing)
        {
            this._IGroupInformationService.Dispose();
            this._IGroupTypeService.Dispose();
            base.Dispose(disposing);
        }
        // GET: Information

        /// <summary>
        /// 创建信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> Create(Api_InformationRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ErrorMsg());
            var _userid = base.ValidataParms(model.UserId);
            if (_userid == null)
                return Error("非法参数");
            model.UserId = _userid;
            if (!SuperiorCommon.ValidateUtil.IsValidMobile(model.PhoneNumber))
                return Error("请输入正确的手机号");
            //将图上传的图片服务器
            if (!UploadService.Upload(model.Images, UploaderKeys.InformationImage))
                return Error("上传图片失败");
            string msg = string.Empty;
            var res = _IGroupInformationService.Api_Create(model, out msg);
            if (res != 1)
                return Error(msg);
            return Success(res);
        }

        /// <summary>
        /// 获取信息列表数据
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> List(Api_InformationListCriteria criteria)
        {
            if (!string.IsNullOrEmpty(criteria.UserId))
            {
                var _userid = base.ValidataParms(criteria.UserId);
                if (_userid == null)
                    return Error("非法参数");
                criteria.UserId = _userid;
            }
            var res = _IGroupInformationService.Api_GetList(criteria);
            return Success(res); 
        }
        /// <summary>
        /// 读取用户收藏的信息列表
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> CollectedList(Api_GroupInformationCollectListCriteria criteria)
        {
            var _userid = base.ValidataParms(criteria.UserId);
            if (_userid == null)
                return Error("非法参数");
            criteria.UserId = _userid;
            var res = _IGroupInformationService.Api_GetCollectedList(criteria);
            return Success(res);
        }

        /// <summary>
        /// 获取推荐信息列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> RecommendList(int id)
        {
            Func<List<Api_InformationItem>> func = () =>
            {
                return _IGroupInformationService.Api_GetRecommendList(id);
            };
            var res = DataIntegration.GetData<List<Api_InformationItem>>("Information_Recommend_" + id, func);
            return Success(res);
        }
        /// <summary>
        /// 根据ID获取单个信息详情数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ApiResultModel<object> Get(int id)
        {
            Func<Api_InformationInfoModel> fucc = delegate ()
            {
                return _IGroupInformationService.Api_Get(id);
            };
            var res = DataIntegration.GetData<Api_InformationInfoModel>("Information_Info_" + id, fucc);
            return Success(res);
        }
        /// <summary>
        /// 更新信息统计数据
        /// </summary>
        /// <param name="type"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ApiResultModel<object> UpdateData(int type, int id)
        {
            _IGroupInformationService.Api_UpdateData(type, id);
            return Success(true);
        }
        /// <summary>
        /// 获取分类列表
        /// </summary>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ApiResultModel<object> TypeList()
        {
            Func<List<GroupTypeModel>> func = () =>
            {
                return _IGroupTypeService.List();
            };
            var res = DataIntegration.GetData<List<GroupTypeModel>>("GroupTypeList", func);
            return Success(res);
        }
        /// <summary>
        /// 获取置顶数据列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ApiResultModel<object> TopConfigList()
        {
            Func<List<Api_TopConfigModel>> func = () =>
            {
                return _IGroupInformationService.GetTopConfigList();
            };
            var res = DataIntegration.GetData<List<Api_TopConfigModel>>("TopConfigList", func);
            return Success(res);
        }
        /// <summary>
        /// 设置信息置顶
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> SetTop(Api_SetTopRequestModel model)
        {
            if (!ModelState.IsValid)
                return Error(ErrorMsg());
            var _userid = base.ValidataParms(model.UserId);
            if (_userid == null)
                return Error("非法参数");
            var msg = string.Empty;
            var ret = _IGroupInformationService.Api_SetTop(_userid.ToInt(),model.InformationId,model.TopConfigId,out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }

    }
}