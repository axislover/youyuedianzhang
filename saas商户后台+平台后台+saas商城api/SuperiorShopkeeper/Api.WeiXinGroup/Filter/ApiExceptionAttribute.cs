﻿using SuperiorCommon;
using SuperiorModel;
using System;
using System.Net.Http;
using System.Web.Http.Filters;

namespace Api.WeiXinGroup
{
    [AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = true)]
    public class ApiExceptionAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            LogManger.Instance.WriteLog(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "——" +
                actionExecutedContext.Exception.GetType().ToString() + "：" + actionExecutedContext.Exception.Message + "——堆栈信息：" +
                actionExecutedContext.Exception.StackTrace);

            //2.返回调用方具体的异常信息
            actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse<ApiResultModel<string>>(ApiResultModel<string>.Conclude(ApiResultEnum.Error, "", "请求异常"));

            base.OnException(actionExecutedContext);
        }
    }
}