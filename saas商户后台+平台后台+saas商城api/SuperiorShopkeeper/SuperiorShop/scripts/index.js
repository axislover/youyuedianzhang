$('.function ul li').hover(
    function(e) {
        $(this)
            .siblings()
            .removeClass('hover')
            .end()
            .addClass('hover');
        $('.function .con div')
            .eq(e.currentTarget.dataset.index)
            .css('display', 'block')
            .siblings()
            .css('display', 'none')
    }
)
// 核心优势的hover
$('.first').hover(
    function(e) {
        $('.first')
            .css({ "height": "510px", "margin-top": "-40px" })
        $('.second')
            .css({ "height": "470px", "margin-top": "-20px" })
    },
    function(e) {
        $('.advantage>.mn>div')
            .css({ "height": "430px", "margin-top": "0px" })
    }
)
$('.second').hover(
    function(e) {
        $('.second')
            .css({ "height": "510px", "margin-top": "-40px" })
        $('.first')
            .css({ "height": "470px", "margin-top": "-20px" })
        $('.third')
            .css({ "height": "470px", "margin-top": "-20px" })
    },
    function(e) {
        $('.advantage>.mn>div')
            .css({ "height": "430px", "margin-top": "0px" })
    }
)
$('.third').hover(
    function(e) {
        $('.third')
            .css({ "height": "510px", "margin-top": "-40px" })
        $('.second')
            .css({ "height": "470px", "margin-top": "-20px" })
        $('.forth')
            .css({ "height": "470px", "margin-top": "-20px" })
    },
    function(e) {
        $('.advantage>.mn>div')
            .css({ "height": "430px", "margin-top": "0px" })
    }
)
$('.forth').hover(
    function(e) {
        $('.forth')
            .css({ "height": "510px", "margin-top": "-30px" })
        $('.third')
            .css({ "height": "470px", "margin-top": "-15px" })
        $('.fifth')
            .css({ "height": "470px", "margin-top": "-15px" })
    },
    function(e) {
        $('.advantage>.mn>div')
            .css({ "height": "430px", "margin-top": "0px" })
    }
)
$('.fifth').hover(
    function(e) {
        $('.fifth')
            .css({ "height": "510px", "margin-top": "-30px" })
        $('.forth')
            .css({ "height": "470px", "margin-top": "-15px" })
    },
    function(e) {
        $('.advantage>.mn>div')
            .css({ "height": "430px", "margin-top": "0px" })
    }
)


$('.swiper-container').hover(function() {
			$(".swiper-button-prev").show();
			$(".swiper-button-next").show();
		}, function() {
			$(".swiper-button-prev").hide();
			$(".swiper-button-next").hide();
		});