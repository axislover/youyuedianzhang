﻿(function () {
    ToolManager.Common = {
        //获取URL参数
        UrlParms: function (key) {
            var reg = new RegExp("(^|&)" + key + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
            var r = window.location.search.substr(1).match(reg); //匹配目标参数
            if (r != null) return unescape(r[2]); return null; //返回参数值
        },
        //替换URL参数
        ReplaceParamVal: function (url,arg,val) {
            var pattern = arg + '=([^&]*)';
            var replaceText = arg + '=' + val;
            return url.match(pattern) ? url.replace(eval('/(' + arg + '=)([^&]*)/gi'), replaceText) : (url.match('[\?]') ? url + '&' + replaceText : url + '?' + replaceText);
        },
        //获取随机token
        Token: function () {
            return Math.random().toString(36).substr(2);
        },
        ///从几个颜色随机获取
        GetRandomColor: function () {
            var rgb = ['#FFB800', '#FF00FF', '#8B1A1A', '#FF5722', '#1E9FFF', '#00cc00', '#99ff00', '#ff33cc', '#00ffff', '#cc3333', '#EE82EE', '#F08080', '#B8860B']
            return rgb[Math.round(Math.random() * 4)];

        },
        //将订单状态数值转成文字
        ConvertOrderStatus: function (orderStatus) {
            var res = "";
            switch (orderStatus) {
                case 0:
                    res = "待付款";
                    break;
                case 1:
                    res = "成功(已发货)";
                    break;
                case 2:
                    res = "交易失败";
                    break;
                case 3:
                    res = "待发货";
                    break;
                case 4:
                    res = "申请售后";
                    break;

            }
            return res;
        }

    }

    ToolManager.Validata = {
        //正整数
        IsPInt: function (str) {
            var g = /^[1-9]*[1-9][0-9]*$/;
            return g.test(str);
        },
        //整数
        IsInt: function (str) {
            var g = /^-?\d+$/;
            return g.test(str);
        },
        //邮箱
        IsEmail: function (str) {
            var reg = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
            return reg.test(str);
        },
        //手机号
        IsPhone: function (str) {
            var reg = /^[1][0-9]{10}$/;
            return reg.test(phoneValue);
        },
        //验证密码复杂度（必须包含数字字母）
        ValidataPwd: function (str) {
            var reg1 = /^(([0-9]{1,})([a-z]{1,}))|(([a-z]{1,})([0-9]{1,}))$/;
            var reg2 = /^(([0-9]{1,})([A-Z]{1,}))|(([A-Z]{1,})([0-9]{1,}))$/;
            str = valueTrim(str);
            if (reg1.test(str)) {
                return true;
            }
            if (reg2.test(str)) {
                return true;
            }
            return false;
        },
        //验证金额，等于0通过
        IsMoney: function (value) {
            var pattern = /(^[1-9](\d+)?(\.\d{1,2})?$)|(^0$)|(^\d\.\d{1,2}$)/;
            return pattern.test(value)
        },
        //验证大于0的数字或小数
        GreaterThanZero: function (value) {
            var r = parseFloat(value);
            return r > 0;
        }
    }

    _keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    ToolManager.Base64 = {
        Encode: function (input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = this._utf8_encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                _keyStr.charAt(enc1) + _keyStr.charAt(enc2) +
                _keyStr.charAt(enc3) + _keyStr.charAt(enc4);
            }
            return output;
        },
        Decode: function (input) {
            var output = "";
            var chr1, chr2, chr3;
            var enc1, enc2, enc3, enc4;
            var i = 0;
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
            while (i < input.length) {
                enc1 = _keyStr.indexOf(input.charAt(i++));
                enc2 = _keyStr.indexOf(input.charAt(i++));
                enc3 = _keyStr.indexOf(input.charAt(i++));
                enc4 = _keyStr.indexOf(input.charAt(i++));
                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;
                output = output + String.fromCharCode(chr1);
                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }
            }
            output = this._utf8_decode(output);
            return output;
        },
        _utf8_encode: function (string) {
            string = string.replace(/\r\n/g, "\n");
            var utftext = "";
            for (var n = 0; n < string.length; n++) {
                var c = string.charCodeAt(n);
                if (c < 128) {
                    utftext += String.fromCharCode(c);
                } else if ((c > 127) && (c < 2048)) {
                    utftext += String.fromCharCode((c >> 6) | 192);
                    utftext += String.fromCharCode((c & 63) | 128);
                } else {
                    utftext += String.fromCharCode((c >> 12) | 224);
                    utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                    utftext += String.fromCharCode((c & 63) | 128);
                }

            }
            return utftext;
        },
        _utf8_decode: function (utftext) {
            var string = "";
            var i = 0;
            var c = c1 = c2 = 0;
            while (i < utftext.length) {
                c = utftext.charCodeAt(i);
                if (c < 128) {
                    string += String.fromCharCode(c);
                    i++;
                } else if ((c > 191) && (c < 224)) {
                    c2 = utftext.charCodeAt(i + 1);
                    string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                    i += 2;
                } else {
                    c2 = utftext.charCodeAt(i + 1);
                    c3 = utftext.charCodeAt(i + 2);
                    string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                    i += 3;
                }
            }
            return string;
        }
    };
})();