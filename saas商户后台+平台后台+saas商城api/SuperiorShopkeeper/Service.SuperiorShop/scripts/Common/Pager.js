﻿
(function () {
    superior.ui.control.Pager = PagerClass;
    superior.ui.control.Pager.enablePaging = function (pagerContainer, redirectToPage) {
        $(pagerContainer).on("click", "#layui-laypage a[pageIndex]", function () {
            var pageIndex = $(this).attr("pageIndex");
            redirectToPage(pageIndex);
        });

        $(pagerContainer).on("click", ".layui-laypage-prev", function () {
            var pageIndex = getCurrentPageIndex() - 1;
            redirectToPage(pageIndex);
        });

        $(pagerContainer).on("click", ".layui-laypage-next", function () {
            var pageIndex = getCurrentPageIndex() + 1;
            redirectToPage(pageIndex);
        });
       
        $(pagerContainer).on("click", ".layui-laypage-refresh", function () {
            redirectToPage();
        });
        $(pagerContainer).on("keyup", "#pageGoIndex", function () {
            // var input = this;
            InitIpw_topage();
        });
        $(pagerContainer).on("click", "#_btn_topage", function () {
            var toPage = $("#pageGoIndex").val();
            redirectToPage(toPage - 1);
        });
        function getCurrentPageIndex() {
            return $("#currentPage", pagerContainer).text() - 1;
        }
        function InitIpw_topage() {
            var input = $("#pageGoIndex");
            $("#ipw_topage").val($("#ipw_topage").val().replace(/[^\d]/g, ''));
            var _pageIndex = parseInt(input.val());
            var _maxPage = parseInt($(input).attr("maxpage"));

            if (_pageIndex > _maxPage) {
                $("#ipw_topage").val(_maxPage);
            }
        }
    }

    function PagerClass(pagerContainer, redirectToPage) {
        var _self = this;

        function _init() {
            init(pagerContainer, redirectToPage);
        }

        function init() {
            $(pagerContainer).on("click", "#layui-laypage a[pageIndex]", function () {
                var pageIndex = $(this).attr("pageIndex");
                refresh(pageIndex);
            });

            $(pagerContainer).on("click", "#layui-laypager a[pageIndex]", function () {
                var pageIndex = $(this).attr("pageIndex");
                redirectToPage(pageIndex);
            });

            $(pagerContainer).on("click", ".layui-laypage-prev", function () {
                var pageIndex = _searchCriteria.PagingRequest.PageIndex - 1;
                redirectToPage(pageIndex);
            });

            $(pagerContainer).on("click", ".layui-laypage-next", function () {
                var pageIndex = _searchCriteria.PagingRequest.PageIndex + 1;
                redirectToPage(pageIndex);
            });
           
            $(pagerContainer).on("click", ".layui-laypage-refresh", function () {
                redirectToPage();
            });

        }

        _init();
    }
})();