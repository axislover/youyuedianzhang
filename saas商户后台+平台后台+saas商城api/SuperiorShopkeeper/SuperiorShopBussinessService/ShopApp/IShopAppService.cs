﻿using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IShopAppService:IService
    {
        PagedSqlList<ShopAppModel> ShopAppList(ShopAppCriteria criteria);
        int Add(ShopAppModel model, out string msg);
        ShopAppModel GetShopAppModel(int id);
        int Edit(ShopAppModel model, out string msg);
        bool IsExistShopByAdminId(int uid);
        string GetPayKeyByAppid(string appid);
        void OptionShopAppStatus(int id, int option);
        void UpdateVersion(int id, string version);
    }
}
