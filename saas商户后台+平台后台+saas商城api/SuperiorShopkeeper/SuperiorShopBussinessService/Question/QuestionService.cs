﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorShopDataAccess;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class QuestionService : IQuestionService
    {
        public void Dispose()
        {
            
        }

        public void Insert(QuestionParms model)
        {
            QuestionDataAccess.Insert(model);
        }

        public PagedSqlList<QuestionModel> SearchQuestionList(QuestionCriteira criteria)
        {
            var queryList = new List<QuestionModel>();
            var totalCount = 0;
            var ds = QuestionDataAccess.SearchQuestionList(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new QuestionModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            CreateTime = dr["CreateTime"].ToString(),
                            ShopAppId = Convert.ToInt32(dr["ShopAppId"]),
                            ShopAdminId = Convert.ToInt32(dr["ShopAdminId"]),
                            AppType = Convert.ToInt32(dr["AppType"]),
                            UserId = Convert.ToInt32(dr["UserId"]),
                            NickName = dr["NickName"].ToString(),
                            LoginName = dr["LoginName"].ToString(),
                            Question = dr["Question"].ToString(),
                            ShopName = dr["ShopName"].ToString(),
                         
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<QuestionModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }
    }
}
