﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorShopDataAccess;
using SuperiorCommon;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class ShopService : IShopService
    {
        public void Dispose()
        {

        }

        public List<int> GetShopAppIdsByShopAdminId(int shopAdminId)
        {
            var res = new List<int>();
            var ds = ShopDataAccess.GetShopAppIdsByShopAdminId(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(Convert.ToInt32(dr["Id"])
                        );
                    }
                }
            }
            return res;
        }

        public List<RechargeModel> GetRechargeList()
        {
            var res = new List<RechargeModel>();
            var ds = ShopDataAccess.GetRechargeList();
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new RechargeModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Title = dr["Title"].ToString(),
                            AppType = Convert.ToInt32(dr["AppType"]),
                            SallType = Convert.ToInt32(dr["SallType"]),
                            Amount = Convert.ToDecimal(dr["Amount"])
                        });
                    }
                }
            }
            return res;
        }
        public ShopInfoModel GetShopModel(string username, string password)
        {
            var res = new ShopInfoModel();
            var ds = ShopDataAccess.GetShopModel(username, password);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.LoginName = username;
                    res.PassWord = dr["PassWord"].ToString();
                    res.Introduce = dr["Introduce"].ToString();
                    res.PayType = Convert.ToInt32(dr["PayType"]);
                    res.PhoneNumber = dr["PhoneNumber"].ToString();
                    res.CanUserAmount = dr["CanUserAmount"].ToString();
                    res.QqCode = dr["QqCode"].ToString();
                    res.WxCode = dr["WxCode"].ToString();
                    res.Contact = dr["Contact"].ToString();
                    res.OptionStatus = Convert.ToInt32(dr["OptionStatus"]);
                    res.ProgrameExpireTime = DataManager.GetRowValue_DateTime(dr["ProgrameExpireTime"]);
                    res.H5ExpireTime = DataManager.GetRowValue_DateTime(dr["H5ExpireTime"]);
                    res.SerectId = SecretClass.EncryptQueryString(dr["Id"].ToString());
                }
            }
            return res;
        }
        public ShopInfoModel GetShopModel_Role(string username, string password)
        {
            var res = new ShopInfoModel();
            var ds = ShopDataAccess.GetShopModel_Role(username, password);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString());
                    res.LoginName = username;
                    res.PassWord = dr["PassWord"].ToString();
                    res.Introduce = dr["Introduce"].ToString();
                    res.PayType = Convert.ToInt32(dr["PayType"]);
                    res.PhoneNumber = dr["PhoneNumber"].ToString();
                    res.QqCode = dr["QqCode"].ToString();
                    res.WxCode = dr["WxCode"].ToString();
                    res.Contact = dr["Contact"].ToString();
                    res.OptionStatus = Convert.ToInt32(dr["AdminOptionStatus"]);
                    res.ProgrameExpireTime = DataManager.GetRowValue_DateTime(dr["ProgrameExpireTime"]);
                    res.H5ExpireTime = DataManager.GetRowValue_DateTime(dr["H5ExpireTime"]);
                    res.SerectId = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.ShopAdminType = 2;
                    res.ShopLoginRoleModel.Account = dr["Account"].ToString();
                    res.ShopLoginRoleModel.Authoritys = dr["Authoritys"].ToString();
                    res.ShopLoginRoleModel.OptionStatus = Convert.ToInt32(dr["RoleOptionStatus"]);
                }
            }
            return res;
        }

       

        public int InsertShop(ShopInfoModel model, out string msg)
        {
            model.PassWord = MD5Manager.MD5Encrypt(model.PassWord);
            var ret = ShopDataAccess.InsertShop(model);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "用户名已存在";
                    break;
                case 3:
                    msg = "手机号已经存在";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public SubmitCashOutResultModel ShopCashOutAmount(int shopAdminId, decimal amount,string orderno, out string msg)
        {
            var ret = 0;
            var ds = ShopDataAccess.ShopCashOutAmount(shopAdminId,amount,orderno,out ret);
            msg = "";
            var res = new SubmitCashOutResultModel();
            if (ret != 1)
            {
                switch (ret)
                {
                    case 2:
                        msg = "参数异常";
                        break;
                    case 3:
                        msg = "余额不足";
                        break;
                    case 4:
                        msg = "执行错误";
                        break;
                    default:
                        msg = "执行出错";
                        break;
                }
            }
            else
            {
                if (DataManager.CheckDs(ds, 1))
                {
                    if (DataManager.CheckHasRow(ds.Tables[0]))
                    {
                        var dr = ds.Tables[0].Rows[0];
                        res.ExpressAmount = dr["ExpressAmount"].ToString();
                        res.OpenId = dr["OpenId"].ToString();
                    }
                }
            }
            
            return res;
        }

        public void UpdateShopCashCoutOder(string orderno, int status)
        {
            ShopDataAccess.UpdateShopCashCoutOder(orderno,status);
        }

        public void UpdateShopAdmin(string openid, string nickname, string headImgUrl, int id)
        {
            ShopDataAccess.UpdateShopAdmin(openid, nickname, headImgUrl, id);
        }
        public PagedSqlList<ShopCashOutModel> SearchShopCashOutList(ShopCashOutCriteria criteria)
        {
            var queryList = new List<ShopCashOutModel>();
            var totalCount = 0;
            var ds = ShopDataAccess.SearchShopCashOutDs(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new ShopCashOutModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            CreateTime = dr["CreateTime"].ToString(),
                            PayTime = dr["PayTime"].ToString(),

                            OrderStatus = Convert.ToInt32(dr["OrderStatus"]),
                            NickName = dr["NickName"].ToString(),
                            OpenId = dr["OpenId"].ToString(),
                            OrderNo = dr["OrderNo"].ToString(),
                            Amount = dr["Amount"].ToString(),
                            Remark = dr["Remark"].ToString(),
                            LoginName = dr["LoginName"].ToString()
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<ShopCashOutModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }
    }
}
