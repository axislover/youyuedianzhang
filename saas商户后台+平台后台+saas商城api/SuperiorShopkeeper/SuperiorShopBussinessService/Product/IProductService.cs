﻿using System.Collections.Generic;
using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IProductService : IService
    {
        List<MenuModel> MenuList(int shopAdminId);
        int AddMenu(MenuModel model, out string msg);
        int EditMenu(MenuModel model, out string msg);

        int OptionMenu(int id, int optionStatus, int shopAdminId, out string msg);
        int DeleteMenu(int id, int shopAdminId, out string msg);

        int OptionProduct(int id, int optionStatus, int shopAdminId, out string msg);
        int DeleteProduct(int id, int shopAdminId, out string msg);

        int AddPropety(AddPropetyParamsModel model, out string msg);

        PagedSqlList<ProductModel> ProductList(ProductCriteria criteria);

        bool AddProduct(ProductModel model);

        bool EditProduct(ProductModel model);


        ProductModel GetProduct(int id);

        ProductSkuManagerModel GetProductSkuManagerModel(int shopAdminId, string productId);
        ProductSkuManagerModel GetProductSkuManagerModel_AJAX(int shopAdminId, string productId);

        bool EditSingleSku(SingleSkuModel model);
        bool EditMoreSku(ProductSkuManagerModel model, int shopAdminId);
        int AddPropetyValue(Product_Sall_Propety_ValueModel model, out string msg);
        bool DelPropetyValue(Product_Sall_Propety_ValueModel model);
        bool DelPropety(int id, string productId, int shopAdminId);
        bool AddProductBanner(ProductBannerModel model);
        bool EditProductBanner(ProductBannerModel model);
        void OptionProductBanner(int id, int optionStatus);
        void DelProductBanner(int id);
        List<ProductBannerModel> GetProductBannerList(int shopAdminId);
        ProductBannerModel GetProductBanner(int id);

        PagedSqlList<AdminMenuModel> SearchAdminMenu(AdminMenuCriteria criteria);
        List<SpecialPlaceModel> SpecialPlaceList(int shopAdminId);
        void DelSpecialPlace(int id);
        bool InsertOrEditSpecialPlace(SpecialPlaceModel model);
        SpecialPlaceModel GetSpecialPlaceModel(int id);

        #region 接口
        Api_HomeDataModel Api_GetHomeData(int shopAppId);
        List<Api_ProductItem> Api_GetBanners(int said);
        List<Api_ProductItem> Api_GetHotProducts(int said);
        List<Api_ProductItem> Api_GetCommandProducts(int said);

        List<Api_MenuItemModel> Api_GetMenuList(int shopAdminId);
        List<Api_ProductItem> Api_GetProductList(Api_ProductListCriteria criteria);
        Api_ProductDetailModel Api_GetProduct(int id);
        List<Api_CreaterOrder_CheckItem> Api_GetCreateOrder_CheckItemList(string skuids);

        List<SpecialPlaceModel> SpecialPlaceList(int shopAdminId,int type=1);
       
        #endregion
    }
}
