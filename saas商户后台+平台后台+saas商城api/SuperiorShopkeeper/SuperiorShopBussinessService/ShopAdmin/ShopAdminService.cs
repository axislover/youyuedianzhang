﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorShopDataAccess;
using SuperiorCommon;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class ShopAdminService : IShopAdminService
    {
        public void Dispose()
        {

        }

        public PagedSqlList<ShopInfoModel> GetShopAdminList(ShopAdminCriteria criteria)
        {
            var queryList = new List<ShopInfoModel>();
            var totalCount = 0;
            var ds = ShopAdminDataAccess.GetShopAdminListDs(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new ShopInfoModel()
                        {
                            Id = dr["Id"].ToString(),
                            LoginName = dr["LoginName"].ToString(),
                            Introduce = dr["Introduce"].ToString(),
                            PhoneNumber = dr["PhoneNumber"].ToString(),
                            QqCode = dr["QqCode"].ToString(),
                            WxCode = dr["WxCode"].ToString(),
                            Contact = dr["Contact"].ToString(),
                            OptionStatus = DataManager.GetRowValue_Int(dr["OptionStatus"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                            AccountManagerId = dr["AccountManagerId"].ToString()
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<ShopInfoModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public PagedSqlList<RechargeOrderModel> SearchRechareOrderPageList(RechareOrderCriteria criteria)
        {
            var queryList = new List<RechargeOrderModel>();
            var totalCount = 0;
            var ds = ShopAdminDataAccess.SearchRechareOrderDs(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new RechargeOrderModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Amount = DataManager.GetRowValue_Decimal(dr["Amount"]),
                            Title = dr["Title"].ToString(),
                            AppType = Convert.ToInt32(dr["AppType"]),
                            SallType = Convert.ToInt32(dr["SallType"]),
                            RechargeNo = dr["RechargeNo"].ToString(),
                            PayCode = dr["PayCode"].ToString(),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                            PayTime = DataManager.GetRowValue_DateTime(dr["PayTime"]),
                            OrderStatus = Convert.ToInt32(dr["OrderStatus"]),
                            LoginName = dr["LoginName"].ToString(),
                            AccountManagerId = DataManager.GetRowValue_Int(dr["AccountManagerId"]),
                            AccountMangerAmount = dr["AccountMangerAmount"].ToString()
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<RechargeOrderModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public void OptionShopAdminStatus(int id, int option)
        {
            ShopAdminDataAccess.OptionShopAdminStatus(id, option);
        }
        public ShopInfoModel GetShopAdminModel(int id)
        {
            var res = new ShopInfoModel();
            var ds = ShopAdminDataAccess.GetShopAdminModel(id);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.LoginName = dr["LoginName"].ToString();
                    res.Introduce = dr["Introduce"].ToString();
                    res.QqCode = dr["QqCode"].ToString();
                    res.WxCode = dr["WxCode"].ToString();
                    res.Contact = dr["Contact"].ToString();
                    res.OptionStatus = DataManager.GetRowValue_Int(dr["OptionStatus"]);
                    res.PhoneNumber = dr["PhoneNumber"].ToString();
                }
            }
            return res;
        }
        public AdminInfoModel GetAdminModel(string loginName, string passWord)
        {
            var res = new AdminInfoModel();
            var ds = ShopAdminDataAccess.GetAdminModel(loginName,passWord);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.LoginName = dr["LoginName"].ToString();
                }
            }
            return res;
        }
        public void EditShopAdmin(ShopInfoModel model)
        {
            ShopAdminDataAccess.EditShopAdmin(model);
        }
        public void EditPassWord(int shopAdminId, string pw)
        {
            ShopAdminDataAccess.EditPw(shopAdminId, pw);
        }

        public List<ShopRoleModel> GetRoleList(int shopAdminId)
        {
            var res = new List<ShopRoleModel>();
            var ds = ShopAdminDataAccess.GetRoleListDs(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new ShopRoleModel()
                        {
                            RoleName = dr["RoleName"].ToString(),
                            Authoritys = dr["Authoritys"].ToString(),
                            Id = Convert.ToInt32(dr["Id"])
                        });
                    }
                }
            }
            return res;
        }

        public ShopRoleModel GetRole(int id)
        {
            var res = new ShopRoleModel();
            var ds = ShopAdminDataAccess.GetRoleDs(id);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.RoleName = dr["RoleName"].ToString();
                    res.Authoritys = dr["Authoritys"].ToString();
                    res.Id = Convert.ToInt32(dr["Id"]);
                }
            }
            return res;
        }
        public int AddShopRole(ShopRoleModel model, out string msg)
        {
            var ret = ShopAdminDataAccess.AddShopRole(model);
            switch (ret)
            {
                case 2:
                    msg = "角色名称已存在";
                    break;
                default:
                    msg = "";
                    break;
            }
            return ret;
        }
        public int EditShopRole(ShopRoleModel model, out string msg)
        {
            var ret = ShopAdminDataAccess.EditShopRole(model);
            switch (ret)
            {
                case 2:
                    msg = "角色名称已存在";
                    break;
                default:
                    msg = "";
                    break;
            }
            return ret;
        }
        public int DelShopRole(int id, out string msg)
        {
            var ret = ShopAdminDataAccess.DelShopRole(id);
            switch (ret)
            {
                case 2:
                    msg = "当前角色正在被员工使用";
                    break;
                default:
                    msg = "";
                    break;
            }
            return ret;
        }

        public void SetShopRoleAuthority(ShopRoleModel model)
        {
            ShopAdminDataAccess.SetShopRoleAuthority(model);
        }

        public void DelShopRoleAccount(int id)
        {
            ShopAdminDataAccess.DelShopRoleAccount(id);
        }
        public void OptionShopRoleAccount(int id, int option)
        {
            ShopAdminDataAccess.OptionShopRoleAccount(id, option);
        }
        public int EditShopRoleAccount(ShopRoleAccountModel model, out string msg)
        {
            var ret = ShopAdminDataAccess.EditShopRoleAccount(model);
            switch (ret)
            {
                case 2:
                    msg = "已存在账号";
                    break;
                default:
                    msg = "";
                    break;
            }
            return ret;
        }
        public int AddShopRoleAccount(ShopRoleAccountModel model, out string msg)
        {
            var ret = ShopAdminDataAccess.AddShopRoleAccount(model);
            switch (ret)
            {
                case 2:
                    msg = "已存在账号";
                    break;
                default:
                    msg = "";
                    break;
            }
            return ret;
        }
        public ShopRoleAccountModel GetRoleAccountDs(int id)
        {
            var res = new ShopRoleAccountModel();
            var ds = ShopAdminDataAccess.GetRoleAccountDs(id);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id = Convert.ToInt32(dr["Id"]);
                    res.Account = dr["Account"].ToString();
                    res.PassWord = dr["PassWord"].ToString();
                    res.RoleId = Convert.ToInt32(dr["RoleId"]);
                }
            }
            return res;
        }
        public List<ShopRoleAccountModel> GetRoleAccountListDs(int shopAdminId)
        {
            var res = new List<ShopRoleAccountModel>();
            var ds = ShopAdminDataAccess.GetRoleAccountListDs(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new ShopRoleAccountModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Account = dr["Account"].ToString(),
                            RoleId = Convert.ToInt32(dr["RoleId"]),
                            RoleName = dr["RoleName"].ToString(),
                            OptionStatus = Convert.ToInt32(dr["OptionStatus"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"])
                        });
                    }

                }
            }
            return res;
        }
    }
}
