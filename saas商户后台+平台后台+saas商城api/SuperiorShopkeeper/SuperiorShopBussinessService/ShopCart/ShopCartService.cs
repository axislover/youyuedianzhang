﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorShopDataAccess;
using System.Data;
using SuperiorCommon;

namespace SuperiorShopBussinessService
{
    public class ShopCartService : IShopCartService
    {
        public int Api_AddShopCart(Api_AddShopCartCriteria critera, out string msg)
        {
            var ret = ShopCartDataAccess.Api_AddShopCart(critera);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "用户不存在或用户异常";
                    break;
                case 3:
                    msg = "商品不存在或商品已下架";
                    break;
                case 4:
                    msg = "该规格商品不存在或已下架";
                    break;
                case 5:
                    msg = "库存不足";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public  List<Api_ShopCartListItem> Api_GetShopCartList(string skuids)
        {
            //验证
            if (ToolManager.IsSqlin(skuids))
            {
                LogManger.Instance.WriteLog("查询购物车发生SQL注入:注入字符串"+skuids);
                return null;
            }
            var queryList = new List<Api_ShopCartListItem>();
            var ds = ShopCartDataAccess.Api_GetShopCartList(skuids);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new Api_ShopCartListItem()
                        {
                            ProductId = SecretClass.EncryptQueryString(dr["ProductId"].ToString()),
                            Product_SkuId = Convert.ToInt32(dr["Product_SkuId"]),
                            SkuType = Convert.ToInt32(dr["SkuType"]),
                            StockNum = Convert.ToInt32(dr["StockNum"]),
                            ImgPath = ConfigSettings.Instance.ImageUrl + dr["ImgPath"].ToString(),
                            ProductName = dr["ProductName"].ToString(),
                            Brand = dr["Brand"].ToString(),
                            ProppetyCombineName = dr["ProppetyCombineName"].ToString(),
                            SallPrice = DataManager.GetRowValue_Decimal(dr["SallPrice"]),
                            IsDel = Convert.ToInt32(dr["IsDel"]),
                            ShippingTemplateId = Convert.ToInt32(dr["ShippingTemplateId"]),
                            Weight = DataManager.GetRowValue_Float(dr["Weight"])
                        });
                    }
                }

            }
            return queryList;
        }

        public bool CreateOrderInsert(Api_CreateOrderCriteria criteria)
        {
            return ShopCartDataAccess.CreateOrderInsert(criteria);
        }

        public void Dispose()
        {
            
        }
    }
}
