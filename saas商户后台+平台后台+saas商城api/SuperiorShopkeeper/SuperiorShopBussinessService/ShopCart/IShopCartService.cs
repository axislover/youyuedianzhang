﻿using System.Collections.Generic;
using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface IShopCartService:IService
    {
        int Api_AddShopCart(Api_AddShopCartCriteria critera, out string msg);
        List<Api_ShopCartListItem> Api_GetShopCartList(string skuids);
        bool CreateOrderInsert(Api_CreateOrderCriteria criteria);
    }
}
