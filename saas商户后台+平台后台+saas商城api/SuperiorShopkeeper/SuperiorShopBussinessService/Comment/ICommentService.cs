﻿using System.Collections.Generic;
using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface ICommentService:IService
    {
        int Add(Api_AddCommentParms criteria, out string msg);
        List<Api_CommentModel> Api_GetCommentPagedList(Api_CommentCriteria criteria);
        int GetCounts(int productId);

        PagedSqlList<CommentModel> Search(SearchCommentCriteria criteria);
        void Del(int id);
    }
}
