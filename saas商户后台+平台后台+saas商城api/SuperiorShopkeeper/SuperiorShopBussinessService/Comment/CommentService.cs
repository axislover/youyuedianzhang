﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorShopDataAccess;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class CommentService : ICommentService
    {

        public PagedSqlList<CommentModel> Search(SearchCommentCriteria criteria)
        {
            var queryList = new List<CommentModel>();
            var totalCount = 0;
            var ds = CommentDataAccess.Search(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new CommentModel()
                        {
                            Id = Convert.ToInt32(dr["Id"].ToString()),
                            CreateTime = dr["CreateTime"].ToString(),
                            ProductName = dr["ProductName"].ToString(),
                            LoginName = dr["LoginName"].ToString(),
                            ShopName = dr["ShopName"].ToString(),   
                            CommentLevel = Convert.ToInt32(dr["CommentLevel"]),
                            OrderNo = dr["OrderNo"].ToString(),
                            CommentMsg = dr["CommentMsg"].ToString(),
                            HeadImgUrl = dr["HeadImgUrl"].ToString(),
                            NickName = dr["NickName"].ToString(),
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<CommentModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }
        public void Del(int id)
        {
            CommentDataAccess.Del(id);
        }

        #region Api
        public int Add(Api_AddCommentParms criteria, out string msg)
        {
            var ret = CommentDataAccess.Add(criteria);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "用户信息不匹配";
                    break;
                case 3:
                    msg = "该订单已经评论过了";
                    break;
                case 4:
                    msg = "订单信息错误";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }

        public List<Api_CommentModel> Api_GetCommentPagedList(Api_CommentCriteria criteria)
        {
            var queryList = new List<Api_CommentModel>();
            var ds = CommentDataAccess.Api_GetCommentPagedList(criteria);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new Api_CommentModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            CommentLevel = Convert.ToInt32(dr["CommentLevel"]),
                            NickName = dr["NickName"].ToString(),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]).ToString("yyyy-MM-dd HH:mm:ss"),
                            HeadImgUrl = dr["HeadImgUrl"].ToString(),
                            CommentMsg = dr["CommentMsg"].ToString()
                        });
                    }
                }

            }
            return queryList;
        }

        public int GetCounts(int productId)
        {
            var res = 0;
            var ds = CommentDataAccess.GetCounts(productId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    res = Convert.ToInt32(ds.Tables[0].Rows[0]["totalCount"]);
                }
            }
            return res;
        }

        #endregion

        public void Dispose()
        {
            
        }
    }
}
