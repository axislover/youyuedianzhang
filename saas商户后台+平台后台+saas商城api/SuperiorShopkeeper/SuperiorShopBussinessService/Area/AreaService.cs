﻿using System;
using System.Collections.Generic;
using SuperiorModel;
using SuperiorShopDataAccess;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class AreaService : IAreaService
    {
        public void Dispose()
        {
            
        }

        public List<AreaModel> Get(int parentId)
        {
            var res = new List<AreaModel>();
            var ds = AreaDataAccess.GetArea(parentId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new AreaModel() {
                            Id=Convert.ToInt32(dr["Id"]),
                            Area_Code = Convert.ToInt32(dr["Area_Code"]),
                            Parent_Id = Convert.ToInt32(dr["Parent_Id"]),
                            Area_Name = dr["Area_Name"].ToString()
                        });
                    }
                }
            }
            return res;
        }
        public List<AreaModel> GetAll()
        {
            var res = new List<AreaModel>();
            var ds = AreaDataAccess.GetAllArea();
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new AreaModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            Area_Code = Convert.ToInt32(dr["Area_Code"]),
                            Parent_Id = Convert.ToInt32(dr["Parent_Id"]),
                            Area_Name = dr["Area_Name"].ToString()
                        });
                    }
                }
            }
            return res;
        }
    }
}
