﻿using System.Collections.Generic;
using SuperiorModel;

namespace SuperiorShopBussinessService
{
    public interface ISetService:IService
    {
        List<ShippingTemplateModel> GetShippingTemplateList(int shopAdminId);
        int DelShippingTemplate(int id, out string msg);
        ShippingTemplateModel GetShippingTemplateModel(int id);
        bool AddShippingTemplate(ShippingTemplateModel model);
        bool EditShippingTemplate(ShippingTemplateModel model);
    }
}
