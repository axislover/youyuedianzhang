﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SuperiorCommon;
using SuperiorModel;
using SuperiorShopDataAccess;

namespace SuperiorShopBussinessService
{
    public class DecorateService : IDecorateService
    {
        public void CreateOrEditDecorate(DecorateModel model)
        {
            var disign = Serialize.SerializeObject(model.DecorateDesign);
            DecorateDataAccess.CreateOrEdit(model.Id.ToInt(), disign, model.ShopAdminId);
        }

        public void Dispose()
        {

        }

        public DecorateModel GetDecorateModel(int shopAdminId)
        {
            var res = new DecorateModel();
            var ds = DecorateDataAccess.GetDetail(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.DecorateDesign = dr["DecorateDesign"].ToString() == string.Empty ? new List<DesignItem>() : Serialize.DeserializeObject<List<DesignItem>>(dr["DecorateDesign"].ToString());

                }
            }
            return res;
        }
    }
}
