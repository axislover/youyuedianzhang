﻿using SuperiorModel;
using System;
using System.Collections.Generic;
using SuperiorCommon;
using SuperiorShopDataAccess;
using System.Data;

namespace SuperiorShopBussinessService
{
    public class MarketingService : IMarketingService
    {
        //public List<int> GetNotHasProductIds(int shopAdminId)
        //{

        //}
        //public List<int> GetNotHasAreaIds(int shopAdminId)
        //{

        //}
        public FullSetModel GetSetModel(int shopAdminId)
        {
            var res = new FullSetModel();
            res.NotHasAreaNames = new List<string>();
            res.NotHasProductImgs = new List<string>();
            var ds = MarketingDataAccess.GetSetModel(shopAdminId);
            if (DataManager.CheckDs(ds, 3))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id = Convert.ToInt32(dr["Id"]);
                    res.Amount = Convert.ToInt32(dr["Amount"]);
                    res.OptionStatus = Convert.ToInt32(dr["OptionStatus"]);
                    res.NotHasProductIds = dr["NotHasProductIds"].ToString();
                    res.NotHasProvinceIds = dr["NotHasProvinceIds"].ToString();
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        res.NotHasProductImgs.Add(ConfigSettings.Instance.ImageUrl + dr["ImagePath"].ToString());
                    }
                }
                if (DataManager.CheckHasRow(ds.Tables[2]))
                {
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        res.NotHasAreaNames.Add(dr["Area_Name"].ToString());
                    }
                }
            }

            return res;
        }

        public void SetNotPids(string ids, int shopAdminId)
        {
            MarketingDataAccess.SetNotPids(ids, shopAdminId);
        }
        public void SetNotAreas(string ids, int shopAdminId)
        {
            MarketingDataAccess.SetNotAreas(ids, shopAdminId);
        }
        public void SetFullAmount(int optionStatus, int amount, int shopAdminId)
        {
            MarketingDataAccess.SetFullAmount(optionStatus, amount, shopAdminId);
        }

        public List<CouponModel> GetCouponList(int shopAdminId)
        {
            var res = new List<CouponModel>();
            var ds = MarketingDataAccess.GetCouponList(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new CouponModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            CouponName = dr["CouponName"].ToString(),
                            CouponType = Convert.ToInt32(dr["CouponType"]),
                            DelAmount = DataManager.GetRowValue_Decimal(dr["DelAmount"]),
                            Discount = DataManager.GetRowValue_Int(dr["Discount"]),
                            MinOrderAmount = DataManager.GetRowValue_Decimal(dr["MinOrderAmount"]),
                            Days = Convert.ToInt32(dr["Days"]),
                            GetCount = Convert.ToInt32(dr["GetCount"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                            TotalCount = Convert.ToInt32(dr["TotalCount"])
                        });
                    }
                }
            }
            return res;
        }
        public PagedSqlList<CouponModel> SearchAdminCoupon(AdminCouponCriteria criteria)
        {
            var queryList = new List<CouponModel>();
            var totalCount = 0;
            var ds = MarketingDataAccess.SearchAdminCouponDs(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new CouponModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            CouponName = dr["CouponName"].ToString(),
                            CouponType = Convert.ToInt32(dr["CouponType"]),
                            DelAmount = DataManager.GetRowValue_Decimal(dr["DelAmount"]),
                            Discount = DataManager.GetRowValue_Int(dr["Discount"]),
                            MinOrderAmount = DataManager.GetRowValue_Decimal(dr["MinOrderAmount"]),
                            Days = Convert.ToInt32(dr["Days"]),
                            GetCount = Convert.ToInt32(dr["GetCount"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                            TotalCount = Convert.ToInt32(dr["TotalCount"]),
                            LoginName = dr["LoginName"].ToString()

                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<CouponModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        public void AddCoupon(CouponModel model)
        {
            MarketingDataAccess.AddCoupon(model);
        }
        public void DelCoupon(int id)
        {
            MarketingDataAccess.DelCoupon(id);
        }

        public PagedSqlList<UserCouponModel> SearchUserCoupon(UserCouponCriteria criteria)
        {
            var queryList = new List<UserCouponModel>();
            var totalCount = 0;
            var ds = MarketingDataAccess.SearchUserCouponList(criteria);
            if (DataManager.CheckDs(ds, 2))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        queryList.Add(new UserCouponModel()
                        {
                            NickName = dr["NickName"].ToString(),
                            CouponName = dr["CouponName"].ToString(),
                            CouponId = Convert.ToInt32(dr["CouponId"]),
                            CouponType = Convert.ToInt32(dr["CouponType"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                            AppType = Convert.ToInt32(dr["AppType"]),
                        });
                    }
                }
                totalCount = Convert.ToInt32(ds.Tables[1].Rows[0]["totalCount"]);
            }
            return new PagedSqlList<UserCouponModel>(queryList, criteria.PagingResult.PageIndex, criteria.PagingResult.PageSize, totalCount);
        }

        #region 接口
        public List<Api_UserCouponItem> Api_GetUserCouponList(int userid)
        {
            var res = new List<Api_UserCouponItem>();
            var ds = MarketingDataAccess.Api_GetUserCouponList(userid);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new Api_UserCouponItem()
                        {
                            CouponId = Convert.ToInt32(dr["CouponId"]),
                            CouponName = dr["CouponName"].ToString(),
                            CouponType = Convert.ToInt32(dr["CouponType"]),
                            DelAmount = DataManager.GetRowValue_Decimal(dr["DelAmount"]),
                            Discount = DataManager.GetRowValue_Int(dr["Discount"]),
                            MinOrderAmount = DataManager.GetRowValue_Decimal(dr["MinOrderAmount"]),
                            Days = Convert.ToInt32(dr["Days"]),
                            CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                        });
                    }
                }
            }
            return res;
        }
        public Api_UserCouponItem Api_GetUserCoupon(int userid, int couponid)
        {
            var res = new Api_UserCouponItem();
            var ds = MarketingDataAccess.Api_GetUserCoupon(userid, couponid);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.CouponId = Convert.ToInt32(dr["CouponId"]);
                    res.CouponName = dr["CouponName"].ToString();
                    res.CouponType = Convert.ToInt32(dr["CouponType"]);
                    res.DelAmount = DataManager.GetRowValue_Decimal(dr["DelAmount"]);
                    res.Discount = DataManager.GetRowValue_Int(dr["Discount"]);
                    res.MinOrderAmount = DataManager.GetRowValue_Decimal(dr["MinOrderAmount"]);
                    res.Days = Convert.ToInt32(dr["Days"]);
                    res.CreateTime = Convert.ToDateTime(dr["CreateTime"]);
                }
            }
            return res;
        }

        public List<Api_CouponItem> Api_GetCouponList(int shopAdminId)
        {
            var res = new List<Api_CouponItem>();
            var ds = MarketingDataAccess.Api_GetCouponList(shopAdminId);
            if (DataManager.CheckDs(ds, 1))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new Api_CouponItem()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            CouponName = dr["CouponName"].ToString(),
                            CouponType = Convert.ToInt32(dr["CouponType"]),
                            DelAmount = DataManager.GetRowValue_Decimal(dr["DelAmount"]),
                            Discount = DataManager.GetRowValue_Int(dr["Discount"]),
                            MinOrderAmount = DataManager.GetRowValue_Decimal(dr["MinOrderAmount"]),
                            Days = Convert.ToInt32(dr["Days"]),
                            TotalCount = Convert.ToInt32(dr["TotalCount"])
                        });
                    }
                }
            }
            return res;
        }

        public int Api_GetCoupon(int userid, int couponid, out string msg)
        {
            var ret = MarketingDataAccess.Api_GetCoupon(userid,couponid);
            msg = string.Empty;
            switch (ret)
            {
                case 2:
                    msg = "优惠券不存在或已删除";
                    break;
                case 3:
                    msg = "优惠券数量不足";
                    break;
                case 4:
                    msg = "该优惠券已领取过了";
                    break;
                default:
                    msg = "请求异常";
                    break;
            }
            return ret;
        }
        #endregion

        public void Dispose()
        {

        }
    }
}
