﻿using SuperiorModel;
using System.Collections.Generic;

namespace SuperiorShopBussinessService
{
    public interface ISearchWordService:IService
    {
        List<SearchWordModel> GetList(int shopAdminId);
        void Add(SearchWordModel model);
        void Del(int id);
        void Option(int id, int option);
    }
}
