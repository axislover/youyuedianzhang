﻿using Redis;
using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;
using System.Web.Mvc;

namespace SuperiorShopkeeper.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class SearchWordController : BaseController
    {
        private readonly ISearchWordService _ISearchWordService;

        public SearchWordController(ISearchWordService ISearchWordService)
        {
            _ISearchWordService = ISearchWordService;
        }
        protected override void Dispose(bool disposin)
        {
            this._ISearchWordService.Dispose();
            base.Dispose(disposin);
        }
        // GET: SearchWord
        /// <summary>
        /// 搜索词管理
        /// </summary>
        /// <returns></returns>
        /// 
        [ShopAuthority(AuthorityMenuEnum.SearchWord)]
        public ActionResult List()
        {
            var model = _ISearchWordService.GetList(UserContext.DeSecretId.ToInt());
            return View(model);
        }
        /// <summary>
        /// 天加搜索词
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        /// 
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.AddSearchWord)]
        public JsonResult Add(SearchWordModel model)
        {
            model.ShopAdminId = UserContext.DeSecretId.ToInt();
            _ISearchWordService.Add(model);
            RedisManager.Remove("GetSearchKeyword_" + model.ShopAdminId);
            return Success(true);
        }

        /// <summary>
        /// 删除搜索词
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.DelSearchWord)]
        public JsonResult Delete(int id)
        {
            _ISearchWordService.Del(id);
            RedisManager.Remove("GetSearchKeyword_" + UserContext.DeSecretId);
            return Success(true);
        }

        /// <summary>
        /// 操作搜索词
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.OptionSearchWord)]
        public JsonResult Option(int id, int optionStatus)
        {
            _ISearchWordService.Option(id, optionStatus);
            RedisManager.Remove("GetSearchKeyword_" + UserContext.DeSecretId);
            return Success(true);
        }
    }
}