﻿using SuperiorCommon;
using SuperiorModel;
using SuperiorShopBussinessService;
using System.Web.Mvc;

namespace SuperiorShopkeeper.Controllers
{
    [JsonException]
    [CheckLoginFitler]
    public class OrderController : BaseController
    {
        private readonly IOrderService _IOrderService;
        private readonly IShopAppService _IShopAppService;
        private readonly ICommentService _ICommentService;

        public OrderController(IOrderService IOrderService, IShopAppService IShopAppService, ICommentService ICommentService)
        {
            _IOrderService = IOrderService;
            _IShopAppService = IShopAppService;
            _ICommentService = ICommentService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IOrderService.Dispose();
            this._IShopAppService.Dispose();
            this._ICommentService.Dispose();
            base.Dispose(disposin);
        }
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 订单列表页面
        /// </summary>
        /// <returns></returns>
        /// 
       [ShopAuthority(AuthorityMenuEnum.OrderList)]
        public ActionResult List()
        {
            var criteria = new OrderCriteria() { AppType = 999, ShopAppId = "unChecke", ShopAdminId = UserContext.DeSecretId.ToInt(), PagingResult = new PagingResult(0, 30),OrderStatus=999 };
            var model = GetOrderPagedList(criteria);
            var shopAppList = _IShopAppService.ShopAppList(new ShopAppCriteria() { ShopAdminId = UserContext.DeSecretId, AppType = 999, PagingResult = new PagingResult(0, 999) });
            ViewBag.ShopAppList = shopAppList;
            return View(model);
        }

        private TPagedModelList<OrderModel> GetOrderPagedList(OrderCriteria criteria)
        {
            var query = _IOrderService.Search(criteria);
            return new TPagedModelList<OrderModel>(query, query.PagingResult);
        }
        /// <summary>
        ///订单列表搜索请求
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.OrderList)]
        public ActionResult List(OrderCriteria criteria)
        {
            criteria.ShopAdminId = UserContext.DeSecretId.ToInt();
            if (criteria.ShopAppId != "unChecke")
                criteria.ShopAppId = base.ValidataParms(criteria.ShopAppId);
            var model = GetOrderPagedList(criteria);
            return PartialView("_List", model);
        }
        /// <summary>
        /// 详情页面
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        public ActionResult Detail(string orderid)
        {
            int id = base.ValidataParms(orderid).ToInt();
            var model = _IOrderService.GetDetail(id);
            return View(model);
        }
        /// <summary>
        /// 发货
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.SendProduct)]
        public JsonResult SendProduct(SendProductParms model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            model.Id = base.ValidataParms(model.Id);
            _IOrderService.SendProduct(model);
            //删除redis订单详情
            Redis.RedisManager.Remove("GetDetail_" + model.Id);
            return Success(true);
        }
        /// <summary>
        /// 修改快递信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.SendProduct)]
        public JsonResult UpdateDelivery(SendProductParms model)
        {
            if (!ModelState.IsValid)
                return Error(base.ErrorMsg());
            model.Id = base.ValidataParms(model.Id);
            _IOrderService.UpdateDelivery(model);
            //删除redis订单详情
            Redis.RedisManager.Remove("GetDetail_" + model.Id);
            return Success(true);
        }

        /// <summary>
        /// 完成售后服务
        /// </summary>
        /// <param name="orderid"></param>
        /// <returns></returns>
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.FinishService)]
        public JsonResult FinishService(string orderid)
        {
            int id = base.ValidataParms(orderid).ToInt();
            _IOrderService.FinishService(id);
            return Success(true);
        }

        public ActionResult ServiceList()
        {
            return View();
        }
        /// <summary>
        /// 评论列表
        /// </summary>
        /// <returns></returns>
        public ActionResult CommentList()
        {
            var criteria = new SearchCommentCriteria()
            {
                PagingResult = new PagingResult(0, 20),
                ShopAdminId = UserContext.DeSecretId.ToInt()
            };
            var res = GetCommentPagedManager(criteria);
            return View(res);
        }
        /// <summary>
        /// 搜索评价 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        public ActionResult CommentList(SearchCommentCriteria criteria)
        {
            criteria.ShopAdminId = UserContext.DeSecretId.ToInt();
            var model = GetCommentPagedManager(criteria);
            return PartialView("_CommentList",model);
        }
        private TPagedModelList<CommentModel> GetCommentPagedManager(SearchCommentCriteria criteria)
        {
            var query = _ICommentService.Search(criteria);
            return new TPagedModelList<CommentModel>(query,query.PagingResult);
        }

    }
}