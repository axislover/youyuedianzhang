﻿using System.Web.Mvc;
using SuperiorShopBussinessService;


namespace SuperiorShopkeeper.Controllers
{
    public class TestController : BaseController
    {
        private readonly ITestService _ITestService;

        public TestController(ITestService ITestService)
        {
            _ITestService = ITestService;
        }
        // GET: Test
        public ActionResult Index()
        {
            var res = _ITestService.TestList();
            return View(res);
        }
    }
}