﻿using SuperiorShopBussinessService;
using System.Web.Mvc;
using SuperiorModel;
using SuperiorCommon;

namespace SuperiorShopkeeper.Controllers
{

    [JsonException]
    [CheckLoginFitler]
    public class UserController : BaseController
    {
        private readonly IUserService _IUserService;
        private readonly IShopAppService _IShopAppService;

        public UserController(IUserService IUserService, IShopAppService IShopAppService)
        {
            _IUserService = IUserService;
            _IShopAppService = IShopAppService;
        }
        protected override void Dispose(bool disposin)
        {
            this._IUserService.Dispose();
            this._IShopAppService.Dispose();
            base.Dispose(disposin);
        }
        // GET: User
        public ActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 用户列表管理页面
        /// </summary>
        /// <returns></returns>
        /// 
        [ShopAuthority(AuthorityMenuEnum.UserList)]
        public ActionResult List()
        {
            var criteria = new UserCriteria() { AppType = 999, ShopAppId = "unChecke", ShopAdminId = UserContext.DeSecretId.ToInt(), OptionStatus = 999, PagingResult = new PagingResult(0, 30) };
            var model = GetUserListManager(criteria);
            var shopAppList = _IShopAppService.ShopAppList(new ShopAppCriteria() { ShopAdminId = UserContext.DeSecretId, AppType = 0, PagingResult = new PagingResult(0, 999) });
            ViewBag.ShopAppList = shopAppList;
            return View(model);
        }

        private TPagedModelList<UserListItem> GetUserListManager(UserCriteria criteria)
        {
            var query = _IUserService.Search(criteria);
            return new TPagedModelList<UserListItem>(query, query.PagingResult);
        }
        /// <summary>
        /// 用户列表，搜索，分页请求
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [AjaxOnly]
        [HttpPost]
        [ShopAuthority(AuthorityOptionEnum.UserList)]
        public ActionResult List(UserCriteria criteria)
        {
            criteria.ShopAdminId = UserContext.DeSecretId.ToInt();
            if (criteria.ShopAppId!= "unChecke")
                criteria.ShopAppId = base.ValidataParms(criteria.ShopAppId);
            var model = GetUserListManager(criteria);
            return PartialView("_List",model);
        }
        /// <summary>
        /// 操作用户状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="optionStatus"></param>
        /// <returns></returns>
        [AjaxOnly]
        [ShopAuthority(AuthorityOptionEnum.OptionUser)]
        public JsonResult OptionUser(string id, int optionStatus)
        {
            int _id = base.ValidataParms(id).ToInt();
            var ret = _IUserService.OptionUser(_id,optionStatus);
            if (ret != 1)
                return Error("操作失败");
            return Success(true);
        }

    }
}