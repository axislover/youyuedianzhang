﻿(function () {
    SpecialListClass = {};
    SpecialListClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_addPlace', this.Add);
            $(document).on('click', '.DeletePlace', this.Delete);
        },
        Add: function () {
            window.location.href = "/Product/SpecialPlace?id=0";
        },
        Delete: function () {
            var id = $(this).attr("OptionId");
            layer.confirm('删除后数据不可恢复，确定要执行吗？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/Product/DelSpecial?id=" + id + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("执行成功", function () {
                            window.location.href = "/Product/SpecialList";
                        })

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {

            });
        }
    }
})();