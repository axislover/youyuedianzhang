﻿(function () {
    ProductListClass = {};
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 15 }, OptionStatus :999};
    ProductListClass.Instance = {
        Init: function () {     
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, ProductListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
            $(document).on('click', '.DeleteProduct', this.Delete);
            $(document).on('click', '.OptionProduct', this.OptionStatus);
        },
        OptionStatus: function () {
            var id = $(this).attr("OptionId");
            var opt = $(this).attr("Opt");
            var index = layer.load(1);
            $.get("/Product/OptionProduct?id=" + id + "&optionStatus=" + opt + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/Product/List";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        Delete: function () {
            var id = $(this).attr("OptionId");
            layer.confirm('删除后数据不可恢复，确定要执行吗？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/Product/DeleteProduct?id=" + id + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("执行成功", function () {
                            window.location.href = "/Product/List";
                        })

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {

            });
        },
        Search: function () {
 
            _searchCriteria.ProductName = $("#search_ProductName").val();
            _searchCriteria.Brand = $("#search_Brand").val();
            _searchCriteria.MenuId = $.trim($("#search_TypeName").val());
            _searchCriteria.OptionStatus = $("#search_status").val();
            ProductListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            var url = "/Product/List";
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Product/List", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }

    };

})();