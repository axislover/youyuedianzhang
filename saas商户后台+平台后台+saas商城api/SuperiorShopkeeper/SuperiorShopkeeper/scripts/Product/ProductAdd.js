﻿(function () {
    ProductAddClass = {};
    var islock = false;//防止同时提交两次数据
    var _layedit;
    var _index;
    var _ishot=0;
    ProductAddClass.Instance = {
        Init: function () {

            layui.use('layedit', function () {
                var layedit = layui.layedit;
                layedit.set({
                    uploadImage: {
                        url: '/Upload/UploadImg?object=ProductDetail', //接口url
                        type: 'post' //默认post
                    }
                });

               _index = layedit.build('productDetail', {
                    height: 480, //设置编辑器高度
               }); //建立编辑器
               _layedit = layedit;
            });
            
            //layui资源加载，验证
            layui.use('form', function () {
                var form = layui.form;
                form.render();
                form.on('checkbox(IsHot)', function (data) {
                    if (data.elem.checked)
                        _ishot = 1;
                    else
                        _ishot = 0;
                });
                form.on('submit(add)', ProductAddClass.Instance.Submit);
            });
            //layui上传图片资源加载
            layui.use('upload', function () {
                var upload = layui.upload;
                //执行实例
                var uploadInst = upload.render({
                    elem: '#btn_upload', //绑定元素
                    url: '/Upload/UploadImg?object=ProductBanner', //上传接口
                    size: 1500,
                    done: function (res) {
                        //上传完毕回调
                        if (res.code == "0") {
                            if ($('.layui-upload-list .img_div').length >= 5) {
                                layer.alert("商品图最多5张");
                                return false;
                            }
                           
                            ProductAddClass.Instance.CreateBannerImg(res.data.src);
                        } else {
                            layer.alert(res.msg);
                        }
                    },
                    error: function () {
                        //请求异常回调
                        layer.alert("图片上传异常");
                    }
                });
            });
            $(document).on('click', '.imgDelete', this.DeleteImg);
            $(document).on('click', '#btnBack', this.Back);
        },
        //删除商品图
        DeleteImg: function () {
            var that = $(this);
            layer.confirm('确定要删除么', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                that.parent().remove();
                layer.closeAll('dialog');
            }, function () {

            });
        },
        //返回
        Back: function () {
            layer.confirm('数据不会保存,确认后返回列表页?', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                window.location.href = "/Product/List";
            }, function () {

            });
        },
        //动态创建商品图层
        CreateBannerImg: function (src) {
            var arr = [];
            arr.push('<div class="img_div">');
            arr.push('<img class="layui-upload-img" src="' + src + '">');
            arr.push('<i class="layui-icon layui-icon-close-fill imgDelete"></i>');
            arr.push('</div>');
            $('.layui-upload-list').append(arr.join(''));
        },
        //提交商品
        Submit: function () {
            if (islock)
                return false;
            islock = true;
            if ($('.layui-upload-list .img_div').length <=0) {
                layer.alert("请上传商品图");
                islock = false;
                return false;
            }
            var BannerImgs = [];
            $('.layui-upload-list img').each(function () {
                BannerImgs.push($(this).attr("src"));
            })
            var DetailImgs = [];
            var Detail = _layedit.getContent(_index);
            if ($.trim(Detail) == "") {
                layer.alert("商品详情必填");
                islock = false;
                return false;
            }
            var imgs = $("#LAY_layedit_1").contents().find("img");
            imgs.each(function () {
                DetailImgs.push($(this).attr("src"));
            })
            var ProductName = $("#ProductName").val();
            var Brand = $("#Brand").val();
            var Title = $("#Title").val();
            var MenuId = $("#MenuId").val();
            var ShipTemplateId = $("#ShipTemplateId").val();
            if (MenuId == "" || ShipTemplateId == "") {
                layer.alert("分类,运费模板必选");
                islock = false;
                return false;
            }
            //var MenuName=$("#MenuId").find("option:selected").text()
            var Unit = $("#Unit").val();
            var Sorft = $("#Sorft").val();
            var InitSallNum = $("#InitSallNum").val();
            
  
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Product/AddProduct", {
                "ProductName": ProductName,
                "Brand": Brand,
                "Title": Title,
                "MenuId": MenuId,
                "Unit": Unit,
                "Sorft": Sorft,
                "IsHot":_ishot,
                "InitSallNum": InitSallNum,
                "Detail": Detail,
                "ShipTemplateId":ShipTemplateId,
                "DetailImgs": DetailImgs,
                "BannerImgs": BannerImgs

            }, true, function (data) {
                layer.close(index);
                islock = false;
                if (data.IsSuccess) {
                    layer.confirm('商品添加成功,是否返回列表?', {
                        btn: ['确定', '取消'] //按钮
                    }, function () {
                        window.location.href = "/Product/List";
                    }, function () {

                    });
                } else {
                    layer.alert(data.Message);
                }
            })
        },


    };
})();