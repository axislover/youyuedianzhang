﻿(function () {
    MenuClass = {};
    var _optionType = 1;//操作类型，1添加，2修改
    var _id = "";//当前操作的分类ID
    MenuClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_addMenu', this.Add);
            $(document).on('click', '.menuEdit', this.Edit);
            $(document).on('click', '.DeleteMenu', this.Delete);
            $(document).on('click', '.OptionMenu', this.OptionStatus);
            //layui验证表单写法-----begin
            var form;
            layui.use(['form'], function () {
                form = layui.form;
                form.render();
                form.on('submit(btn_submit)', MenuClass.Instance.Submit);
            });

            //-----end
        },
        Add: function () {
            layer.open({
                type: 1,
                skin: 'layui-layer-rim', //加上边框
                area: ['420px', '400px'], //宽高
                content: MenuClass.Instance.CreateHtml()

            });
            layui.use('form', function () {
                var form = layui.form;
                form.render();

            });
            _optionType = 1;
            //layui上传图片资源加载
            layui.use('upload', function () {
                var upload = layui.upload;
                //执行实例
                var uploadInst = upload.render({
                    elem: '#btn_upload', //绑定元素
                    url: '/Upload/UploadImg?object=ProductMenu', //上传接口
                    size: 1500,
                    done: function (res) {
                        //上传完毕回调
                        if (res.code == "0") {
                            MenuClass.Instance.CreateBannerImg(res.data.src);
                        } else {
                            layer.alert(res.msg);
                        }
                    },
                    error: function () {
                        //请求异常回调
                        layer.alert("图片上传异常");
                    }
                });
            });
        },
        //动态创建图层
        CreateBannerImg: function (src) {
            var arr = [];
            arr.push('<div class="img_div">');
            arr.push('<img style="width:75px;height:75px;" class="layui-upload-img" src="' + src + '">');

            arr.push('</div>');
            $('.layui-upload-list').html(arr.join(''));
        },
        OptionStatus: function () {
            var id = $(this).attr("OptionId");
            var opt = $(this).attr("Opt");
            var index = layer.load(1);
            $.get("/Product/OptionMenu?id=" + id + "&optionStatus=" + opt + "&r=" + new Date().getTime(), function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/Product/Menu";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        Delete: function () {
            var id = $(this).attr("OptionId");
            layer.confirm('删除后数据不可恢复，确定要执行吗？', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                var index = layer.load(1);
                $.get("/Product/DeleteMenu?id=" + id + "&r=" + new Date().getTime(), function (data) {
                    layer.close(index);
                    if (data.IsSuccess) {
                        layer.alert("执行成功", function () {
                            window.location.href = "/Product/Menu";
                        })

                    } else {
                        layer.alert(data.Message);
                    }
                })
            }, function () {

            });
        },
        Edit: function () {
            layer.open({
                type: 1,
                skin: 'layui-layer-rim', //加上边框
                area: ['420px', '400px'], //宽高
                content: MenuClass.Instance.CreateHtml()

            });

            _optionType = 2;
            _id = $(this).attr("OptionId");
            $("#text_menuname").val($(this).attr("MenuName"));
            $("#text_sorft").val($(this).attr("Sorft"));
            MenuClass.Instance.CreateBannerImg($(this).attr("ImgPath"));
            var _isAll = $(this).attr("IsAll");
            $("input[name=text_isAll][value=0]").attr("checked", _isAll == 0 ? true : false)
            $("input[name=text_isAll][value=1]").attr("checked", _isAll == 1 ? true : false)
            layui.use('form', function () {
                var form = layui.form;
                form.render();

            });
            //layui上传图片资源加载
            layui.use('upload', function () {
                var upload = layui.upload;
                //执行实例
                var uploadInst = upload.render({
                    elem: '#btn_upload', //绑定元素
                    url: '/Upload/UploadImg?object=ProductBanner', //上传接口
                    size: 1500,
                    done: function (res) {
                        //上传完毕回调
                        if (res.code == "0") {
                            MenuClass.Instance.CreateBannerImg(res.data.src);
                        } else {
                            layer.alert(res.msg);
                        }
                    },
                    error: function () {
                        //请求异常回调
                        layer.alert("图片上传异常");
                    }
                });
            });
        },
        Submit: function () {
            var menuname = $("#text_menuname").val();
            var sorft = $("#text_sorft").val();
            if (!ToolManager.Validata.IsPInt(sorft)) {
                layer.alert("排序请输入正整数");
                return false;
            }
            if ($('.layui-upload-list .img_div').length <= 0) {
                layer.alert("请上传分类图标");
                return false;
            }
            var imgpath = $('.layui-upload-list img').attr("src");
            var model = {};
            var url = "";
            if (_optionType == 2) {
                model.Id = _id;
                url = "/Product/EditMenu";
            } else {
                url = "/Product/AddMenu";
            }

            var _isAll;
            $("input[name=text_isAll]").each(function (index, item) {
                if (item.checked)
                    _isAll = item.value;
            });

            model.IsAll = _isAll;
            model.MenuName = menuname;
            model.Sorft = sorft;
            model.ImgPath = imgpath;
            var index = layer.load(1);
            RequestManager.Ajax.Post(url, model, true, function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("执行成功", function () {
                        window.location.href = "/Product/Menu";
                    })

                } else {
                    layer.alert(data.Message);
                }
            })
        },
        CreateHtml: function () {
            var arr = [];
            arr.push('<div class="layui-form" action="" >');
            arr.push('<div class="layui-form-item">');
            arr.push('<label for="text_menuname" class="layui-form-label">分类名称 </label>');
            arr.push('<div class="layui-input-inline" style="width:50%">');
            arr.push('<input type="text" id="text_menuname" name="text_menuname" lay-verify="required" class="layui-input" placeholder="请输入分类名称">');
            arr.push('</div>');
            arr.push(' <div class="layui-form-mid layui-word-aux"><span class="x-red">*</span></div>');
            arr.push('</div>');

            arr.push('<div class="layui-form-item">');
            arr.push(' <label for="text_sorft" class="layui-form-label">分类排序 </label>');
            arr.push('<div class="layui-input-inline" style="width:50%">');
            arr.push(' <input type="text" id="text_sorft" name="text_sorft" lay-verify="required" value="10" class="layui-input" placeholder="请输入分类排序">');
            arr.push('</div>');
            arr.push(' <div class="layui-form-mid layui-word-aux"><span class="x-red">*</span></div>');
            arr.push('</div>');

            arr.push('<div class="layui-form-item">');
            arr.push('<label for="text_isAll" class="layui-form-label" >是否更多</label >');
            arr.push('<div class="layui-input-inline" style="width:50%">');
            arr.push('<input type="radio" name="text_isAll" value="1" title="是" >');
            arr.push('<input type="radio" name="text_isAll" value="0" title="否" checked>')
            arr.push('</div>');
            arr.push('</div>');

            arr.push('<div class="layui-form-item">');
            arr.push(' <label for="text_sorft" class="layui-form-label">分类图标</label>');
            arr.push('<div class="layui-input-inline" style="width:50%">');
            arr.push('<button type="button" class="layui-btn" id="btn_upload">                                <i class="layui-icon">&#xe67c;</i>上传图片</button>');
            arr.push('<div class="layui-upload-list"></div>');
            arr.push('</div>');
            arr.push(' <div class="layui-form-mid layui-word-aux"><span class="x-red">*</span></div>');
            arr.push('</div>');



            arr.push('<div class="layui-form-item" style="text-align:center;">');
            arr.push('<div class="layui-input-block" style="margin:0;"><button class="layui-btn" lay-submit lay-filter="btn_submit">确定</button></div>');
            arr.push('</div>');
            arr.push('</div>');
            return arr.join('');
        }
    };
})();