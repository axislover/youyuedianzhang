﻿
(function () {
    RegisterClass = {};
    RegisterClass.Instance = {
        Init: function () {
            //layui验证表单写法-----begin
            var form;
            layui.use(['form'], function () {
                form = layui.form;
                form.on('submit(LAY-user-reg-submit)', RegisterClass.Instance.Register);
            });
            $(document).on('click', '#btn_getCode', this.GetCode);  
            //-----end
        },
        GetCode: function () {
            var phone = $("#text_phonenumber").val();
            if (phone == "")
            {
                layer.alert("请输入正确得手机号");
                return false;
            }
            $.get("/Login/GetCode?phone=" + phone + "&r=" + new Date().getTime(), function (data) {
                if (data.IsSuccess) {
                    $("#btn_getCode").addClass("layui-btn-disabled").attr("disabled", "disabled");
                    RegisterClass.Instance.TimerMsg();
                } else {
                    layer.alert(data.Message);
                }
            })
        },
        TimerMsg: function () {
            var count = 60;
            var timer = setInterval(function () {

                if (count == 0) {

                    $("#btn_getCode").removeClass("layui-btn-disabled").attr("disabled", false).html("获取手机验证码");
                    window.clearInterval(timer);
                    return false;
                }
                $("#btn_getCode").html(count + "秒后再次发送");
                count--;
            }, 1000)
        },
        Register: function () {
            var text_username = $("#text_username").val();
            var text_password = $("#text_password").val();
            var reset_password = $("#reset_password").val();
            var text_contact = $("#text_contact").val();
            var text_phonenumber = $("#text_phonenumber").val();
            var text_qq = $("#text_qq").val();
            var text_wx = $("#text_wx").val();
            var text_code = $("#text_code").val();
            var text_AccountManagerId = $("#text_AccountManagerId").val();
            var area_introduce = $("#area_introduce").val();
            if (text_username.length < 8 || text_username.length > 20) {
                layer.alert("账户长度8-20位");
                return false;
            }
            if (text_password.length < 6 || text_password.length > 25) {
                layer.alert("密码为6-25位的字母或数字");
                return false;
            }
            if (text_password != reset_password) {
                layer.alert("两次密码输入不一致");
                return false;
            }
            var index = layer.load(1);
            RequestManager.Ajax.Post("/Login/Register", {
                "LoginName": text_username,
                "PassWord": text_password,
                "ResetPassWord": reset_password,
                "Introduce": area_introduce,
                "PhoneNumber": text_phonenumber,
                "QqCode": text_qq,
                "WxCode": text_wx,
                "Contact": text_contact,
                "Code":text_code,
                "AccountManagerId": text_AccountManagerId

            }, true, function (data) {
                layer.close(index);
                if (data.IsSuccess) {
                    layer.alert("注册成功,审核结果会以短信通知!");
                    window.location.href = "/Login/Login";
                } else {
                    layer.alert(data.Message);
                }
            })
        }

    };
})()