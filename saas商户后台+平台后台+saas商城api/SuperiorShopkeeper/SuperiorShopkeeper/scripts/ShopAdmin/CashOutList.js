﻿(function () {
    CashOutListClass = {};
    var _layedit;
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 15 }, OrderStatus: 999};
    CashOutListClass.Instance = {
        Init: function () {
            layui.use('form', function () {
                var form = layui.form;
                form.render();
            });
      
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, CashOutListClass.Instance.Refresh);
            $(document).on('click', '#btn_search', this.Search);
        },
        Search: function () {

            _searchCriteria.OrderStatus = $("#search_status").val();
            CashOutListClass.Instance.Refresh(0);
        },
        Refresh: function (pageIndex) {
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/ShopAdmin/CashOutList", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    };
})()