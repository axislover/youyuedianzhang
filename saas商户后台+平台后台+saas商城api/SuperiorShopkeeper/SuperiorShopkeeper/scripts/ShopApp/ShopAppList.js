﻿(function () {
    ShopAppListClass = {};
    var _searchCriteria = { PagingResult: { PageIndex: 0, PageSize: 15 },AppType:999 };

    ShopAppListClass.Instance = {
        Init: function () {
            $(document).on('click', '#btn_addApp', this.GoToAdd);
            $(document).on('click', '#btn_addH5', this.GoToAddH5);
            $(document).on('click', '.showUrl', this.ShowUrl);
            $(document).on('click', '.btn_showDemo', this.ShowDemo);
            $(document).on('click', '.btn_gobuy', this.GoBuy);
            //分页事件注册
            superior.ui.control.Pager.enablePaging(document, ShopAppListClass.Instance.Refresh);
        },
        GoBuy:function(){
            window.location.href = "/Home/Recharge";
        },
        ShowDemo:function(){
            layer.alert("演示商城在官方公众号yydz1118,请各位商户务必关注,以便于提供更多服务!");
        },
        ShowUrl:function(){
            var url = $(this).attr("url");
            layer.alert(url);
        },
        GoToAdd: function () {
            window.location.href = "/ShopApp/Add";
        },
        GoToAddH5: function () {
            window.location.href = "/ShopApp/Add_H5";
        },
        Refresh: function (pageIndex) {
            var url = "/ShopApp/List";
            if (pageIndex !== undefined)
                _searchCriteria.PagingResult.PageIndex = pageIndex;
            var index = layer.load(1);
            RequestManager.Ajax.Post("/ShopApp/List", _searchCriteria, true, function (data) {
                layer.close(index);
                $("#dataList").html(data);
            })
        }
    }
})()