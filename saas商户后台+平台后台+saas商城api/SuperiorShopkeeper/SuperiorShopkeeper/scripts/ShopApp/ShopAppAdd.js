﻿(function () {
    ShopAppAddClass = {};
    var islock = false;//防止同时提交两次数据
    ShopAppAddClass.Instance = {
        Init: function () {
            //layui验证表单写法-----begin
            var form;
            layui.use(['form'], function () {
                form = layui.form;
                form.on('submit(add)', ShopAppAddClass.Instance.Submit);
            });
            $(document).on('click', '#btnBack', this.Back);
        },
        Back: function () {
            layer.confirm('数据不会保存,确认后返回列表页?', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                window.location.href = "/ShopApp/List";
            }, function () {
                
            });
        },
        Submit: function () {
                if (islock)
                    return false;
                islock = true;
                var AppId = $("#text_appid").val();
                var AppSecret = $("#text_appsecret").val();
                var PaymentId = $("#text_paymentId").val();
                var PaySecret = $("#text_paySecret").val();
                var ServiceQQ = $("#text_serviceqq").val();
                var ServiceWx = $("#text_servicewx").val();
                var ServicePhone = $("#text_servicephone").val();
                var ShopName = $("#text_shopname").val();
                var index = layer.load(1);
                RequestManager.Ajax.Post("/ShopApp/Add", {
                    "AppId": AppId,
                    "AppSecret": AppSecret,
                    "PaymentId": PaymentId,
                    "PaySecret": PaySecret,
                    "ServiceQQ": ServiceQQ,
                    "ServiceWx": ServiceWx,
                    "ServicePhone": ServicePhone,
                    "ShopName": ShopName,
                    "AppType": 1,
                    "PayType":2

                }, true, function (data) {
                    layer.close(index);
                    islock = false;
                    if (data.IsSuccess) {
                        layer.alert("添加成功!");
                        window.location.href = "/ShopApp/List";
                    } else {
                        layer.alert(data.Message);
                    }
                })
        }

    };
})()