﻿(function () {
    ShopH5EditClass = {};
    var islock = false;//防止同时提交两次数据
    ShopH5EditClass.Instance = {
        Init: function () {
            //layui验证表单写法-----begin
            var form;
            layui.use(['form'], function () {
                form = layui.form;
                form.on('submit(add)', ShopH5EditClass.Instance.Submit);
                form.on('radio(choseType)', function (data) {
                    var value = data.elem.value;
                    switch (value) {
                        case '1':
                            $("#payMsgDiv").hide();
                            break;
                        case '2':
                            $("#payMsgDiv").show();
                            break;
                    }
                });
            });

            $(document).on('click', '#btnBack', this.Back);
        },
        Back: function () {
            layer.confirm('数据不会保存,确认后返回列表页?', {
                btn: ['确定', '取消'] //按钮
            }, function () {
                window.location.href = "/ShopApp/List";
            }, function () {

            });
        },
        Submit: function () {
            if (islock)
                return false;
            islock = true;
            var _type = "";
            var id = $(this).attr("ShopId");
            $("input[name=text_status]").each(function (index, item) {
                if (item.checked)
                    _type = item.value;
            });
            var AppId = $("#text_appid").val();
            var AppSecret = $("#text_appsecret").val();
            var PaymentId = $("#text_paymentId").val();
            var PaySecret = $("#text_paySecret").val();
            if (_type == "2") {
                //走的自有支付，验证
                if (AppId == "" || AppSecret == "" || PaymentId == "" || PaySecret == "") {
                    layer.alert("请检查相关支付配置参数不能为空");
                    islock = false;
                    return false;
                }
            }
            var ServiceQQ = $("#text_serviceqq").val();
            var ServiceWx = $("#text_servicewx").val();
            var ServicePhone = $("#text_servicephone").val();
            var ShopName = $("#text_shopname").val();
            var index = layer.load(1);
            RequestManager.Ajax.Post("/ShopApp/Edit", {
                "AppId": AppId,
                "AppSecret": AppSecret,
                "PaymentId": PaymentId,
                "PaySecret": PaySecret,
                "ServiceQQ": ServiceQQ,
                "ServiceWx": ServiceWx,
                "ServicePhone": ServicePhone,
                "ShopName": ShopName,
                "AppType": 2,
                "PayType": 1,
                "Id":id

            }, true, function (data) {
                layer.close(index);
                islock = false;
                if (data.IsSuccess) {
                    layer.alert("添加成功!");
                    window.location.href = "/ShopApp/List";
                } else {
                    layer.alert(data.Message);
                }
            })
        }

    };
})()