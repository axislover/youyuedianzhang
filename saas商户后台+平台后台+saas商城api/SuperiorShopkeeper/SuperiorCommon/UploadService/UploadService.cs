﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;

namespace SuperiorCommon
{
    public class UploadService
    {
        private static string _requestUrl = ConfigurationManager.AppSettings["UploadServiceUrl"];
        /// <summary>
        /// 上传至图片服务器
        /// </summary>
        /// <param name="imgs"></param>
        /// <returns></returns>
        public static bool Upload(List<string> imgs, UploaderKeys key)
        {
            bool res = true;
            var fileUploader = GetUploadInstance(key);
            if (imgs.Count > 0)
            {
                foreach (var img in imgs)
                {
                    var _tempFileUrl = Path.Combine(fileUploader.TempPhysicalPath, Path.GetFileName(img));
                    if (!UploadService.UploadImage(fileUploader.FolderName, _tempFileUrl))
                    {
                        res = false;
                        break;
                    }
                }
            }
            return res;
        }
        /// <summary>
        /// 上传至图片服务器
        /// </summary>
        /// <param name="imgs"></param>
        /// <returns></returns>
        public static bool Upload(string img, UploaderKeys key)
        {
            bool res = true;
            var fileUploader = GetUploadInstance(key);
            var _tempFileUrl = Path.Combine(fileUploader.TempPhysicalPath, Path.GetFileName(img));
            if (!UploadService.UploadImage(fileUploader.FolderName, _tempFileUrl))
            {
                res = false;
            }
            return res;
        }

        /// <summary>
        /// 获取图片类型对象
        /// </summary>
        /// <param name="uploadType"></param>
        /// <returns></returns>
        public static IFileUploader GetUploadInstance(string uploadType)
        {
            var uploaderKey = (UploaderKeys)Enum.Parse(typeof(UploaderKeys), uploadType, true);
            IFileUploader fileUploader = GetFileUploader(uploaderKey);
            return fileUploader;
        }
        /// <summary>
        /// 获取图片类型对象
        /// </summary>
        /// <param name="uploaderKey"></param>
        /// <returns></returns>
        public static IFileUploader GetUploadInstance(UploaderKeys uploaderKey)
        {
            IFileUploader fileUploader = GetFileUploader(uploaderKey);
            return fileUploader;
        }

        private static IFileUploader GetFileUploader(UploaderKeys uploaderKeys)
        {
            switch (uploaderKeys)
            {
                case UploaderKeys.ProductBanner:
                    return ProductBannerUploader.Instance;
                case UploaderKeys.GroupBanner:
                    return GroupBannerUploader.Instance;
                case UploaderKeys.ProductDetail:
                    return ProductDetailUploader.Instance;
                case UploaderKeys.ProductMenu:
                    return ProductMenuUploader.Instance;
                case UploaderKeys.ArticleTitleImage:
                    return ArticleTitleImageUploader.Instance;
                case UploaderKeys.ArticleDetail:
                    return ArticleDetailUploader.Instance;
                case UploaderKeys.GroupShopImage:
                    return GroupShopUploader.Instance;
                case UploaderKeys.GroupTypeImage:
                    return GroupTypeUploader.Instance;
                case UploaderKeys.InformationImage:
                    return InformationUploader.Instance;
                case UploaderKeys.CourseImg:
                    return CourseUploader.Instance;
                default:
                    throw new ArgumentException();
            }
        }

        /// <summary>
        /// 上传服务
        /// </summary>
        /// <returns></returns>
        private static bool UploadImage(string uploadType, string path)
        {
            using (WebClient webclient = new WebClient())
            {
                byte[] responseArray = webclient.UploadFile(_requestUrl + uploadType, "POST", path);
                string getPath = Encoding.GetEncoding("UTF-8").GetString(responseArray);
                return getPath.Contains("1");
            }
            
        }
    }
}
