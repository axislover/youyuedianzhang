﻿using System.Configuration;

namespace SuperiorCommon
{
    public sealed class ConfigSettings
    {
        public static readonly ConfigSettings Instance = new ConfigSettings();

        private ConfigSettings() { }

        public string FileUploadPath
        {
            get
            {
                return ConfigurationManager.AppSettings["FileUploadPath"];
            }
        }

        public string FileUploadFolderNameTemp
        {
            get
            {
                return ConfigurationManager.AppSettings["FileUploadFolderNameTemp"];
            }
        }

        public string WebSetting(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        /// <summary>
        /// API的图片网关
        /// </summary>
        public string ApiImgHost
        {
            get
            {
                return ConfigurationManager.AppSettings["ApiImgHost"];
            }
        }
        /// <summary>
        /// 后台图片网关
        /// </summary>
        public string ImageHost
        {
            get
            {
                return ConfigurationManager.AppSettings["ImageHost"];
            }
        }
        /// <summary>
        /// 图片服务器网关
        /// </summary>
        public string ImageUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["ImgUrl"];
            }
        }


        public string RedisHost
        {
            get
            {
                return ConfigurationManager.AppSettings["RedisHost"];
            }
        }

        public string WxCallBackUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["WxCallBackUrl"];
            }
        }
        public string WxCallBackUrl_H5
        {
            get
            {
                return ConfigurationManager.AppSettings["WxCallBackUrl_H5"];
            }
        }

        public string APPID
        {
            get
            {
                return ConfigurationManager.AppSettings["APPID"];
            }
        }

        public string APP_Secret
        {
            get
            {
                return ConfigurationManager.AppSettings["APP_Secret"];
            }
        }

        public string Merchant_Id
        {
            get
            {
                return ConfigurationManager.AppSettings["Merchant_Id"];
            }
        }
        public string PaySecret
        {
            get
            {
                return ConfigurationManager.AppSettings["PaySecret"];
            }
        }
        public string WebHost
        {
            get
            {
                return ConfigurationManager.AppSettings["WebHost"];
            }
        }
        public string ApiHost
        {
            get
            {
                return ConfigurationManager.AppSettings["ApiHost"];
            }
        }
     

        public string WxRechargeNoticeUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["WxRechargeNoticeUrl"];
            }
        }

        public string AuthRedirectUri
        {
            get
            {
                return ConfigurationManager.AppSettings["AuthRedirectUri"];
            }
        }
        public string ServerToken
        {
            get
            {
                return ConfigurationManager.AppSettings["ServerToken"];
            }
        }
        public string ServerAppID
        {
            get
            {
                return ConfigurationManager.AppSettings["ServerAppID"];
            }
        }
        public string ServerEncodingAESKey
        {
            get
            {
                return ConfigurationManager.AppSettings["ServerEncodingAESKey"];
            }
        }
        public string ServerAppSecret
        {
            get
            {
                return ConfigurationManager.AppSettings["ServerAppSecret"];
            }
        }
        public string ServeAuthRedirectUri
        {
            get
            {
                return ConfigurationManager.AppSettings["ServeAuthRedirectUri"];
            }
        }
        public string TemplateId
        {
            get
            {
                return ConfigurationManager.AppSettings["TemplateId"];
            }
        }
        public string SmsAccessKey
        {
            get
            {
                return ConfigurationManager.AppSettings["SmsAccessKey"];
            }
        }

        public string SmsAccessSecret
        {
            get
            {
                return ConfigurationManager.AppSettings["SmsAccessSecret"];
            }
        }
        public string SmsSignName
        {
            get
            {
                return ConfigurationManager.AppSettings["SmsSignName"];
            }
        }
        public string SmsTemplateCode
        {
            get
            {
                return ConfigurationManager.AppSettings["SmsTemplateCode"];
            }
        }
    }
}
