﻿using System.Web;

namespace SuperiorCommon
{
    public class SessionManager
    {
        /// <summary>
        /// 设置登录session，id参数，是针对同一个用户打开不同得两个商城或者后台得拼接seesionkey,主要针对H5商城设置得参数，因为H5商城得商城信息从url读取，用户信息从seesion读，必须得保证该用户是该商城得
        /// </summary>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <param name="mins"></param>
        /// <param name="id"></param>
        public static void SetSession(string key, object obj, string id = "", int mins = 360)
        {
            if (id == "")
                HttpContext.Current.Session[key] = obj;
            else
                HttpContext.Current.Session[key + id] = obj;
            HttpContext.Current.Session.Timeout = mins;
        }

    }

    public class SessionKey
    {
        /// <summary>
        ///商户后台
        /// </summary>
        public static string ShopKey
        {
            get
            {
                return "YYDZ";
            }
        }
        /// <summary>
        /// 总后台
        /// </summary>
        public static string AdminnKey
        {
            get
            {
                return "ADMIN_SUPERIOR";
            }
        }
        /// <summary>
        /// H5商城
        /// </summary>
        public static string H5UserKey
        {
            get
            {
                return "H5_User";
            }
        }
        /// <summary>
        /// 服务号，商户提现
        /// </summary>
        public static string ServiceShopKey
        {
            get
            {
                return "Service_ShopKey";
            }
        }
        /// <summary>
        /// 服务号，推广人
        /// </summary>
        public static string ServiceManagerAccountKey
        {
            get
            {
                return "Service_ManagerAccountKey";
            }
        }
    }
}
