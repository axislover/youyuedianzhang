﻿using System;

namespace SuperiorCommon
{
    /// <summary>
    /// 枚举注释的自定义属性类
    /// </summary>
    public class EnumDescriptionAttribute : Attribute
    {
        private string m_strDescription;
        public EnumDescriptionAttribute(string strPrinterName)
        {
            m_strDescription = strPrinterName;
        }

        public string Description
        {
            get { return m_strDescription; }
        }
    }


}
