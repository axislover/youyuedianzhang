﻿namespace SuperiorCommon
{
    public enum CommonStatus
    {
        下架 = 2,
        正常 = 1,
        删除 = -1
    }
   

    public enum DataFromType
    {
        admin = 1,
        api = 2
    }


    public enum WriteOrReadEnum
    {
        //从主库读取
        FromWrite = 0,
        //从库读取
        FromRead = 1
    }

    public enum UserType
    {
        Shop = 1,
        Admin=2
    }

    public enum UploaderKeys
    {
        ProductBanner,
        GroupBanner,
        ProductDetail,
        ProductMenu,
        ArticleTitleImage,
        ArticleDetail,
        GroupShopImage,
        GroupTypeImage,
        InformationImage,
        CourseImg
    }

    public enum ShopAdminType
    {
        MainAccount=1,//店主后台主账号类型
        Role=2//角色账号类型
    }

    public enum AuthorityModuleEnum
    {
        [EnumDescription("系统管理")]
        SytemManager=101,
        [EnumDescription("商城管理")]
        MallManager =102,
        [EnumDescription("商品管理")]
        ProductManager =103,
        [EnumDescription("用户管理")]
        UserManager =104,
        [EnumDescription("订单管理")]
        OrderManager =105,
        [EnumDescription("营销管理")]
        MarketingManager =106,
        [EnumDescription("设置")]
        SetManager =107
    }

    public enum AuthorityMenuEnum
    {
        [EnumDescription("角色权限管理")]
        RoleList =2,
        [EnumDescription("员工账号管理")]
        ShopAccountList =3,
        [EnumDescription("资料管理")]
        ShopAdmin =4,
        [EnumDescription("商城管理")]
        ShopAppList =6,
        [EnumDescription("分类管理")]
        ProductMenu =8,
        [EnumDescription("商品管理")]
        ProductList =9,
        [EnumDescription("首页轮播图管理")]
        ProductBannerList =10,

        [EnumDescription("用户列表")]
        UserList =11,
        [EnumDescription("订单管理列表")]
        OrderList =12,
        [EnumDescription("满额包邮")]
        FullAmountSendSet =13,
        [EnumDescription("优惠券管理")]
        CouponList =14,
        [EnumDescription("领取优惠券列表")]
        UserCouponList =15,
        [EnumDescription("运费模板管理")]
        ShippingTemplateList =16,
        [EnumDescription("商品专场设置")]
        SpecialPlace = 17,
        [EnumDescription("搜索词管理")]
        SearchWord = 18,
        [EnumDescription("商城装修")]
        Decorate = 19,
    }

    public enum AuthorityOptionEnum
    {
        [EnumDescription("查看")]
        RoleSee = 10000,
        [EnumDescription("添加角色")]   
        AddRole = 1001, 
        [EnumDescription("修改角色")]
        EditRole = 1002,
        [EnumDescription("删除角色")]
        DeleteRole = 1003,
        [EnumDescription("设置角色权限")]
        SetRoleAuthority = 1004,


        [EnumDescription("查看")]
        RoleAccountSee = 10001,
        [EnumDescription("添加或修改账号")]
        AddOrEditRoleAccount = 1005,
        [EnumDescription("删除")]
        OptionAccount =1006,
        [EnumDescription("修改状态")]
        DeleteRoleAccount = 1007,

        [EnumDescription("查看")]
        ShopAdminInfoSee = 10002,
        [EnumDescription("编辑商户")]
        EditShopAdminInfo = 1008,


        [EnumDescription("查看")]
        ShopAppSee = 10003,
        [EnumDescription("添加")]
        AddShopApp = 1009   ,
        [EnumDescription("修改")]
        EditShopApp = 1010,
        [EnumDescription("提交微信审核")]
        UploadVersion = 1046,
        [EnumDescription("获取审核结果")]
        GetWxCheckResult = 1047,
        [EnumDescription("发布小程序")]
         PublicProgram= 1048,
        [EnumDescription("重置流程")]
        ResetAllPublic = 1049,

        [EnumDescription("查看")]
        MenuSee = 10004,
        [EnumDescription("添加")]
        AddMenu = 1011,
        [EnumDescription("修改")]
        EditMenu = 1012,
        [EnumDescription("上下架")]
        OptionMenu = 1013,
        [EnumDescription("删除")]
        DeleteMenu = 1014,

        [EnumDescription("查看")]
        ProductSee = 10005,
        [EnumDescription("搜索")]
        SearchList = 1015,
        [EnumDescription("发布商品")]   
        AddProduct = 1016,
        [EnumDescription("删除")]
        DeleteProduct = 1017,
        [EnumDescription("上下架")]
        OptionProduct = 1018,
        [EnumDescription("商品修改")]
        ProductEdit = 1019,
        [EnumDescription("设置单规格数据")]
        EditSingleSku = 1020,
        [EnumDescription("设置多规格数据")]
        EditMoreSku = 1021,
        [EnumDescription("添加属性")]   
        AddPropety = 1022,
        [EnumDescription("追加规格值")]
        AddPropetyValue = 1023,
        [EnumDescription("删除整个规格属性")]
        DelPropety = 1024,
        [EnumDescription("删除规格值")]
        DelPropetyValue =1025,

        [EnumDescription("查看")]
        BannerSee = 10006,
        [EnumDescription("添加")]
        AddBanner=1026,
        [EnumDescription("修改")]
        EditBanner = 1027,
        [EnumDescription("上下架")]
        OptionBanner = 1028,
        [EnumDescription("删除")]
        DelBanner =1029,

        [EnumDescription("查看")]
        UserSee = 10007,
        [EnumDescription("搜索")]
        UserList = 1030,
        [EnumDescription("修改状态")]
        OptionUser=1031,

        [EnumDescription("查看")]
        OrderSee = 10008,
        [EnumDescription("搜索")]
        OrderList=1032,
        [EnumDescription("发货")]
        SendProduct=1033,
        [EnumDescription("完成售后")]
        FinishService=1034,

        [EnumDescription("查看")]
        SetFullSendSee = 10009,
        [EnumDescription("设置不参与商品")]
        SetNotPids=1035,
        [EnumDescription("设置不参与地区")]
        SetNotAreas=1036,
        [EnumDescription("提交")]
        SetFullSendModel=1037,

        [EnumDescription("查看")]
        CouponSee = 10010,
        [EnumDescription("添加")]
        AddCoupon=1038,
        [EnumDescription("删除")]
        DelCoupon=1039,

        [EnumDescription("查看")]
        UserCouponSee = 10011,
        [EnumDescription("搜索")]
        UserCouponList=1040,

        [EnumDescription("查看")]
        TemplateSee = 10012,
        [EnumDescription("添加")]
        AddTemplate=1041,
        [EnumDescription("删除")]
        DelTemplate=1042,
        [EnumDescription("修改")]
        EditTemplate=1043,

        [EnumDescription("查看")]
        SpecialPlaceSee = 10013,
        [EnumDescription("修改")]
        OptionSpecialPlace =1044,
        [EnumDescription("删除")]
        DeleteSepecial = 1045,

        [EnumDescription("查看")]
        SearchWordSee = 10014,
        [EnumDescription("添加")]
        AddSearchWord = 1046,
        [EnumDescription("上下架")]
        OptionSearchWord = 1047,
        [EnumDescription("删除")]
        DelSearchWord = 1048,

        [EnumDescription("保存")]
        SaveDecorate = 1049,
    }
}
