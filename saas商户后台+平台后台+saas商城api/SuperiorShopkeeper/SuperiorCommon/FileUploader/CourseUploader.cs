﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperiorCommon
{
    public class CourseUploader: MinDimensionUploader
    {
        public static readonly CourseUploader Instance = new CourseUploader();
        private CourseUploader() { }

        public override int MinWidth
        {
            get { return 100; }
        }

        public override int MinHeight
        {
            get { return 100; }
        }

        /// <summary>
        /// 50
        /// </summary>
        public override int MaxBytesLength
        {
            get { return 5000 * 1024; }
        }

        public override string[] AllowedImageTypes
        {
            get { return new string[] { ".jpg", ".png", ".jpeg" }; }
        }

        public override string FolderName
        {
            get
            {
                return "Course";
                //    ConfigSettings.Instance.FileUploadFolderNameAd;
            }
        }
    }
}
