﻿using Redis;

namespace Api.SuperiorShop
{
    public class ValidataTool
    {
        /// <summary>
        /// 验证当前用户的token是否过期
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        internal static bool CheckToken(string token_key,string token)
        {
            var res = false;
            if (RedisManager.Get<string>(token_key) == token)
                res = true;
            return res;
        }
    }
}