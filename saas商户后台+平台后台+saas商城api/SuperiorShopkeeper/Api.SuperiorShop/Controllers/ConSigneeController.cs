﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using SuperiorModel;
using SuperiorCommon;
using SuperiorShopBussinessService;
using Redis;

namespace Api.SuperiorShop.Controllers
{
    [ApiExceptionAttribute]
    public class ConSigneeController : ApiControllerBase
    {
        // GET: ConSignee
        private readonly IConsigneeService _IConsigneeService;
        private readonly IAreaService _IAreaService;

        public ConSigneeController(IConsigneeService IConsigneeService, IAreaService IAreaService)
        {
            _IConsigneeService = IConsigneeService;
            _IAreaService = IAreaService;

        }
        protected override void Dispose(bool disposing)
        {
            this._IConsigneeService.Dispose();
            this._IAreaService.Dispose();
            base.Dispose(disposing);
        }
        /// <summary>
        /// 添加收获地址
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> Add(Api_ConsigneeModel criteria)
        {
            if (string.IsNullOrEmpty(criteria.Address) || string.IsNullOrEmpty(criteria.AreaCode) || string.IsNullOrEmpty(criteria.CityCode) || string.IsNullOrEmpty(criteria.ConsigneeName) || string.IsNullOrEmpty(criteria.ConsigneePhone) || string.IsNullOrEmpty(criteria.UserId) || string.IsNullOrEmpty(criteria.ProvinceCode))
                return Error("参数错误");
            if (!ValidateUtil.IsValidMobile(criteria.ConsigneePhone))
                return Error("手机格式错误");
            string _userid = base.ValidataParms(criteria.UserId);
            if (_userid == null)
                return Error("用户异常");
            criteria.UserId = _userid;
            string msg = "";
            var ret = _IConsigneeService.Add(criteria,out msg);
            if (ret != 1)
                return Error(msg);
            return Success(true);
        }
        /// <summary>
        /// 删除地址
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ApiResultModel<object> Del(int id, string userid)
        {
            string _userid = base.ValidataParms(userid);
            if (_userid == null)
                return Error("用户异常");
            _IConsigneeService.Del(id,_userid.ToInt());
            return Success(true);
        }
        /// <summary>
        /// 编辑地址
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpPost]
        public ApiResultModel<object> Edit(Api_ConsigneeModel criteria)
        {
            if (string.IsNullOrEmpty(criteria.Address) || string.IsNullOrEmpty(criteria.AreaCode) || string.IsNullOrEmpty(criteria.CityCode) || string.IsNullOrEmpty(criteria.ConsigneeName) || string.IsNullOrEmpty(criteria.ConsigneePhone) || string.IsNullOrEmpty(criteria.UserId) || string.IsNullOrEmpty(criteria.ProvinceCode)||criteria.Id==0)
                return Error("参数错误");
            if (!ValidateUtil.IsValidMobile(criteria.ConsigneePhone))
                return Error("手机格式错误");
            string _userid = base.ValidataParms(criteria.UserId);
            if (_userid == null)
                return Error("用户异常");
            criteria.UserId = _userid;
            _IConsigneeService.Edit(criteria);
            return Success(true);
        }
        /// <summary>
        /// 更改地址状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userid"></param>
        /// <param name="isdefault"></param>
        /// <returns></returns>
        /// 
        [HttpGet]
        public ApiResultModel<object> UpdateIsDefault(int id, string userid, int isdefault)
        {
            string _userid = base.ValidataParms(userid);
            if (_userid == null)
                return Error("用户异常");
            _IConsigneeService.UpdateIsDefault(id,_userid.ToInt(),isdefault);
            return Success(true);
        }
        /// <summary>
        /// 获取地址列表
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public ApiResultModel<object> GetList(string userid)
        {
            string _userid = base.ValidataParms(userid);
            if (_userid == null)
                return Error("用户异常");
            var res = _IConsigneeService.GetList(_userid.ToInt());
            return Success(res);
        }
        /// <summary>
        /// 根据父ID获取地区
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        public ApiResultModel<object> GetAreaList(int parentId, string userid)
        {
            string _userid = base.ValidataParms(userid);
            if (_userid == null)
                return Error("用户异常");
            Func<List<AreaModel>> fucc = delegate ()
            {
                return _IAreaService.GetAll();
            };
            var allAreas = DataIntegration.GetData<List<AreaModel>>("AllAreas", fucc,9999);
            var res = allAreas.Where(p=>p.Parent_Id==parentId);
            return Success(res);
        }

    }
}