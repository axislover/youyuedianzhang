﻿using SuperiorModel;
using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;

namespace SuperiorShopDataAccess
{
    public class SearchWordDataAccess
    {
        public static DataSet GetListDs(int shopAdminId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "select * from C_SearchWord where ShopAdminId=@shopAdminId and IsDel=0 order by Id desc";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static void Add(SearchWordModel model)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@keyWord", model.KeyWord),
                new SqlParameter("@ShopAdminId", model.ShopAdminId)
            };
            var sql = "insert into C_SearchWord(ShopAdminId,KeyWord)values(@ShopAdminId,@keyWord)";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
            
        }

        public static void Del(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
          {
                 new SqlParameter("@id", id)
            };
            var sql = "update C_SearchWord set IsDel=1 where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }
        public static void Option(int id, int option)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
          {
                 new SqlParameter("@id", id),
                 new SqlParameter("@option", option)
            };
            var sql = "update C_SearchWord set OptionStatus=@option where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

    }
}
