﻿using System.Collections.Generic;
using SuperiorModel;
using SuperiorSqlTools;
using System.Data;
using System.Data.SqlClient;


namespace SuperiorShopDataAccess
{
    public class CommentDataAccess
    {

        public static DataSet Search(SearchCommentCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            if (!string.IsNullOrEmpty(criteria.OrderNo))
            {
                sb.Append(" and o.OrderNo=@OrderNo");
                parms.Add(new SqlParameter("@OrderNo", criteria.OrderNo));
            }
            if (!string.IsNullOrEmpty(criteria.LoginName))
            {
                sb.Append(" and c.LoginName=@LoginName ");
                parms.Add(new SqlParameter("@LoginName", criteria.LoginName));
            }

            if (criteria.CommentLevel != 0)
            {
                sb.Append(" and c.CommentLevel=@CommentLevel ");
                parms.Add(new SqlParameter("@CommentLevel", criteria.CommentLevel));
            }
            if (criteria.ShopAdminId != criteria.ShopAdminId)
            {
                sb.Append(" and c.ShopAdminId=@ShopAdminId ");
                parms.Add(new SqlParameter("@ShopAdminId", criteria.ShopAdminId));
            }

            var sql = string.Format("select top({0}) * from(select ROW_NUMBER() over(order by c.Id desc)as    rownum,c.Id,u.NickName,u.HeadImgUrl,o.OrderNo,c.LoginName,s.ShopName,c.CommentLevel, c.CommentMsg,c.CreateTime,p.ProductName from C_Comment c left join C_Order o on c.OrderId=o.Id left join C_Users u on c.UserId= u.Id left join C_ShopApp s on c.ShopAppId=s.Id left join C_Product p on c.ProductId = p.Id where {1} and c.IsDel=0 )tt where tt.rownum>{2};select count(*) as totalCount from  C_Comment c left join C_Order o on c.OrderId=o.Id left join C_Users u on c.UserId= u.Id left join C_ShopApp s on c.ShopAppId=s.Id left join C_Product p on c.ProductId = p.Id  where {1} and c.IsDel=0", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }

        public static void Del(int id)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
           {
                new SqlParameter("@id",id)
            };
            var sql = "update C_Comment set IsDel=1 where Id=@id";
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        #region api
        public static int Add(Api_AddCommentParms criteria)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@OrderId", criteria.OrderId),
                new SqlParameter("@Spid",criteria.Spid.ToInt()),
                new SqlParameter("@UserId",criteria.UserId.ToInt()),
                new SqlParameter("@Said",criteria.Said.ToInt()),
                new SqlParameter("@CommentLevel ",criteria.CommentLevel),
                new SqlParameter("@CommentMsg",criteria.CommentMsg),
                rtn_err
            };
            sqlManager.ExecuteNonQuery(CommandType.StoredProcedure, "add_comment", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public static DataSet Api_GetCommentPagedList(Api_CommentCriteria criteria)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@offset",criteria.OffSet),
                new SqlParameter("@size",criteria.Size),
                 new SqlParameter("@productId",criteria.ProductId.ToInt())
            };
            return sqlManager.ExecuteDataset(CommandType.StoredProcedure, "api_getcommentpagedlist", parms);
        }

        public static DataSet GetCounts(int productId)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@productId",productId)
            };
            var sql = "select COUNT(Id) as totalCount from C_Comment where ProductId=@productId and IsDel = 0";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }
        #endregion
    }
}
