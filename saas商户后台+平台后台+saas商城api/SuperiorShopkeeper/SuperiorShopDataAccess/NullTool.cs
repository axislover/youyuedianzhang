﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperiorShopDataAccess
{
    public static class NullTool
    {
        public static string IsNull(this string str)
        {
            return str == null ? "" : str;
        }

        public static int ToInt(this string str)
        {
            return Convert.ToInt32(str);
        }
    }
}
