﻿using SuperiorModel;
using System;

namespace SuperiorShopDataAccess
{
    public class BaseDataAccess
    {
        /// <summary>
        /// 获取日期时间段，用于报表,比如查询报表数据，查询昨天开始7天的数据，需要一个最小时间和一个最大时间，Sql的datedif函数可以实现，但是为了走索引，性能，这里计算出，Sql直接进行比较就可以。
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static DateArea GetDataArea(int min, int max)
        {
            var res = new DateArea();
            //获取min天前的最后1秒的时间xxxxx:23:59:59
            res.MaxDate = DateTime.Now.AddDays(-min).ToShortDateString() + " 23:59:59";
            //获取max天前的最后1秒的时间xxxx:00:00:00
            res.MinDate = DateTime.Now.AddDays(-max).ToShortDateString() + " 00:00:00";
            return res;
        }
    }
}
