﻿using SuperiorModel;
using SuperiorSqlTools;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace SuperiorShopDataAccess
{
    public class UserDataAccess
    {
        public static DataSet Search(UserCriteria criteria)
        {
            var sqlManager = new SqlManager();
            List<SqlParameter> parms = new List<SqlParameter>();
            var sb = sqlManager.CreateSb();
            if (!string.IsNullOrEmpty(criteria.NickName))
            {
                sb.Append(" and u.NickName=@NickName");
                parms.Add(new SqlParameter("@NickName", criteria.NickName));
            }
            if (!string.IsNullOrEmpty(criteria.LoginName))
            {
                sb.Append(" and u.LoginName=@LoginName");
                parms.Add(new SqlParameter("@LoginName", criteria.LoginName));
            }
            if (criteria.AppType != 999)
            {
                sb.Append(" and u.AppType=@AppType ");
                parms.Add(new SqlParameter("@AppType", criteria.AppType));
            }
            if (criteria.ShopAppId!="unChecke")
            {
                sb.Append(" and u.ShopAppId=@ShopAppId ");
                parms.Add(new SqlParameter("@ShopAppId", criteria.ShopAppId.ToInt()));
            }
            if (criteria.ShopAdminId != 0)
            {
                sb.Append(" and u.ShopAdminId=@ShopAdminId ");
                parms.Add(new SqlParameter("@ShopAdminId", criteria.ShopAdminId));
            }
            if (criteria.OptionStatus != 999)
            {
                sb.Append(" and u.OptionStatus=@OptionStatus ");
                parms.Add(new SqlParameter("@OptionStatus", criteria.OptionStatus));
            }
            if (!string.IsNullOrEmpty(criteria.BeginTime))
            {
                sb.Append(" and u.CreateTime>=@BeginTime ");
                parms.Add(new SqlParameter("@BeginTime", criteria.BeginTime));
            }
            if (!string.IsNullOrEmpty(criteria.EndTime))
            {
                sb.Append(" and u.CreateTime<=@EndTime ");
                parms.Add(new SqlParameter("@EndTime", criteria.EndTime));
            }
            var sql = string.Format("select top({0}) * from (select ROW_NUMBER() over(order by u.Id desc)as rownum,u.*,ISNULL(s.ShopName,'') as ShopName from C_Users u left join C_ShopApp s on u.ShopAppId=s.Id where {1} )tt where tt.rownum>{2};select count(*) as totalCount from C_Users u where {1} ", criteria.PagingResult.PageSize, sb.ToString(), criteria.PagingResult.PageSize * criteria.PagingResult.PageIndex);
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms.ToArray());
        }
        public static int OptionUser(int id, int optionStatus)
        {
            var sqlManager = new SqlManager();
            SqlParameter rtn_err = sqlManager.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@id", id),
                new SqlParameter("@optionStatus",optionStatus)
            };
            var sql = "update C_Users set OptionStatus=@optionStatus where Id=@id";
            return sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public static DataSet GetUserSessionDs(int userid)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
           {
                new SqlParameter("@userid", userid)
            };
            var sql = "select u.Id,u.NickName,u.HeadImgUrl,u.Token,u.OptionStatus,u.CreateTime,s.ShopName from C_Users u left join C_ShopApp s on u.ShopAppId = s.Id where u.Id=@userid";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        #region 接口
        public static DataSet Api_WxUserLoginOrRegister(Api_WxUserLoginCriteria criteria)
        {
            var sqlManager = new SqlManager();
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAppId", criteria.Spid.ToInt()),
                new SqlParameter("@appType",criteria.AppType),
                new SqlParameter("@nickName",criteria.NickName),
                new SqlParameter("@headImgUrl",criteria.HeadImgUrl),
                new SqlParameter("@sex",criteria.Sex),
                new SqlParameter("@openid",criteria.OpenId),
                new SqlParameter("@token",SuperiorCommon.ToolManager.CreateShortToken()),
            };
            return sqlManager.ExecuteDataset(CommandType.StoredProcedure, "wx_login_register", parms);
        }
        #endregion
    }
}
