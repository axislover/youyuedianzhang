﻿using SuperiorSqlTools;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SuperiorShopDataAccess
{
    public static class DecorateDataAccess
    {
        public static DataSet GetDetail(int shopAdminId)
        {
            var sqlManager = new SqlManager();//
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "select * from C_Decorate where ShopAdminId=@shopAdminId;";
            return sqlManager.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public static void CreateOrEdit(int id, string design, int shopAdminId)
        {
            var sqlManager = new SqlManager();
            string sql = string.Empty;
            List<SqlParameter> parms = new List<SqlParameter>();
            parms.Add(new SqlParameter("@design", design));
            parms.Add(new SqlParameter("@shopAdminId", shopAdminId));
            if (id > 0)
            {
                //更新
                parms.Add(new SqlParameter("@id", id));
                sql = "update C_Decorate set DecorateDesign =@design,UpdateTime = Getdate() where Id =@id and ShopAdminId =@shopAdminId ";
            }
            else
            {
                sql = "insert C_Decorate(ShopAdminId,DecorateDesign)values(@shopAdminId,@design)";
            }
            sqlManager.ExecuteNonQuery(CommandType.Text, sql, parms.ToArray());
        }
    }
}
