﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace CqCore.DB
{
    public interface ISqlService<TDb> : ISqlService 
        where TDb : IBaseDb
    { }
    public interface ISqlService
    {
        Task<SqlParameter> GetRtnParameter();
        Task<StringBuilder> CreateSb();
        /// <summary>
        /// 查询单个数据值
        /// </summary>
        /// <param name="sqlStr"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        Task<object> ExecuteScalar(string sqlStr, params SqlParameter[] commandParameters);
        /// <summary>
        /// 非查询操作，commandType可能是存储过程或SQL语句
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sqlOrStoredProcedure"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        Task<int> ExecuteNonQuery(CommandType type, string sqlOrStoredProcedure, params SqlParameter[] commandParameters);
        /// <summary>
        /// 查询结果集
        /// </summary>
        /// <param name="type"></param>
        /// <param name="sqlOrStoredProcedure"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        Task<DataSet> ExecuteDataset(CommandType type, string sqlOrStoredProcedure, params SqlParameter[] commandParameters);
        /// <summary>
        /// 查询结果集
        /// </summary>
        /// <param name="cmdType"></param>
        /// <param name="sqlOrStoredProcedure"></param>
        /// <param name="commandParameters"></param>
        /// <returns></returns>
        Task<SqlDataReader> ExecuteReader(CommandType cmdType, string sqlOrStoredProcedure, params SqlParameter[] commandParameters);
        /// <summary>
        /// 事务执行,没行SQL对应一组参数化
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="paramers"></param>
        /// <returns></returns>
        Task<bool> ExecTransactionAsUpdate(string[] sql, List<SqlParameter[]> paramers = null);
        /// <summary>
        /// 事务执行，所有SQL公用一组参数化
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="paramers"></param>
        /// <returns></returns>
        Task<bool> ExecTransactionAsUpdate_Public(string[] sql, SqlParameter[] paramers = null);
    }
}
