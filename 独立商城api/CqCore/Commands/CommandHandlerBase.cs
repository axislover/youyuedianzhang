﻿namespace CqCore.Commands
{
    public abstract class CommandHandlerBase<TResult>
    {
        public CommandResult<TResult> Success(TResult data)
        {
            return CommandResult<TResult>.Success(data);
        }

        public CommandResult<TResult> Failure(string reason)
        {
            return CommandResult<TResult>.Failure(reason);
        }
    }

    public abstract class CommandHandlerDefault
    {
        public CommandObjectResult Success(object data)
        {
            return CommandObjectResult.Success(data);
        }

        public CommandObjectResult Failure(string reason)
        {
            return CommandObjectResult.Failure(reason);
        }
    }
}
