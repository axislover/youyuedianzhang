﻿using System;
using System.IO;

namespace CqCore.Extensions
{
    public static class AppDomainExtensions
    {
        public static string GetApplicationName(this AppDomain context)
        {
            var applicationFile = new FileInfo($"{context.BaseDirectory}{context.FriendlyName}");

            if (applicationFile.Exists)
            {
                return applicationFile.Name.Replace(applicationFile.Extension, string.Empty);
            }
            else
            {
                return context.FriendlyName;
            }
        }
    }
}
