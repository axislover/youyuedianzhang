﻿using System;

namespace CqCore.Logging
{
    public interface ILogRecorder : IDisposable
    {
        void Trace(EventData data);
        void Debug(EventData data);
        void Info(EventData data);
        void Warn(EventData data);
        void Error(EventData data);
        void Fatal(EventData data);
    }

    public interface ILogRecorder<out TCategoryName> : ILogRecorder
    {

    }
}
