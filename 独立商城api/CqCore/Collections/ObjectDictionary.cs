﻿using System.Collections.Generic;

namespace CqCore.Collections
{
    public class ObjectDictionary : Dictionary<string, object>
    {
        public ObjectDictionary()
        { }

        public T Value<T>(string key)
        {
            if (this.TryGetValue(key, out object value))
            {
                return (T)value;
            }
            else
            {
                return default(T);
            }
        }
        public T Value<T>(string key, T value)
        {
            if (this.ContainsKey(key))
            {
                this[key] = value;
            }
            else
            {
                this.Add(key, value);
            }
            return value;
        }
    }
}
