﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace CqCore.Collections
{
    public class DataDictionary : Dictionary<string, JToken>
    {
        public DataDictionary()
        { }

        public T Value<T>(string key)
        {
            if (this.TryGetValue(key, out JToken value))
            {
                return value.ToObject<T>();
            }
            else
            {
                return default(T);
            }
        }
        public T Value<T>(string key, T value)
        {
            if (this.ContainsKey(key))
            {
                this[key] = JToken.FromObject(value);
            }
            else
            {
                this.Add(key, JToken.FromObject(value));
            }
            return value;
        }

        public JObject ToJSON()
        {
            return JObject.FromObject(this);
        }
    }
}
