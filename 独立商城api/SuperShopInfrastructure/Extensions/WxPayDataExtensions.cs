﻿using SuperShopInfrastructure.Weixin.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace SuperShopInfrastructure.Extensions
{
    public static class WxPayDataExtensions
    {
        public static HttpResponseMessage ConvertRes(this WxPayData data)
        {
            var msg = data.ToXml();
            return new HttpResponseMessage
            {
                Content = new StringContent(msg.ToString(), new UTF8Encoding(false)
                  , "text/plain")
            };
        }
    }
}
