﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.User
{
    public class WxUserLoginViewModel
    {
        public string Id { get; set; }
        public string NickName { get; set; }
        public string HeadImgUrl { get; set; }
        public int OptionStatus { get; set; }
        public DateTime CreateTime { get; set; }
        public string Token { get; set; }
        public string ShopName { get; set; }
    }
}
