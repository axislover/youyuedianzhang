﻿using SuperShopInfrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SuperShopInfrastructure.Models.Consignee
{
    public class CreateOrEditConsigneeCommand
    {
        public int Id { get; set; }
        public int IsDefault { get; set; }
        [Required]
        [Decrypt]
        public string UserId { get; set; }
        [Required]
        public string ConsigneeName { get; set; }
        [Required]
        public string ConsigneePhone { get; set; }
        public string ZipCode { get; set; }
        [Required]
        public string ProvinceCode { get; set; }
        [Required]
        public string CityCode { get; set; }
        [Required]
        public string AreaCode { get; set; }
        [Required]
        public string Address { get; set; }
    }
}
