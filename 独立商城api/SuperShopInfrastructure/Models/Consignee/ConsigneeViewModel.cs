﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Consignee
{
    public class ConsigneeViewModel
    {
        public int Id { get; set; }
        public int IsDefault { get; set; }
        public string UserId { get; set; }
        public string ConsigneeName { get; set; }
        public string ConsigneePhone { get; set; }
        public string ZipCode { get; set; }
        public string ProvinceCode { get; set; }
        public string CityCode { get; set; }
        public string AreaCode { get; set; }
        public string CityName { get; set; }
        public string ProvinceName { get; set; }
        public string AreaName { get; set; }
        public string Address { get; set; }
    }
}
