﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Product
{
    public class HomeProductDataViewModel
    {
        public HomeProductDataViewModel()
        {
            this.Banners = new List<ProductItem>();
            this.NewRecommends = new List<ProductItem>();
            this.Hots = new List<ProductItem>();
        }
        /// <summary>
        /// 轮播商品集合
        /// </summary>
        public List<ProductItem> Banners { get; set; }
        /// <summary>
        /// 最新推荐集合，
        /// </summary>
        public List<ProductItem> NewRecommends { get; set; }
        /// <summary>
        /// 热门商品集合
        /// </summary>
        public List<ProductItem> Hots { get; set; }

    }

    public class ProductItem
    {
        public string Id { get; set; }
        public string ProductName { get; set; }
        public string Brand { get; set; }
        public string Title { get; set; }
        public string ImgPath { get; set; }
        public decimal SallPrice { get; set; }
        public string ShopAdminId { get; set; }
        public int SallNum { get; set; }

    }
}
