﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Product
{
    public class MakeProductQrImageCommand
    {
        public string ProductImage { get; set; }
        public string PECName { get; set; }
        public string PriceRanage { get; set; }
        public string ProductId { get; set; }
        public string ShopID { get; set; }
    }
}
