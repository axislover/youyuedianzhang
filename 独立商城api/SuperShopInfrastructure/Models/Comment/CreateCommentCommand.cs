﻿using SuperShopInfrastructure.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SuperShopInfrastructure.Models.Comment
{
    public class CreateCommentCommand
    {
        [Required]
        public int OrderId { get; set; }

        [Required]
        [Decrypt]
        public string Spid { get; set; }

        [Required]
        [Decrypt]
        public string UserId { get; set; }

        [Required]
        [Decrypt]
        public string Said { get; set; }


        public int CommentLevel { get; set; }
        [Required]
        public string CommentMsg { get; set; }
    }
}
