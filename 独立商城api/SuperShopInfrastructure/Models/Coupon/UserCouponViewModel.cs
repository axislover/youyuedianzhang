﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperShopInfrastructure.Models.Coupon
{
    public class UserCouponViewModel
    {
        public int CouponId { get; set; }
        public string CouponName { get; set; }
        public int CouponType { get; set; }
        public decimal DelAmount { get; set; }
        public int Discount { get; set; }
        public decimal MinOrderAmount { get; set; }
        public DateTime CreateTime { get; set; }
        public int Days { get; set; }
        public DateTime ExpressTime
        {
            get
            {
                return this.CreateTime.AddDays(this.Days);
            }
        }
        public bool IsExpress
        {
            get
            {
                return DateTime.Now > this.ExpressTime;
            }
        }
    }
}
