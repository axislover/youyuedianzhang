﻿namespace RabbitmqService.Options
{
    public class RabbitmqSubscriberOption
    {
        public string HostName { get; set; }
        public string VirtualHost { get; set; } = "/";
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string QueueName { get; set; }
        public int AttemptCount { get; set; } = 0;
        public bool MultipleAck { get; set; } = false;
        public ushort PrefetchCount { get; set; } = 4;

        public int StartRetryInterval { get; set; } = 5;
        public string ClientProvidedName { get; set; } = null;
        public bool UseDeadLetter { get; set; } = false;
    }
}
