﻿using CqBuiltinService.Options;
using CqCore.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using NLogService;
using NLogService.Options;
using System.IO;

namespace CqBuiltinService.Logging
{
    public static class LoggingConfiguratorExtensions
    {
        public static IServiceCollection AddBuiltinLogging(this IServiceCollection services, string configPath)
        {
            var provider = services.BuildServiceProvider();
            var runtimeConfig = provider.GetRequiredService<IOptions<RuntimeOption>>().Value;
            var loggingConfig = provider.GetRequiredService<IOptions<LoggingOption>>().Value;

            NLogConfigurator.Config(new NLogOption
            {
                ConfigPath = configPath ?? loggingConfig.ConfigPath,
                DataPath = Path.Combine(runtimeConfig.ModuleCacheDirectory, loggingConfig.DirectoryName),
                ModuleName = runtimeConfig.ModuleName,
                NodeName = runtimeConfig.NodeName
            });

            services.AddTransient(typeof(ILogRecorder<>), typeof(NLogRecorder<>));

            return services;
        }
    }
}
