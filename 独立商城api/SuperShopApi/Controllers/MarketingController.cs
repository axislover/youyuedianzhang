﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.Coupon;
using SuperShopInfrastructure.Static;
using SuperShopService.IServices;

namespace SuperShopApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MarketingController : ControllerBase
    {
        private readonly IMarketingService marketingService;

        public MarketingController(IMarketingService marketingService)
        {
            this.marketingService = marketingService;
        }

        /// <summary>
        /// 领取优惠券
        /// </summary>
        /// <param name="couponid"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpPost("")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCoupon([FromQuery] int couponid,[FromQuery] string userid)
        {
            string _userid = ToolManager.DecryptParms(userid);
            if (_userid == null)
                return BadRequest("用户异常");
            var result = await marketingService.GetCoupon(_userid.ToInt(),couponid);
            if (!result.IsSuccess)
                return BadRequest(result.FailureReason);
            return Ok(result.GetData());
        }

        /// <summary>
        /// 获取当前商户未被删除得优惠券列表
        /// </summary>
        /// <param name="said"></param>
        /// <returns></returns>
        [HttpGet("couponList")]
        [ProducesResponseType(typeof(List<CouponViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetCouponList([FromQuery] string said)
        {
            string _said = ToolManager.DecryptParms(said);
            if (_said == null)
                return BadRequest("参数异常");
            var res = await marketingService.GetCouponList(_said.ToInt());
            return Ok(res);
        }

        /// <summary>
        /// 获取当前用户得可用得优惠券列表
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet("userCouponList")]
        [ProducesResponseType(typeof(List<UserCouponViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetUserCoupon([FromQuery] string userid)
        {
            string _userid = ToolManager.DecryptParms(userid);
            if (_userid == null)
                return BadRequest("用户异常");
            var res = await marketingService.GetUserCouponList(_userid.ToInt());
            res = res.Where(p => p.IsExpress == false).ToList();
            return Ok(res);
        }


    }
}