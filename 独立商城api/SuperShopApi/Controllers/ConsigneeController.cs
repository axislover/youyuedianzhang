﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CqBuiltinService.Controller;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.Area;
using SuperShopInfrastructure.Models.Consignee;
using SuperShopInfrastructure.Static;
using SuperShopService.IServices;

namespace SuperShopApi.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ConsigneeController : ControllerBase
    {
        private readonly IConsigneeService consigneeService;
        private readonly IAreaService areaService;

        public ConsigneeController(
            IConsigneeService consigneeService,
            IAreaService areaService)
        {
            this.consigneeService = consigneeService;
            this.areaService = areaService;
        }

        /// <summary>
        /// 添加收货地址
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [ModelValidate]
        [HttpPost("")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create([FromBody]  CreateOrEditConsigneeCommand command)
        {
            if (!ValidateUtil.IsValidMobile(command.ConsigneePhone))
                return BadRequest("手机格式错误");
            var decErrMsg = command.DecryptProperty();
            if (!string.IsNullOrEmpty(decErrMsg))
                return BadRequest(decErrMsg);
            var result = await consigneeService.Add(command);
            if (!result.IsSuccess)
                return BadRequest(result.FailureReason);
            return Ok(result.GetData());
        }

        /// <summary>
        /// 修改收货地址
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        [ModelValidate]
        [HttpPut("")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Edit([FromBody]  CreateOrEditConsigneeCommand command)
        {
            if (command.Id <= 0)
                return BadRequest("参数异常");
            if (!ValidateUtil.IsValidMobile(command.ConsigneePhone))
                return BadRequest("手机格式错误");
            var decErrMsg = command.DecryptProperty();
            if (!string.IsNullOrEmpty(decErrMsg))
                return BadRequest(decErrMsg);
            await consigneeService.Edit(command);
            return Ok(true);
        }

        /// <summary>
        /// 删除收获地址
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpDelete("{id}/user/{userid}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Del([FromRoute] int id, [FromRoute] string userid)
        {
            string _userid = ToolManager.DecryptParms(userid);
            if (_userid == null)
                return BadRequest("用户异常");
            await consigneeService.Del(id, _userid.ToInt());
            return Ok(true);
        }

        /// <summary>
        /// 修改状态
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userid"></param>
        /// <param name="isdefault"></param>
        /// <returns></returns>
        [HttpPut("{id}/user/{userid}/state/{isdefault}")]
        [ProducesResponseType(typeof(bool), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> UpdateIsDefault([FromRoute] int id, [FromRoute] string userid, [FromRoute] int isdefault)
        {
            string _userid = ToolManager.DecryptParms(userid);
            if (_userid == null)
                return BadRequest("用户异常");
            await consigneeService.UpdateIsDefault(id, _userid.ToInt(), isdefault);
            return Ok(true);
        }

        /// <summary>
        /// 获取用户地址列表
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet("")]
        [ProducesResponseType(typeof(List<ConsigneeViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetList([FromQuery] string userid)
        {
            string _userid = ToolManager.DecryptParms(userid);
            if (_userid == null)
                return BadRequest("用户异常");
            var res = await consigneeService.GetList(_userid.ToInt());
            return Ok(res);
        }

        /// <summary>
        /// 根据父ID获取地区
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="userid"></param>
        /// <returns></returns>
        [HttpGet("areas")]
        [ProducesResponseType(typeof(List<AreaViewModel>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(typeof(string), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> GetAreaList([FromQuery] int parentId, [FromQuery] string userid)
        {
            string _userid = ToolManager.DecryptParms(userid);
            if (_userid == null)
                return BadRequest("用户异常");
            var allAreas = await areaService.GetAll();
            var res = allAreas.Where(p => p.Parent_Id == parentId);
            return Ok(res);
        }
    }
}