﻿using CqCore.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using SuperShopInfrastructure.Weixin.Options;
using SuperShopInfrastructure.Weixin.Services;

namespace SuperShopApi.Extensions
{
    public static class WxHelperExtensions
    {
        public static IServiceCollection AddWxHelper(this IServiceCollection services)
        {
            var provider = services.BuildServiceProvider();
            var options = provider.GetRequiredService<IOptions<WxConfigOption>>().Value;
            var logger = provider.GetRequiredService<ILogRecorder<WeiXinHelper>>();
            var wxHelper = new WeiXinHelper(options, logger);
            services.AddSingleton<WeiXinHelper>(wxHelper);
            return services;
        }
    }
}
