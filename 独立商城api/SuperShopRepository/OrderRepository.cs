﻿using CqCore.DB;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.Order;
using SuperShopInfrastructure.Static;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class OrderRepository //deleted
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public OrderRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task<DataSet> GetDetail(int orderid)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@orderid",orderid)
            };
            var sql = "select ConsigneeName,ConsigneePhone,Address,RealityAmount,OrderNo,Remark,CreateTime,PayTime,CouponDelAmount,ShippingAmount,CouponName,Id,OrderAmount,DeliveryName,DeliveryNo,isnull((select top(1) Id from C_Comment with(nolock) where OrderId=@orderid order by Id),0)as CommentId  from C_Order where Id= @orderid;select ProductId, ProductName, ProppetyCombineName, BuyNum, SalePrice,(select ImagePath from C_ProductImg where ProductId = od.ProductId and IsDefault = 1 and IsDel = 0) as ImagePath from C_OrderDetails od where OrderId = @orderid";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public async Task<DataSet> GetOrderList(QueryOrderListCriteria criteria)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@spid",criteria.Spid.ToInt()),
                new SqlParameter("@userid",criteria.UserId.ToInt()),
                new SqlParameter("@offset",criteria.OffSet),
                new SqlParameter("@size",criteria.Size),
                new SqlParameter("@orderstatus",criteria.OrderStatus)
            };
            return await sqlService.ExecuteDataset(CommandType.StoredProcedure, "getorderlist_program", parms);
        }

        public async Task<int> Program_WxNotice_Cart(string out_trade_no, int status, float total_fee, string transaction_id)
        {
            SqlParameter rtn_err = await sqlService.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@orderno", out_trade_no),
                new SqlParameter("@totalFee", total_fee),
                new SqlParameter("@status",status),
                new SqlParameter("@transaction_id", transaction_id),
                rtn_err

            };
            await sqlService.ExecuteNonQuery(CommandType.StoredProcedure, "[program_wx_notice_cart]", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public async Task<bool> ServiceOrder(int orderid, int userid)
        {
            var res = true;
            SqlParameter[] parms =
            {
                new SqlParameter("@orderid",orderid),
                new SqlParameter("@userid",userid)
            };
            var sql = "update C_Order set OrderStatus=4 where Id=@orderid and UserId=@userid and OrderStatus=1";
            var ret = await sqlService.ExecuteNonQuery(CommandType.Text, sql, parms);
            if (ret != 1)
                res = false;
            return res;
        }

        public async Task<DataSet> GetOrderStatus(int orderId)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@orderId",orderId)
            };
            var sql = "select OrderStatus from C_Order where Id=@orderId";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }

        public async Task<Tuple<DataSet, int>> Program_CreateOrder_Cart(CreateOrderCommand command)
        {
            SqlParameter rtn_err = await sqlService.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@orderno", ToolManager.CreateNo()),
                new SqlParameter("@userid",command.UserId.ToInt()),
                new SqlParameter("@spid", command.Spid.ToInt()),
                new SqlParameter("@said", command.Said.ToInt()),
                new SqlParameter("@consigneeId", command.ConsigneeId),
                new SqlParameter("@remark", command.Remarks.IsNull()),
                new SqlParameter("@couponId", command.CouponId),
                new SqlParameter("@shipAmount", command.ShipAmount),
                new SqlParameter("@totalAmount", command.ToalAmount),
                new SqlParameter("@productAmount",command.ProductAmount),
                new SqlParameter("@couponAmount",command.CouponAmount),
                rtn_err

            };
            var ds = await sqlService.ExecuteDataset(CommandType.StoredProcedure, "[programe_create_order_cart]", parms);
            if (rtn_err.Value != null)
            {
                var ret = int.Parse(rtn_err.Value.ToString());
                return Tuple.Create<DataSet, int>(ds, ret);
            }
            return Tuple.Create<DataSet, int>(null, -1);

        }
    }
}
