﻿using CqCore.DB;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class SearchKeywordRepository
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public SearchKeywordRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task<DataSet> GetListDs(int shopAdminId)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAdminId",shopAdminId)
            };
            var sql = "select * from C_SearchWord where ShopAdminId=@shopAdminId and IsDel=0 and OptionStatus=1 order by Id desc";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }
    }
}
