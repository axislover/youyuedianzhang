﻿using CqCore.DB;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.User;
using SuperShopInfrastructure.Static;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class UserRepository//deleted
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public UserRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task<DataSet> WxUserLoginOrRegister(WxUserLoginCommand command)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@shopAppId", command.Spid.ToInt()),
                new SqlParameter("@nickName",command.NickName),
                new SqlParameter("@headImgUrl",command.HeadImgUrl),
                new SqlParameter("@sex",command.Sex),
                new SqlParameter("@openid",command.OpenId),
                new SqlParameter("@token",ToolManager.CreateShortToken()),
            };
            return await sqlService.ExecuteDataset(CommandType.StoredProcedure, "wx_login_register", parms);
        }
    }
}
