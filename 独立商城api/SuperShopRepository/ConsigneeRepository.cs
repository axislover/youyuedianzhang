﻿using CqCore.DB;
using SuperShopInfrastructure.Extensions;
using SuperShopInfrastructure.Models.Consignee;
using SuperShopRepository.Dbs;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopRepository
{
    public class ConsigneeRepository
    {
        private readonly ISqlService<SuperShopCustomerDb> sqlService;

        public ConsigneeRepository(ISqlService<SuperShopCustomerDb> sqlService)
        {
            this.sqlService = sqlService;
        }

        public async Task<int> Add(CreateOrEditConsigneeCommand command)
        {
            SqlParameter rtn_err = await sqlService.GetRtnParameter();
            SqlParameter[] parms =
            {
                new SqlParameter("@UserId", command.UserId.ToInt()),
                new SqlParameter("@ConsigneeName",command.ConsigneeName),
                new SqlParameter("@ConsigneePhone",command.ConsigneePhone),
                new SqlParameter("@ZipCode",command.ZipCode.IsNull()),
                new SqlParameter("@ProvinceCode ",command.ProvinceCode),
                new SqlParameter("@CityCode",command.CityCode),
                new SqlParameter("@AreaCode",command.AreaCode),
                new SqlParameter("@Address",command.Address),
                rtn_err
            };
            await sqlService.ExecuteNonQuery(CommandType.StoredProcedure, "add_consignee", parms);
            if (rtn_err.Value != null)
            {
                return int.Parse(rtn_err.Value.ToString());
            }
            return -1;
        }

        public async Task Edit(CreateOrEditConsigneeCommand command)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@Id", command.Id),
                new SqlParameter("@ConsigneeName",command.ConsigneeName),
                new SqlParameter("@ConsigneePhone",command.ConsigneePhone),
                new SqlParameter("@ZipCode",command.ZipCode.IsNull()),
                new SqlParameter("@ProvinceCode ",command.ProvinceCode),
                new SqlParameter("@CityCode",command.CityCode),
                new SqlParameter("@AreaCode",command.AreaCode),
                new SqlParameter("@Address",command.Address),
                 new SqlParameter("@UserId",command.UserId.ToInt())
            };
            var sql = "update C_Consignee set ZipCode=@ZipCode,ProvinceCode=@ProvinceCode,CityCode=@CityCode,AreaCode=@AreaCode,ConsigneeName=@ConsigneeName,ConsigneePhone=@ConsigneePhone,Address=@Address where UserId = @UserId and Id = @Id";
            await sqlService.ExecuteNonQuery(CommandType.Text, sql, parms);
        }
        public async Task Del(int id, int userid)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@id", id),
                 new SqlParameter("@userid",userid),
            };
            var sql = "update C_Consignee set IsDel=1 where Id=@id and UserId=@userid";
            await sqlService.ExecuteNonQuery(CommandType.Text, sql, parms);
        }

        public async Task UpdateIsDefault(int id, int userid, int isdefault)
        {
            SqlParameter[] parms =
            {
                new SqlParameter("@id", id),
                new SqlParameter("@userid",userid),
                new SqlParameter("@isdefault",isdefault)
            };
            await sqlService.ExecuteNonQuery(CommandType.StoredProcedure, "update_consignee_isdefault", parms);
        }

        public async Task<DataSet> GetList(int userid)
        {
            SqlParameter[] parms =
            {
                 new SqlParameter("@userid",userid),
            };
            var sql = "select Id,ConsigneeName,ConsigneePhone,IsDefault,Address,ZipCode,(select Area_Name from C_Areas where Area_Code =ProvinceCode ) as ProvinceName,(select Area_Name from C_Areas where Area_Code = CityCode) as CityName, (select Area_Name from C_Areas where Area_Code = AreaCode) as AreaName,ProvinceCode,CityCode,AreaCode from C_Consignee  where UserId=@userid and IsDel=0";
            return await sqlService.ExecuteDataset(CommandType.Text, sql, parms);
        }
    }
}
