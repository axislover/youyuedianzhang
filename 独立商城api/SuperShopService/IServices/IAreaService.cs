﻿using SuperShopInfrastructure.Models.Area;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface IAreaService
    {
        Task<List<AreaViewModel>> Get(int parentId);
        Task<List<AreaViewModel>> GetAll();
    }
}
