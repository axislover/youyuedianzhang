﻿using CqCore.Commands;
using SuperShopInfrastructure.Models.Order;
using SuperShopInfrastructure.Models.Ship;
using SuperShopInfrastructure.Weixin.Services;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface IOrderService
    {
        Task<OrderDetail_InfoViewModel> GetDetail(int orderid);
        Task<int> GetOrderStatus(int orderId);
        Task<List<OrderListViewModel>> GetOrderList(QueryOrderListCriteria criteria);
        Task<bool> ServiceOrder(int orderid, int userid);
        Task<CommandResult<CreateOrderResultViewModel>> CreateOrder(CreateOrderCommand command);
        Task<WxPayData> WxCallBack(StringBuilder stringBuilder);
        Task<CreateOrderResultViewModel> GetPayData(string userid, string orderno);
        Task<CommandResult<decimal>> CountShipAmount(CountShipAmountCommand command);
    }
}
