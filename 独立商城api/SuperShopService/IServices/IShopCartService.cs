﻿using SuperShopInfrastructure.Models.Order;
using SuperShopInfrastructure.Models.ShopCart;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.IServices
{
    public interface IShopCartService
    {
        Task<List<ShopCartListViewModel>> GetShopCartList(string skuids);
        Task<bool> CreateOrderInsert(CreateOrderCommand command);
    }
}
