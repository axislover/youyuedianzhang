﻿using CqCore.Commands;
using Microsoft.Extensions.Options;
using SuperShopInfrastructure.Models.Coupon;
using SuperShopInfrastructure.Models.Ship;
using SuperShopInfrastructure.Static;
using SuperShopRepository;
using SuperShopService.IServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using UploadService.Option;

namespace SuperShopService.Services
{
    public class MarketingService : IMarketingService
    {
        private readonly MarketingRepository marketingRepository;
        private readonly ImageServiceOption imageServiceOption;

        public MarketingService(
            MarketingRepository marketingRepository,
            IOptions<ImageServiceOption> options)
        {
            this.marketingRepository = marketingRepository;
            this.imageServiceOption = options.Value;
        }

        public async Task<FullSetViewModel> GetSetModel(int shopAdminId)
        {
            var res = new FullSetViewModel();
            res.NotHasAreaNames = new List<string>();
            res.NotHasProductImgs = new List<string>();
            var ds = await marketingRepository.GetSetModel(shopAdminId);
            if (DataManager.CheckDs(ds, 3))
            {
                if (DataManager.CheckHasRow(ds.Tables[0]))
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    res.Id = Convert.ToInt32(dr["Id"]);
                    res.Amount = Convert.ToInt32(dr["Amount"]);
                    res.OptionStatus = Convert.ToInt32(dr["OptionStatus"]);
                    res.NotHasProductIds = dr["NotHasProductIds"].ToString();
                    res.NotHasProvinceIds = dr["NotHasProvinceIds"].ToString();
                }
                if (DataManager.CheckHasRow(ds.Tables[1]))
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        res.NotHasProductImgs.Add(imageServiceOption.ImgHost + dr["ImagePath"].ToString());
                    }
                }
                if (DataManager.CheckHasRow(ds.Tables[2]))
                {
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {
                        res.NotHasAreaNames.Add(dr["Area_Name"].ToString());
                    }
                }
            }

            return res;
        }

        public async Task<CommandResult<bool>> GetCoupon(int userid, int couponid)
        {
            var ret = await marketingRepository.GetCoupon(userid, couponid);
            switch (ret)
            {
                case 1:
                    return CommandResult<bool>.Success(true);
                case 2:
                    return CommandResult<bool>.Failure("优惠券不存在或已删除");
                case 3:
                    return CommandResult<bool>.Failure("优惠券数量不足");
                case 4:
                    return CommandResult<bool>.Failure("该优惠券已领取过了");
                default:
                    return CommandResult<bool>.Failure("请求出错");

            }
        }

        public async Task<List<CouponViewModel>> GetCouponList(int shopAdminId)
        {
            var res = new List<CouponViewModel>();
            var ds = await marketingRepository.GetCouponList(shopAdminId);
            if (DataManager.CheckDs(ds, 1) && DataManager.CheckHasRow(ds.Tables[0]))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new CouponViewModel()
                    {
                        Id = Convert.ToInt32(dr["Id"]),
                        CouponName = dr["CouponName"].ToString(),
                        CouponType = Convert.ToInt32(dr["CouponType"]),
                        DelAmount = DataManager.GetRowValue_Decimal(dr["DelAmount"]),
                        Discount = DataManager.GetRowValue_Int(dr["Discount"]),
                        MinOrderAmount = DataManager.GetRowValue_Decimal(dr["MinOrderAmount"]),
                        Days = Convert.ToInt32(dr["Days"]),
                        TotalCount = Convert.ToInt32(dr["TotalCount"])
                    });
                }
            }
            return res;
        }

        public async Task<UserCouponViewModel> GetUserCoupon(int userid, int couponid)
        {
            var res = new UserCouponViewModel();
            var ds = await marketingRepository.GetUserCoupon(userid, couponid);
            if (DataManager.CheckDs(ds, 1) && DataManager.CheckHasRow(ds.Tables[0]))
            {
                DataRow dr = ds.Tables[0].Rows[0];
                res.CouponId = Convert.ToInt32(dr["CouponId"]);
                res.CouponName = dr["CouponName"].ToString();
                res.CouponType = Convert.ToInt32(dr["CouponType"]);
                res.DelAmount = DataManager.GetRowValue_Decimal(dr["DelAmount"]);
                res.Discount = DataManager.GetRowValue_Int(dr["Discount"]);
                res.MinOrderAmount = DataManager.GetRowValue_Decimal(dr["MinOrderAmount"]);
                res.Days = Convert.ToInt32(dr["Days"]);
                res.CreateTime = Convert.ToDateTime(dr["CreateTime"]);

            }
            return res;
        }

        public async Task<List<UserCouponViewModel>> GetUserCouponList(int userid)
        {
            var res = new List<UserCouponViewModel>();
            var ds = await marketingRepository.GetUserCouponList(userid);
            if (DataManager.CheckDs(ds, 1) && DataManager.CheckHasRow(ds.Tables[0]))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    res.Add(new UserCouponViewModel()
                    {
                        CouponId = Convert.ToInt32(dr["CouponId"]),
                        CouponName = dr["CouponName"].ToString(),
                        CouponType = Convert.ToInt32(dr["CouponType"]),
                        DelAmount = DataManager.GetRowValue_Decimal(dr["DelAmount"]),
                        Discount = DataManager.GetRowValue_Int(dr["Discount"]),
                        MinOrderAmount = DataManager.GetRowValue_Decimal(dr["MinOrderAmount"]),
                        Days = Convert.ToInt32(dr["Days"]),
                        CreateTime = Convert.ToDateTime(dr["CreateTime"]),
                    });
                }
            }
            return res;
        }
    }
}
