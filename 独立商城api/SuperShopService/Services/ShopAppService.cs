﻿using CqCore.Redis;
using SuperShopInfrastructure.Models.ShopApp;
using SuperShopInfrastructure.Static;
using SuperShopRepository;
using SuperShopService.IServices;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.Services
{
    public class ShopAppService : IShopAppService
    {
        private readonly IRedisService redisService;
        private readonly ShopAppRepository shopAppRepository;

        public ShopAppService(
            IRedisService redisService,
            ShopAppRepository shopAppRepository)
        {
            this.redisService = redisService;
            this.shopAppRepository = shopAppRepository;
        }

        public async Task<string> GetPayKeyByAppid(string appid)
        {
            var res = "";
            var ds = await shopAppRepository.GetPaykeyByAppid(appid);
            if (DataManager.CheckDs(ds, 1) && DataManager.CheckHasRow(ds.Tables[0]))
            {
                var dr = ds.Tables[0].Rows[0];
                res = dr["PaySecret"].ToString();
            }
            return res;
        }

        public async Task<ShopAppViewModel> GetShopAppModel(int id)
        {
            var result = await redisService.GetOrSet<ShopAppViewModel>("GetShopAppModel_" + id, async () =>
            {
                //deleted
                var ds = await shopAppRepository.GetShopAppModel(id);
                if (DataManager.CheckDs(ds, 1) && DataManager.CheckHasRow(ds.Tables[0]))
                {
                    var res = new ShopAppViewModel();
                    var dr = ds.Tables[0].Rows[0];
                    res.Id = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    res.AppId = dr["AppId"].ToString();
                    res.AppSecret = dr["AppSecret"].ToString();
                    res.ShopAdminId = SecretClass.EncryptQueryString(dr["ShopAdminId"].ToString());
                    res.PaymentId = dr["PaymentId"].ToString();
                    res.PaySecret = dr["PaySecret"].ToString();
                    res.ServiceQQ = dr["ServiceQQ"].ToString();
                    res.ServiceWx = dr["ServiceWx"].ToString();
                    res.ServicePhone = dr["ServicePhone"].ToString();
                    res.ShopName = dr["ShopName"].ToString();
                    res.OptionStatus = Convert.ToInt32(dr["OptionStatus"]);
                    res.SecretId = SecretClass.EncryptQueryString(dr["Id"].ToString());
                    return res;
                }
                return null;
            });
            return result;
        }
    }
}
