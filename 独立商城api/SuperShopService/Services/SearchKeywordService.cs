﻿using CqCore.Redis;
using SuperShopInfrastructure.Models.SearchKeyword;
using SuperShopInfrastructure.Static;
using SuperShopRepository;
using SuperShopService.IServices;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace SuperShopService.Services
{
    public class SearchKeywordService : ISearchKeywordService
    {
        private readonly IRedisService redisService;
        private readonly SearchKeywordRepository searchKeywordRepository;

        public SearchKeywordService(
            IRedisService redisService,
            SearchKeywordRepository searchKeywordRepository)
        {
            this.redisService = redisService;
            this.searchKeywordRepository = searchKeywordRepository;
        }

        public async Task<List<SearchWordViewModel>> GetList(int said)
        {
            var result = await redisService.GetOrSet<List<SearchWordViewModel>>("GetSearchKeyword_"+said, async () =>
            {
                var res = new List<SearchWordViewModel>();
                var ds = await searchKeywordRepository.GetListDs(said);
                if (DataManager.CheckDs(ds, 1) && DataManager.CheckHasRow(ds.Tables[0]))
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        res.Add(new SearchWordViewModel()
                        {
                            Id = Convert.ToInt32(dr["Id"]),
                            KeyWord = dr["KeyWord"].ToString()
                        });
                    }
                }
                return res;
            }, 24);
            return result;
        }
    }
}
