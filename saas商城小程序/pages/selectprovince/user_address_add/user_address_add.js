import TestData from '../../../mock/testData.js'
import WxValidate from '../../../utils/wx_validate.js';
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    canClick: true,
    selfType: 0, //编辑地址返回1
    editId: null,
    form: {
      ConsigneeName: '',
      ConsigneePhone: '',
      ZipCode: '',
      Address: '',
    },
    multiIndex: [0, 0, 0],
    multiArrayNew: [], // 弹窗显示数据用
    ProvinceCode: null, //省 页面显示数据用
    CityCode: null, //市
    AreaCode: null //区: '',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.hideShareMenu()
    if (options.type == 1) {
      wx.setNavigationBarTitle({
        title: '编辑地址'
      })
      this.setData({
        selfType: 1
      });
      let addressInfo = JSON.parse(options.addressInfo)
      this.setData({
        form: {
          ConsigneeName: addressInfo.ConsigneeName,
          ConsigneePhone: addressInfo.ConsigneePhone,
          ZipCode: addressInfo.ZipCode,
          Address: addressInfo.Address,
        },
        editId: addressInfo.Id
        // ProvinceCode: [{ AreaName:addressInfo.ProvinceName}], //省
        // CityCode: [{ AreaName: addressInfo.CityName}], //市
        // AreaCode: [{ AreaName: addressInfo.AreaName}] //区: '',
      })
    }


    //初始化 获取省 市 区
    this.initMobileAreaArea();
    this.initValidate();
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindMultiPickerChange: function (e) {
    this.setData({
      multiIndex: e.detail.value
    })
  },
  //时间选择器
  bindMultiPickerColumnChange: function (e, num) {
    if (e.detail.column == 0) {
      let code = this.data.ProvinceCode[e.detail.value].Area_Code;
      this.initMobileAreaArea(code, 0);
    }

    if (e.detail.column == 1) {
      let code = this.data.CityCode[e.detail.value].Area_Code;
      this.initMobileAreaArea(code, 1);
    }
    console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
    var data = {
      multiIndex: this.data.multiIndex
    };
    data.multiIndex[e.detail.column] = e.detail.value;
    this.setData(data);
  },


  initMobileAreaArea(e, leave) {
    let arr = [];
    if (leave == 0) {
      TestData.MobileAreaArea(e).then((res) => {
        TestData.MobileAreaArea(res.Data[0].Area_Code).then((_res) => {
          //页面显示
          this.setData({
            CityCode: res.Data,
            AreaCode: _res.Data
          });
          //下拉显示
          let arr = [].concat([this.data.ProvinceCode], [this.data.CityCode], [this.data.AreaCode]);
          let arrnew = [];  
          arr.forEach((v, i) => {
            let arr = v.map((v, i) => {
              return v.Area_Name
            })
            arrnew.push(arr);
          });
          this.setData({
            multiArrayNew: arrnew
          });
        })
        return
      })
    }
    if (leave == 1) {
      TestData.MobileAreaArea(e).then((res) => {
       
        this.setData({
          ProvinceCode: this.data.ProvinceCode,
          CityCode: this.data.CityCode,
          AreaCode: res.Data
        });
        arr[0] = this.data.ProvinceCode;
        arr[0] = arr[0].map((v, i) => {
          return v.Area_Name
        })
        arr[1] = this.data.CityCode;
        arr[1] = arr[1].map((v, i) => {
          return v.Area_Name
        })
        arr[2] = res.Data;
        arr[2] = arr[2].map((v, i) => {
          return v.Area_Name
        })
        this.setData({
          multiArrayNew: arr
        });
      })
    }

    if (leave != 0 && leave != 1) {
      TestData.MobileAreaArea(e ? e : 0).then((res) => {
        
        this.setData({
          ProvinceCode: res.Data
        });

        arr[0] = res.Data;
        arr[0] = arr[0].map((v, i) => {
          return v.Area_Name
        })
        TestData.MobileAreaArea(res.Data[0].Area_Code).then((res) => {
          this.setData({
            CityCode: res.Data
          });
          arr[1] = res.Data;
          arr[1] = arr[1].map((v, i) => {
            return v.Area_Name
          })
          TestData.MobileAreaArea(res.Data[0].Area_Code).then((res) => {
            this.setData({
              AreaCode: res.Data
            });
            arr[2] = res.Data;
            arr[2] = arr[2].map((v, i) => {
              return v.Area_Name
            })
            this.setData({
              multiArrayNew: arr
            });
            console.log(arr);
          })
        })
      })
    }
  },
  //提交
  submitForm(e) {
    const params = e.detail.value
    Object.assign(params, {
      ProvinceCode: this.data.ProvinceCode[this.data.multiIndex[0]].Area_Code,
      CityCode: this.data.CityCode[this.data.multiIndex[1]].Area_Code,
      AreaCode: this.data.AreaCode[this.data.multiIndex[2]].Area_Code,
    })
    // 传入表单数据，调用验证方法
    if (!this.WxValidate.checkForm(e)) {
      const error = this.WxValidate.errorList[0]
      // this.showModal(error)
      wx.showToast({
        title: error.msg,
        icon: 'none'
      })
      return false
    }
    //提交信息
    if (this.data.selfType == 1) {
      Object.assign(params, {
        Id: this.data.editId
      });
      TestData.MobileConsigneeUpdateAddress(params).then((res) => {
        wx.showToast({
          title: '修改成功',
          icon: 'none',
          success: function () {
            wx.navigateBack()
          }
        })
      });
      return
    }

    TestData.MobileConsigneeAdd(params).then((res) => {
      
      wx.showToast({
        title: '创建成功',
        icon: 'none',
        success: function () {
          wx.navigateBack()
        }
      })
    });

  },
  // 验证规则
  initValidate: function () {
    // 验证字段的规则  :'',

    const rules = {
      ConsigneeName: {
        required: true,
        maxlength: 11
      },
      ConsigneePhone: {
        required: true,
        tel: true,
      },
      ZipCode: {
        required: false,
        // maxlength: 11
      },
      Address: {
        required: true,
        maxlength: 100
      }
    }

    //验证字段的提示信息
    const messages = {
      ConsigneeName: {
        required: '收货人是必填的'
      },
      ConsigneePhone: {
        required: '手机号是必填的'
      },
      // ZipCode: {
      //   required: '邮编是必填的'
      // },
      Address: {
        required: '地址详情是必填的'
      },

    }
    // 创建实例对象
    this.WxValidate = new WxValidate(rules, messages)
  },

})