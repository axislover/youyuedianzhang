// pages/mall/myorderdetail/myorderdetail.js
const app = getApp();
import TestData from '../../mock/testData.js'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderDetail: null,
    canClick: true,
    storeName:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) { //"20180924205026254421"
    wx.hideShareMenu()
    this.data.orderId = options.orderId;
    TestData.MobileOrderDetail(options.orderId).then((res) => {
      // res.Data.BaseInfo.Integral = 2;
      // res.Data.BaseInfo.State = 0
      this.setData({
        orderDetail: res.Data,
        storeName: app.globalData.userInfo.ShopName
      });

    });
  },
  btnComment:function(){
      wx.navigateTo({
        url: '/pages/comment/add?orderid='+this.data.orderDetail.OrderID,
      })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  btnServiceOrder:function(ev){
    var that = this;
    wx.showLoading({
      title: '请求中...',
    })
    wx.request({
      //url: app.appSetting.host + "/api/MobileMall/index?ShopID=1AD227F46DFB1666",
      url: app.appSetting.host + "/api/Order/ServiceOrder?orderid=" + this.data.orderDetail.OrderID + "&userid=" + app.globalData.userInfo.Id,
      data: {
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.Code = 200 && res.data.Message == '') {
          wx.showToast({
            title: '申请成功,请联系客服沟通',
            icon: 'none',
            duration: 2000
          })
          that.setData({
            'orderDetail.OrderStatus':4
          });
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    }) 
  },
  btnViewOrder: function (ev) {
    setTimeout(() => {
      this.data.canClick = true;
    }, 1000);
    let self = this;
    if (this.data.canClick) {
      this.data.canClick = false;
      TestData.MobileOrderGetPayData(self.data.orderDetail.OrderNo).then((res) => {
        if (res.Code == 200 && res.Message=='') {
          wx.requestPayment({
            'timeStamp': res.Data.PayParamater.TimeStamp,
            'nonceStr': res.Data.PayParamater.NonceStr,
            'package': res.Data.PayParamater.Package,
            'signType': res.Data.PayParamater.SignType,
            'paySign': res.Data.PayParamater.PaySign,
            success: function (res) {
              wx.showToast({
                title: '支付成功',
                icon: 'none',
                success: function () {
                  TestData.MobileOrderDetail(self.data.orderDetail.OrderID).then((res) => {
                    self.setData({
                      orderDetail: res.Data
                    });

                  });
                }
              })
            },
            'fail': function (res) {
              console.log(res);
              wx.showToast({
                title: '取消微信支付',
                icon: 'none'
              })
            },
            'complete': function (res) {
              self.setData({
                canClick: true
              });
            }
          })
        } else {
          this.data.canClick = true;
          wx.showToast({
            title: res.Message,
            icon: 'none'
          })
        }
      });
    }
  },
})