// pages/coupon/list.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    allIndex:1,
    coupons:null,
    myCoupons:null
  },
  clickTab:function(e){
    var index = e.currentTarget.dataset.index;
      this.setData({
        allIndex:index,
        coupons:null,
        myCoupons:null
      });
      if(index==1)
        this.getCouponList();
      if(index==0)
        this.getUserCouponList();
  },
  getUserCouponList:function(){
    var that = this;
    wx.showLoading({
      title: '读取我的优惠券中...',
    })
    wx.request({
      url: app.appSetting.host + "/api/Marketing/GetUserCoupon?userid=" + app.globalData.userInfo.Id ,
      data: {
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.Code = 200 && res.data.Message == '') {
          that.setData({
            myCoupons: res.data.Data,
          })
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },
  getCouponList:function(){
    var that = this;
    wx.showLoading({
      title: '读取商城优惠券中...',
    })
    wx.request({
      url: app.appSetting.host + "/api/Marketing/GetCouponList?said=" + app.appSetting.said,
      data: {
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        wx.hideLoading();
        if (res.data.Code = 200 && res.data.Message == '') {
          that.setData({
            coupons: res.data.Data,
          })
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },
  getCoupon:function(e){
    var couponid = e.currentTarget.dataset.id;
    wx.showLoading({
      title: '领取中...',
    })
    wx.request({
      url: app.appSetting.host + "/api/Marketing/GetCoupon?couponid=" + couponid + "&userid=" + app.globalData.userInfo.Id,
      data: {
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        wx.hideLoading();
        if (res.data.Code = 200 && res.data.Message == '') {
          wx.showToast({
            title: "领取成功",
            icon: 'none'
          })
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
      this.setData({
        allIndex:1,
        coupons:null,
        myCoupons:null
      });
      this.getCouponList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})