// pages/mall/user/index.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    shouquan: false,
    avatarUrl: '',
    name: '',
    userAuthenticationModel: false,
    integral: false,
    shopmodel:null,
    isexpress: 0,
    showModal:false,
    question:""
  },
  questionChange: function (event){
    console.log(event.detail.value);
      this.setData({
        "question": event.detail.value
      });

  },
  showQuestion:function(){

    this.setData({
      "showModal":true
    });
  },
  onCancel:function(){
    this.setData({
      "showModal": false
    });
  },
  onConfirm:function(){
    var question = this.data.question
    if (question != '' && question != null && question != undefined && question != "") {
      wx.showLoading({
        title: '提交中',
      })
      var that = this;
      wx.request({
        url: app.appSetting.host + "/Api/User/AddQuestion",
        data: {
          ShopAppId:app.appSetting.spid,
          ShopAdminId:app.appSetting.said,
          UserId: app.globalData.userInfo.Id,
          AppType:1,
          Question: this.data.question,
        },
        method: 'POST',
        header: {
          'content-type': 'application/json' // 默认值
        },
        success: function (res) {
          console.log(res);
          wx.hideLoading();
          if (res.data.Code = 200 && res.data.Message == '') {
            that.setData({
              "showModal": false,
              "question":""
            });
            wx.showToast({
              title: "提交成功",
              icon: 'none'
            })

          } else {
            wx.showToast({
              title: res.data.Message,
              icon: 'none'
            })
           
          }
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      isexpress: app.globalData.isExpress
    })
    wx.hideShareMenu();
    let self = this;
    if (app.globalData.userInfo && app.globalData.userInfo.NickName) {
      this.setData({
        avatarUrl: app.globalData.userInfo.HeadImgUrl,
        name: app.globalData.userInfo.NickName
      });
    } else {
      wx.getStorage({
        key: 'seltUser',
        success: function (res) {
          self.setData({
            avatarUrl: res.data.HeadImgUrl,
            name: res.data.NickName
          });
          app.globalData.userInfo=res.data;
        },
        fail: function (res) {
          self.setData({
            shouquan: true
          });
        }
      })

    }
    this.getShop();
  },
  callPhone:function(){
      wx.makePhoneCall({
        phoneNumber: this.data.shopmodel.ServicePhone,
      })
  },
  getShop:function(){
    var that = this;
    wx.showLoading({
      title: '读取中 ',
    })
    wx.request({
      //url: app.appSetting.host + "/api/MobileMall/index?ShopID=1AD227F46DFB1666",
      url: app.appSetting.host + "/api/User/GetShopApp?spid=" + app.appSetting.spid,
      data: {
      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log(res);
        wx.hideLoading();
        if (res.data.Code = 200 && res.data.Message == '') {
          that.setData({
            shopmodel: res.data.Data,
          })
        } else {
          wx.showToast({
            title: res.data.Message,
            icon: 'none'
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },
  getUserInfo: function(res) {
    let self = this;
    // 登录 
    wx.login({
      success: function(_res) {
        //获取用户信息
        wx.request({
          method: 'POST',
          url: app.appSetting.host + '/api/User/Login',
          data: {
            Spid: app.appSetting.spid,
            HeadImgUrl: res.detail.userInfo.avatarUrl,
            Sex: res.detail.userInfo.gender,
            NickName: res.detail.userInfo.nickName,
            Code: _res.code,
            AppType: 1
          },
          header: {
            'content-type': 'application/json' // 默认值
          },
          success: function(result) {
            if (result.data.Code == 200 && result.data.Message=='') {
              app.globalData.userInfo = result.data.Data;
              wx.setStorage({
                key: 'seltUser',
                data: result.data.Data,
                success: function(res) {
                  self.setData({
                    shouquan: false,
                    avatarUrl: app.globalData.userInfo.HeadImgUrl,
                    name: app.globalData.userInfo.NickName
                  });
                  
                }
              })
            } else {
              wx.showToast({
                title: result.data.Message,
                icon: 'none'
              })
            }
          },
          complete: function(res) {}
        })
      }
    });
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //


  goEditUserInfo() {
    wx.navigateTo({
      url: "/pages/mall/user/edit_user_info/edit_user_info",
    })
  },

 





})